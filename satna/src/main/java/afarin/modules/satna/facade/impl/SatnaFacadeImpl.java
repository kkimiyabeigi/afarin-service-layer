package afarin.modules.satna.facade.impl;

import afarin.modules.satna.exception.DontWithdrawConditionException;
import afarin.modules.satna.exception.SatnaErrorException;
import afarin.modules.satna.exception.SatnaTransactionException;
import afarin.modules.satna.facade.SatnaFacade;
import afarin.modules.satna.model.dto.*;
import afarin.modules.satna.model.dto.db.ContentInDto;
import afarin.modules.satna.model.dto.db.ParamsDto;
import afarin.modules.satna.model.dto.db.ReceiverInfoDto;
import afarin.modules.satna.model.dto.db.SenderInfoDto;
import afarin.modules.satna.service.SatnaService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
public class SatnaFacadeImpl implements SatnaFacade {

    @Autowired
    SatnaService satnaService;

    @Autowired
    RestTemplate restTemplate;

    private Logger logger = LoggerFactory.getLogger(SatnaFacadeImpl.class);

    @Override
    public String serializeToXML(ContentInDto contentInDto) {
        String xmlString = "";
        try {
            XmlMapper xmlMapper = new XmlMapper();
            xmlString = xmlMapper.writeValueAsString(contentInDto);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return xmlString;
    }

    @Override
    public OutputSatnaDto satnaTransaction(InputSatnaDto inputSatnaDto) {
        HttpHeaders header = new HttpHeaders();
        for (Map.Entry<String, String> entry : inputSatnaDto.getHeader().entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            header.add(key, value);
        }
        HttpEntity<String> request = new HttpEntity<>(header);
        String url = "http://RB-ACCOUNT-V1/rb-acct/ver1/accounts/" + inputSatnaDto.getSourceAccount() + "/checkCondition?amount="
                + inputSatnaDto.getAmount() + "&signedBy=" + inputSatnaDto.getStringSigners() + "&transactionDate=" + inputSatnaDto.getTransactionDate();
        try {
            ResponseEntity<AllowedDto> checkCondintionResult = restTemplate.exchange(url, HttpMethod.GET, request, AllowedDto.class);
            AllowedDto allowedDto = checkCondintionResult.getBody();
            if (!allowedDto.getAllowed().equals("True")) {
                throw new DontWithdrawConditionException();
            }
        } catch (Exception exception) {
            throw new DontWithdrawConditionException(exception.getMessage(), null);
        }

        OutputSatnaDto outputSatnaDto = new OutputSatnaDto();
        ParamsDto params = new ParamsDto();

        params.setAmount(inputSatnaDto.getAmount());
        params.setClientNo(inputSatnaDto.getClientNo());
        params.setNarrative(inputSatnaDto.getTransactionDescription());
        params.setPaymentDesc(inputSatnaDto.getReason());
        params.setRefCode(inputSatnaDto.getTraceId());
        params.setStamp(inputSatnaDto.getTraceId());

        String[] tranDate = inputSatnaDto.getTransactionDate().split("/");
        String transactionDate = "";
        for (String s : tranDate)
            transactionDate = transactionDate.concat(s);
        params.setTransactionDate(transactionDate);

        SenderInfoDto senderInfo = new SenderInfoDto(inputSatnaDto.getSourceAccount(), inputSatnaDto.getSourcePaymentId());
        ReceiverInfoDto receiverInfo = new ReceiverInfoDto(inputSatnaDto.getDestinationBank(), inputSatnaDto.getDestinationName(), inputSatnaDto.getDestinationLastName(), inputSatnaDto.getDestinationIban(), inputSatnaDto.getBranchCode(), inputSatnaDto.getDestinationPaymentId());

        params.setSenderInfo(senderInfo);
        params.setReceiverInfo(receiverInfo);

        ContentInDto contentInDto = new ContentInDto("SatnaOutTransaction", inputSatnaDto.getOriginalServiceCode(), "", "", params);
        String contentXml = serializeToXML(contentInDto);

        try {
            SatnaOutMsgDto satnaOutMsgDto = satnaService.getSatnaOutTransaction(contentXml);
            outputSatnaDto.setTraceId(inputSatnaDto.getTraceId());
            outputSatnaDto.setTransactionId(satnaOutMsgDto.getContentOutDto().getOutDto().getTransactionID());
        } catch (SatnaTransactionException stx) {
            throw new SatnaErrorException("Error In satna Transaction", stx.getErrorList());
        } catch (Exception ex) {
            logger.error(Arrays.toString(ex.getStackTrace()));
            List<String> errors = new ArrayList<>();
            errors.add(ex.getMessage());
            throw new SatnaErrorException("Unhandeled Exception", errors);
        }
        return outputSatnaDto;
    }

    @Override
    public OutputSatnaInquiryDto traceSatnaOutgoingTransfer(String traceId, String clientNo, String paymentId) {

        OutputSatnaInquiryDto outputSatnaInquiryDto = new OutputSatnaInquiryDto();
        try {
            SatnaInquiryOutMsg satnaInquiryOutMsg = satnaService.getSataInquiry(traceId, clientNo, paymentId);
            outputSatnaInquiryDto.setTransactionId(satnaInquiryOutMsg.getTransactionId());
            outputSatnaInquiryDto.setStatus(satnaInquiryOutMsg.getStatusDesc());
        } catch (SatnaTransactionException stx) {
            throw new SatnaErrorException("Error In satna", stx.getErrorList());
        } catch (Exception ex) {
            logger.error(Arrays.toString(ex.getStackTrace()));
            List<String> errors = new ArrayList<>();
            errors.add(ex.getMessage());
            throw new SatnaErrorException("Unhandeled Exception", errors);
        }
        return outputSatnaInquiryDto;

    }

}
