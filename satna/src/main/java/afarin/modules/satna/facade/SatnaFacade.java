package afarin.modules.satna.facade;

import afarin.modules.satna.model.dto.InputSatnaDto;
import afarin.modules.satna.model.dto.OutputSatnaDto;
import afarin.modules.satna.model.dto.OutputSatnaInquiryDto;
import afarin.modules.satna.model.dto.db.ContentInDto;

public interface SatnaFacade {

    String serializeToXML(ContentInDto contentInDto);

    OutputSatnaDto satnaTransaction(InputSatnaDto inputSatnaDto);

    OutputSatnaInquiryDto traceSatnaOutgoingTransfer(String traceId, String clientNo, String paymentId);
}
