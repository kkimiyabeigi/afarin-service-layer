package afarin.modules.satna.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

@Repository
public class SatnaInquiry {

    @Autowired
    EntityManagerFactory entityManagerFactory;

    public String traceSatnaOutgoingTransfer(String traceId, String clientNo, String paymentId) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        String msgOutXML = new String();

        try {

            StoredProcedureQuery satnaTrace = entityManager
                    .createStoredProcedureQuery("RB_SATNA.SATNACLIENTTRACKING_DIRECTLY")
                    .registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(4, String.class, ParameterMode.OUT);
            satnaTrace.setParameter(1, traceId);
            satnaTrace.setParameter(2, clientNo);
            satnaTrace.setParameter(3, paymentId);
            satnaTrace.execute();
            msgOutXML = (String) satnaTrace.getOutputParameterValue(4);
            return msgOutXML;
        }finally {
            entityManager.close();
        }


    }
}
