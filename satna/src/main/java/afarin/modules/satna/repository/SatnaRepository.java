package afarin.modules.satna.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

@Repository
public class SatnaRepository {

    @Autowired
    EntityManagerFactory entityManagerFactory;

    public String traceSatnaOutgoingTransfer(String traceId, String clientNo, String paymentId) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        String msgOutXML = new String();

        try {

            StoredProcedureQuery satnaTrace = entityManager
                    .createStoredProcedureQuery("RB_SATNA.SATNACLIENTTRACKING_DIRECTLY")
                    .registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(4, String.class, ParameterMode.OUT);
            satnaTrace.setParameter(1, traceId);
            satnaTrace.setParameter(2, clientNo);
            satnaTrace.setParameter(3, paymentId);
            satnaTrace.execute();
            msgOutXML = (String) satnaTrace.getOutputParameterValue(4);
            return msgOutXML;
        }finally {
            entityManager.close();
        }

    }


    public String getSatnaOutTransaction(String satnaOutTransactionXml) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        String xmlMsgOut = new String();
        String errorNumber = new String();

        try {
            StoredProcedureQuery satnaOut = entityManager
                    .createStoredProcedureQuery("RB_SATNA.PROCESS_MSG")
                    .registerStoredProcedureParameter(1, String.class, ParameterMode.INOUT)
                    .registerStoredProcedureParameter(2, String.class, ParameterMode.INOUT)
                    .registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(5, String.class, ParameterMode.IN);
            satnaOut.setParameter(1, errorNumber);
            satnaOut.setParameter(2, xmlMsgOut);
            satnaOut.setParameter(3, satnaOutTransactionXml);
            satnaOut.setParameter(4, "ip");
            satnaOut.setParameter(5, "port");
            satnaOut.execute();

            //todo  check errorNumber
            errorNumber = (String) satnaOut.getOutputParameterValue(1);
            xmlMsgOut = (String) satnaOut.getOutputParameterValue(2);

            return xmlMsgOut;
        } finally {
            entityManager.close();
        }
    }
}
