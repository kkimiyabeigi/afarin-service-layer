package afarin.modules.satna.model.dto;

import afarin.modules.satna.repository.SatnaInquiry;

import java.util.List;
import java.util.Map;

public class InputSatnaDto {

    private String traceId;
    private String transactionDate;
    private String sourceAccount;
    private String destinationIban;
    private String destinationBank;
    private String amount;
    private String sourcePaymentId;
    private String destinationPaymentId;
    private String reason;
    private String destinationName;
    private String destinationLastName;
    private String transactionDescription;
    private String branchCode;
    private String clientNo;
    private String originalServiceCode;
    private List<String> signedBy;
    private Map<String,String> header;

    public String getTraceId() {
        return traceId;
    }

    public String getOriginalServiceCode() {
        return originalServiceCode;
    }

    public void setOriginalServiceCode(String originalServiceCode) {
        this.originalServiceCode = originalServiceCode;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getSourceAccount() {
        return sourceAccount;
    }

    public void setSourceAccount(String sourceAccount) {
        this.sourceAccount = sourceAccount;
    }

    public String getDestinationIban() {
        return destinationIban;
    }

    public void setDestinationIban(String destinationIban) {
        this.destinationIban = destinationIban;
    }

    public String getDestinationBank() {
        return destinationBank;
    }

    public void setDestinationBank(String destinationBank) {
        this.destinationBank = destinationBank;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSourcePaymentId() {
        return sourcePaymentId;
    }

    public void setSourcePaymentId(String sourcePaymentId) {
        this.sourcePaymentId = sourcePaymentId;
    }

    public String getDestinationPaymentId() {
        return destinationPaymentId;
    }

    public void setDestinationPaymentId(String destinationPaymentId) {
        this.destinationPaymentId = destinationPaymentId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public String getDestinationLastName() {
        return destinationLastName;
    }

    public void setDestinationLastName(String destinationLastName) {
        this.destinationLastName = destinationLastName;
    }

    public String getTransactionDescription() {
        return transactionDescription;
    }

    public void setTransactionDescription(String transactionDescription) {
        this.transactionDescription = transactionDescription;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getClientNo() {
        return clientNo;
    }

    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }

    public List<String> getSignedBy() {

        return signedBy;
    }

    public Map<String, String> getHeader() {
        return header;
    }

    public void setHeader(Map<String, String> header) {
        this.header = header;
    }

    public String getStringSigners() {

        String signersList = "";
        for (String signer : signedBy) {
            if (signersList.equals(""))
                signersList = signer;
            else
                signersList = signer + "," + signersList;
        }

        return signersList;
    }

    public void setSignedBy(List<String> signedBy) {
        this.signedBy = signedBy;
    }

    @Override
    public String toString() {
        return "InputSatnaDto{" +
                "traceId='" + traceId + '\'' +
                ", transactionDate='" + transactionDate + '\'' +
                ", sourceAccount='" + sourceAccount + '\'' +
                ", destinationIban='" + destinationIban + '\'' +
                ", destinationBank='" + destinationBank + '\'' +
                ", amount='" + amount + '\'' +
                ", sourcePaymentId='" + sourcePaymentId + '\'' +
                ", destinationPaymentId='" + destinationPaymentId + '\'' +
                ", reason='" + reason + '\'' +
                ", destinationName='" + destinationName + '\'' +
                ", destinationLastName='" + destinationLastName + '\'' +
                ", transactionDescription='" + transactionDescription + '\'' +
                ", branchCode='" + branchCode + '\'' +
                ", clientNo='" + clientNo + '\'' +
                ", originalServiceCode='" + originalServiceCode + '\'' +
                ", signedBy=" + getStringSigners() +
                '}';
    }
}
