package afarin.modules.satna.model.dto.db;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ReceiverInfoDto {
    @JacksonXmlProperty(localName = "BankCode")
    private String bankCode;

    @JacksonXmlProperty(localName = "Name")
    private String name;

    @JacksonXmlProperty(localName = "Family")
    private String family;

    @JacksonXmlProperty(localName = "IBAN")
    private String iban;

    @JacksonXmlProperty(localName = "BranchCode")
    private String branchCode;

    @JacksonXmlProperty(localName = "PaymentId")
    private String paymentId;

    public ReceiverInfoDto() {
    }

    public ReceiverInfoDto(String bankCode, String name, String family, String iban, String branchCode, String paymentId) {
        this.bankCode = bankCode;
        this.name = name;
        this.family = family;
        this.iban = iban;
        this.branchCode = branchCode;
        this.paymentId = paymentId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
}
