package afarin.modules.satna.model.dto.db;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class SenderInfoDto {
    @JacksonXmlProperty(localName = "AccNumber")
    private String accNumber;

    @JacksonXmlProperty
    private String withdrawID;

    public String getAccNumber() {
        return accNumber;
    }

    public void setAccNumber(String accNumber) {
        this.accNumber = accNumber;
    }

    public String getWithdrawID() {
        return withdrawID;
    }

    public void setWithdrawID(String withdrawID) {
        this.withdrawID = withdrawID;
    }

    public SenderInfoDto() {
    }

    public SenderInfoDto(String accNumber, String withdrawID) {
        this.accNumber = accNumber;
        this.withdrawID = withdrawID;
    }
}
