package afarin.modules.satna.model.dto;

public class OutputSatnaDto {

    private String traceId;

    private String transactionId;

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public String toString() {
        return "OutputSatnaDto{" +
                "traceId='" + traceId + '\'' +
                ", transactionId='" + transactionId + '\'' +
                '}';
    }
}
