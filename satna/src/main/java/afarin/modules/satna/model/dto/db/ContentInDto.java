package afarin.modules.satna.model.dto.db;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "content")
public class ContentInDto {

    @JacksonXmlProperty
    private String cmd;

    @JacksonXmlProperty(localName = "Device")
    private String device;

    @JacksonXmlProperty(localName = "Type")
    private String type;

    @JacksonXmlProperty(localName = "SubType")
    private String subType;

    @JacksonXmlProperty(localName = "Params")
    private ParamsDto params;

    public ContentInDto(String cmd, String device, String type, String subType, ParamsDto params) {
        this.cmd = cmd;
        this.device = device;
        this.type = type;
        this.subType = subType;
        this.params = params;
    }

    public ContentInDto() {
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public ParamsDto getParams() {
        return params;
    }

    public void setParams(ParamsDto params) {
        this.params = params;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }
}
