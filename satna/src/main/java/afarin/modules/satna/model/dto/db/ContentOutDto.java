package afarin.modules.satna.model.dto.db;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName ="content")
public class ContentOutDto {

    //response/content
    @JacksonXmlProperty(localName = "Device")
    private String device;

    @JacksonXmlProperty(localName = "Type")
    private String type;

    @JacksonXmlProperty(localName = "SubType")
    private String subType;

    @JacksonXmlProperty(localName = "Output")
    private OutDto outDto;

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public OutDto getOutDto() {
        return outDto;
    }

    public void setOutDto(OutDto outDto) {
        this.outDto = outDto;
    }
}
