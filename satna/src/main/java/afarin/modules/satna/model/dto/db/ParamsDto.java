package afarin.modules.satna.model.dto.db;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.sql.Date;

public class ParamsDto {

    @JacksonXmlProperty(localName = "ClientNo")
    private String clientNo;

    @JacksonXmlProperty(localName = "Stamp")
    private String stamp;

    @JacksonXmlProperty(localName = "TransactionDate")
    private String transactionDate;

    @JacksonXmlProperty(localName = "Amount")
    private String amount;

    @JacksonXmlProperty(localName = "RefCode")
    private String refCode;

    @JacksonXmlProperty(localName = "PaymentDesc1")
    private String paymentDesc;

    @JacksonXmlProperty(localName = "Narrative")
    private String narrative;

    @JacksonXmlProperty(localName = "SenderInfo")
    private SenderInfoDto senderInfo;

    @JacksonXmlProperty(localName = "ReceiverInfo")
    private ReceiverInfoDto receiverInfo;

    public ParamsDto() {
    }

    public String getRefCode() {
        return refCode;
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public String getStamp() {
        return stamp;
    }

    public void setStamp(String stamp) {
        this.stamp = stamp;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPaymentDesc() {
        return paymentDesc;
    }

    public void setPaymentDesc(String paymentDesc) {
        this.paymentDesc = paymentDesc;
    }

    public String getClientNo() {
        return clientNo;
    }

    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }

    public String getNarrative() {
        return narrative;
    }

    public void setNarrative(String narrative) {
        this.narrative = narrative;
    }

    public SenderInfoDto getSenderInfo() {
        return senderInfo;
    }

    public void setSenderInfo(SenderInfoDto senderInfo) {
        this.senderInfo = senderInfo;
    }

    public ReceiverInfoDto getReceiverInfo() {
        return receiverInfo;
    }

    public void setReceiverInfo(ReceiverInfoDto receiverInfo) {
        this.receiverInfo = receiverInfo;
    }
}
