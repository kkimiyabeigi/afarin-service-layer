package afarin.modules.satna.model.dto;

public class OutputSatnaInquiryDto {

    private String transactionId;

    private String status;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "OutputSatnaInquiryDto{" +
                "transactionId='" + transactionId + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
