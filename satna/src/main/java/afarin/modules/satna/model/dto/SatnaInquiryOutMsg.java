package afarin.modules.satna.model.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName ="response")
public class SatnaInquiryOutMsg {

    @JacksonXmlProperty(localName = "errno")
    private String errno;

    @JacksonXmlProperty(localName = "desc")
    private String desc;

    @JacksonXmlProperty(localName = "fdesc")
    private String fdesc;

    @JacksonXmlProperty(localName = "Status")
    private String status;

    @JacksonXmlProperty(localName = "StatusDesc")
    private String statusDesc;

    @JacksonXmlProperty(localName = "RefCode")
    private String refCode;

    @JacksonXmlProperty(localName = "TransactionID")
    private String transactionId;

    public SatnaInquiryOutMsg(String errno, String desc, String fdesc, String status, String statusDesc, String refCode, String transactionId) {
        this.errno = errno;
        this.desc = desc;
        this.fdesc = fdesc;
        this.status = status;
        this.statusDesc = statusDesc;
        this.refCode = refCode;
        this.transactionId = transactionId;
    }

    public SatnaInquiryOutMsg() {
    }

    public String getErrno() {
        return errno;
    }

    public void setErrno(String errno) {
        this.errno = errno;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getFdesc() {
        return fdesc;
    }

    public void setFdesc(String fdesc) {
        this.fdesc = fdesc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getRefCode() {
        return refCode;
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
