package afarin.modules.satna.model.dto;

import afarin.modules.satna.model.dto.db.ContentOutDto;
import afarin.modules.satna.model.dto.db.OutDto;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName ="response")
public class SatnaOutMsgDto {

    //response
    @JacksonXmlProperty(localName = "cmd")
    private String cmd;

    @JacksonXmlProperty(localName = "result")
    private String result;

    @JacksonXmlProperty(localName = "errno")
    private String errno;

    @JacksonXmlProperty(localName = "desc")
    private String desc;

    @JacksonXmlProperty(localName = "fdesc")
    private String fdesc;

    @JacksonXmlProperty(localName = "msgid")
    private String msgid;

    @JacksonXmlProperty(localName = "jobid")
    private String jobid;

    @JacksonXmlProperty(localName = "content")
    private ContentOutDto contentOutDto;

    public ContentOutDto getContentOutDto() {
        return contentOutDto;
    }

    public void setContentOutDto(ContentOutDto contentOutDto) {
        this.contentOutDto = contentOutDto;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getErrno() {
        return errno;
    }

    public void setErrno(String errno) {
        this.errno = errno;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getFdesc() {
        return fdesc;
    }

    public void setFdesc(String fdesc) {
        this.fdesc = fdesc;
    }

    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid;
    }

    public String getJobid() {
        return jobid;
    }

    public void setJobid(String jobid) {
        this.jobid = jobid;
    }

    @Override
    public String toString() {
        return "SatnaOutMsgDto{" +
                "cmd='" + cmd + '\'' +
                ", result='" + result + '\'' +
                ", errno='" + errno + '\'' +
                ", desc='" + desc + '\'' +
                ", fdesc='" + fdesc + '\'' +
                '}';
    }
}
