package afarin.modules.satna.service.impl;

import afarin.modules.satna.exception.SatnaTransactionException;
import afarin.modules.satna.model.dto.SatnaInquiryOutMsg;
import afarin.modules.satna.model.dto.SatnaOutMsgDto;
import afarin.modules.satna.repository.SatnaRepository;
import afarin.modules.satna.service.SatnaService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SatnaServiceImpl implements SatnaService {

    private Logger logger = LoggerFactory.getLogger(SatnaServiceImpl.class);

    @Autowired
    SatnaRepository satnaRepository;

    @Override
    @Transactional(readOnly = false,
            propagation = Propagation.REQUIRES_NEW,
            rollbackFor = {Exception.class, SatnaTransactionException.class})
    public SatnaOutMsgDto getSatnaOutTransaction(String satnaOutTransactionXml) throws SatnaTransactionException {
        logger.debug(satnaOutTransactionXml);
        String satnaOutput = satnaRepository.getSatnaOutTransaction(satnaOutTransactionXml);
        logger.debug(satnaOutput);
        SatnaOutMsgDto satnaOutMsgDto = deserializeFromXML(satnaOutput);
        if (!(satnaOutMsgDto.getErrno().equals("000000") || satnaOutMsgDto.getErrno().equals("0"))){
            List<String> errors = new ArrayList<String>();
            errors.add(satnaOutMsgDto.getErrno());
            errors.add(satnaOutMsgDto.getDesc());
            errors.add(satnaOutMsgDto.getFdesc());
            logger.debug(satnaOutMsgDto.toString());
            throw new SatnaTransactionException("error in Satna Transaction", errors);
        }

        return satnaOutMsgDto;
    }

    @Override
    public SatnaInquiryOutMsg getSataInquiry(String traceId, String clientNo, String paymentId)
            throws SatnaTransactionException {
        String satnaInquiryOutMsgXML = satnaRepository.traceSatnaOutgoingTransfer(traceId, clientNo, paymentId);

        SatnaInquiryOutMsg satnaInquiryOutMsg = new SatnaInquiryOutMsg();
        satnaInquiryOutMsg = deserializeInquiryOutFromXML(satnaInquiryOutMsgXML);

        if (!(satnaInquiryOutMsg.getErrno().equals("000000") || satnaInquiryOutMsg.getErrno().equals("0"))){
            List<String> errors = new ArrayList<String>();
            errors.add(satnaInquiryOutMsg.getErrno());
            errors.add(satnaInquiryOutMsg.getDesc());
            errors.add(satnaInquiryOutMsg.getFdesc());
            throw new SatnaTransactionException("error in Satna", errors);
        }

        return satnaInquiryOutMsg;
    }

    @Override
    public SatnaInquiryOutMsg deserializeInquiryOutFromXML(String input) {
        SatnaInquiryOutMsg satnaInquiryOutMsg = new SatnaInquiryOutMsg();
        try {
            XmlMapper xmlMapper = new XmlMapper();
            satnaInquiryOutMsg = xmlMapper.readValue(input, SatnaInquiryOutMsg.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return satnaInquiryOutMsg;
    }

    @Override
    public SatnaOutMsgDto deserializeFromXML(String input) {
        SatnaOutMsgDto satnaOutMsgDto = new SatnaOutMsgDto();
        try {
            XmlMapper xmlMapper = new XmlMapper();
            satnaOutMsgDto = xmlMapper.readValue(input, SatnaOutMsgDto.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return satnaOutMsgDto;
    }

}
