package afarin.modules.satna.service;

import afarin.modules.satna.exception.SatnaTransactionException;
import afarin.modules.satna.model.dto.SatnaInquiryOutMsg;
import afarin.modules.satna.model.dto.SatnaOutMsgDto;

import java.util.Date;

public interface SatnaService {

    SatnaOutMsgDto getSatnaOutTransaction(String satnaOutTransactionXml) throws SatnaTransactionException;

    SatnaInquiryOutMsg getSataInquiry(String traceId, String clientNo, String paymentId)  throws SatnaTransactionException;

    SatnaInquiryOutMsg deserializeInquiryOutFromXML(String input);

    SatnaOutMsgDto deserializeFromXML(String input);

}
