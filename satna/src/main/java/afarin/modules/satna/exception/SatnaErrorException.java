package afarin.modules.satna.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class SatnaErrorException extends GeneralRestException {

    public SatnaErrorException(String message) {
        super(HttpStatus.NOT_ACCEPTABLE, message);
    }

    public SatnaErrorException() {
        super(HttpStatus.NOT_ACCEPTABLE);
    }

    public SatnaErrorException(String errorDesc, List<String> errorDetail) {

        super(HttpStatus.NOT_ACCEPTABLE, errorDesc, errorDetail);
    }
}
