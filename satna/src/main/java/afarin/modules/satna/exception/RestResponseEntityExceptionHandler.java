package afarin.modules.satna.exception;

import afarin.modules.base.language.MessageApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Locale;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    Logger loger = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);

    @Autowired
    MessageApi messageApi;

    private String language;

    @ExceptionHandler(value = {IllegalArgumentException.class, IllegalStateException.class})
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        String bodyOfResponse = "Error in Request Processing";
        loger.info(bodyOfResponse + " : " + ex.getMessage());
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(value = {GeneralRestException.class, SatnaTransactionException.class})
    protected ResponseEntity<ErrorClass> handledException(RuntimeException ex, WebRequest request) {
        String lang = request.getHeader("Accept-Language");
        this.language = lang == null ? "EN" : lang;
        String bodyOfResponse = ex.toString();

        GeneralRestException exc = (GeneralRestException) ex;
        ErrorClass errorClass = createErrorClass(exc);
        logger.info("response is : " + "http status : " + exc.getHttpStatus() + " Body is : " + errorClass.toString());

        return ResponseEntity.status(exc.getHttpStatus()).body(errorClass);
    }


    public ErrorClass createErrorClass(GeneralRestException exObject) {
        ErrorClass errorClass = new ErrorClass();
        String errorCode = "";
        String errorDesc = exObject.getErrorDesc();
        if (exObject.getErrorCode().equals("")) {
            errorCode = findErrorCode(exObject.getClassName());
        }

        errorDesc = findErrorDesc(errorCode, exObject.getErrorDesc());

        if (exObject.getErrorDetails() != null) {
            errorClass.setErrorDetails(exObject.getErrorDetails());
        }

        errorClass.setErrorCode(errorCode);
        errorClass.setErrorDesc(errorDesc);
        return errorClass;

    }

    protected String findErrorCode(String className) {
        String errorCode = messageApi.getErrorCodeByExcpetionName(className);
        return errorCode;
    }

    private String findErrorDesc(String errorCode, String errorDesc) {
        String translatedErrorCode = "No Translation Found";
        try {
            if (this.language.equals("fa")) {
                Locale locale1 = new Locale("fa", "IR");
                String msg = messageApi.getPersianMessageByCode(errorCode, locale1);

                if (!msg.equals(""))
                    translatedErrorCode = msg;

            } else {
                Locale locale1 = new Locale(this.language);
                translatedErrorCode = messageApi.getMessageByCode(errorCode, locale1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            translatedErrorCode = errorDesc;
        }
        return translatedErrorCode;
    }

}