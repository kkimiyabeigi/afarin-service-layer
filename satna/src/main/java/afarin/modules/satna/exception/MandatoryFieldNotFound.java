package afarin.modules.satna.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Collections;
import java.util.List;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MandatoryFieldNotFound extends GeneralRestException {

    public MandatoryFieldNotFound(String errorDesc, List<String> errorDetail) {
        super(HttpStatus.BAD_REQUEST, errorDesc, Collections.singletonList(""));
    }
}
