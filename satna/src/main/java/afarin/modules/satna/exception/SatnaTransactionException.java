package afarin.modules.satna.exception;

import java.util.ArrayList;
import java.util.List;


public class SatnaTransactionException extends Exception {

    private List<String> errorList = new ArrayList<String>();

    public SatnaTransactionException() {
        super();
    }

    public SatnaTransactionException(String message) {
        super(message);
    }

    public SatnaTransactionException(String message, List<String> errorList) {
        super(message);
        this.errorList = errorList;
    }

    public List<String> getErrorList() {
        return errorList;
    }
}

