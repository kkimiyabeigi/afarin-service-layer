package afarin.modules.satna.controller;

import afarin.modules.base.common.utility.AccessController;
import afarin.modules.satna.facade.SatnaFacade;
import afarin.modules.satna.model.dto.InputSatnaDto;
import afarin.modules.satna.model.dto.OutputSatnaDto;
import afarin.modules.satna.model.dto.OutputSatnaInquiryDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;
import java.util.Map;

@RestController
@RequestMapping("/satna/ver1")
public class SatnaController {

    @Autowired
    SatnaFacade satnaFacade;

    private Logger logger = LoggerFactory.getLogger(SatnaController.class);

    @PostMapping(path = "/transactions/transfer")
    private ResponseEntity<OutputSatnaDto> satnaOutgoingTransfer(@RequestHeader Map<String, String> headers,
                                                                 @RequestBody InputSatnaDto inputSatnaDto) {
        logger.info("Incoming message Satna-AchOutgoing-Transfer:" + headers + " InputSatna : " + inputSatnaDto.toString());
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        inputSatnaDto.setHeader(headers);
        OutputSatnaDto outputSatnaDto = satnaFacade.satnaTransaction(inputSatnaDto);
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : " + outputSatnaDto.toString());
        return ResponseEntity.status(HttpStatus.OK).body(outputSatnaDto);
    }

    @GetMapping(path = "/transactions/inquiry")
    private ResponseEntity<OutputSatnaInquiryDto> traceSatnaOutgoingTransfer(@RequestHeader Map<String, String> headers,
                                                                             @RequestParam("traceId") String traceId,
                                                                             @RequestParam("clientNo") String clientNo,
                                                                             @RequestParam("paymentId") String paymentId,
                                                                             Locale locale) {
        logger.info("Incoming message Satna-AchOutgoing-inquiry:" + headers + " traceId : " + traceId +
                "client Number : " + clientNo + "payment Id : " + paymentId);
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        OutputSatnaInquiryDto outputSatnaInquiryDto = satnaFacade.traceSatnaOutgoingTransfer(traceId, clientNo, paymentId);
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : " + outputSatnaInquiryDto.toString());
        return ResponseEntity.status(HttpStatus.OK).body(outputSatnaInquiryDto);
    }
}
