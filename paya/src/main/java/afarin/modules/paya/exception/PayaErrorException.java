package afarin.modules.paya.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class PayaErrorException extends GeneralRestException {

    public PayaErrorException(String message) {
        super(HttpStatus.NOT_ACCEPTABLE,message );
    }
    public PayaErrorException() {
        super(HttpStatus.NOT_ACCEPTABLE);
    }

    public PayaErrorException(String errorDesc, List<String> errorDetail) {

        super(HttpStatus.NOT_ACCEPTABLE, errorDesc, errorDetail);
    }
}
