package afarin.modules.paya.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ClientNotFoundException extends GeneralRestException {

   // HttpStatus httpStatus;
    public ClientNotFoundException(String clientNo)
    {
        super(HttpStatus.NOT_FOUND,clientNo);
        //httpStatus=HttpStatus.NOT_FOUND;
    }


}
