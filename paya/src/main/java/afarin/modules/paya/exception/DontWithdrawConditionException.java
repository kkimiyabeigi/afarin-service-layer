package afarin.modules.paya.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class DontWithdrawConditionException extends GeneralRestException {

    public DontWithdrawConditionException() {
        super(HttpStatus.NOT_ACCEPTABLE);
    }

    public DontWithdrawConditionException(String errorDesc, List<String> errorDetail) {

        super(HttpStatus.NOT_ACCEPTABLE, errorDesc, errorDetail);
    }
}
