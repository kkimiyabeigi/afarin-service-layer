package afarin.modules.paya.exception;

import java.util.ArrayList;
import java.util.List;


public class PayaTransactionException extends Exception {

        List<String> errorList=new ArrayList<String>();

        public PayaTransactionException() {
            super();
        }

        public PayaTransactionException(String message) {
            super(message);
        }

    public PayaTransactionException(String message, List<String> errorList) {
            super(message);
        this.errorList = errorList;
    }

    public List<String> getErrorList() {
        return errorList;
    }
}

