package afarin.modules.paya.model.dto;

public class OutputParameterPayaInqueryDto {

    private String transactionId;
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public OutputParameterPayaInqueryDto(String transactionId, String status) {
        this.transactionId = transactionId;
        this.status = status;
    }

    public OutputParameterPayaInqueryDto() {
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public String toString() {
        return "OutputParameterPayaInqueryDto{" +
                "transactionId='" + transactionId + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
