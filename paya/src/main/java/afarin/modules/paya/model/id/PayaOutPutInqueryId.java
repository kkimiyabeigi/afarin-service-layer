package afarin.modules.paya.model.id;

public class PayaOutPutInqueryId {

    private String errorCode;
    private String errorDesc;
    private String transactionId;
    private String endToEndId;
    private String status;

    public PayaOutPutInqueryId(String errorCode, String errorDesc, String transactionId, String endToEndId, String status) {
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
        this.transactionId = transactionId;
        this.endToEndId = endToEndId;
        this.status = status;
    }

    public PayaOutPutInqueryId() {
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getEndToEndId() {
        return endToEndId;
    }

    public void setEndToEndId(String endToEndId) {
        this.endToEndId = endToEndId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
