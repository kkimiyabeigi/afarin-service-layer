package afarin.modules.paya.model.dto;

public class OutputParameterPayaDto {

    private String traceId;
    private String transactionId;
    private String endToEndId;

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getEndToEndId() {
        return endToEndId;
    }

    public void setEndToEndId(String endToEndId) {
        this.endToEndId = endToEndId;
    }

    @Override
    public String toString() {
        return "OutputParameterPayaDto{" +
                "traceId='" + traceId + '\'' +
                ", transactionId='" + transactionId + '\'' +
                ", endToEndId='" + endToEndId + '\'' +
                '}';
    }
}
