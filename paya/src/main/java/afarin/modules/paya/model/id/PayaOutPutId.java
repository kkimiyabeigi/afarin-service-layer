package afarin.modules.paya.model.id;

public class PayaOutPutId {

    private String errorCode;
    private String errorDesc;
    private String transactionStatus;
    private Long msgInternalKey;
    private String transactionId;
    private String endToEndId;

    public PayaOutPutId(String errorCode, String errorDesc, String transactionStatus, Long msgInternalKey, String transactionId, String endToEndId) {
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
        this.transactionStatus = transactionStatus;
        this.msgInternalKey = msgInternalKey;
        this.transactionId = transactionId;
        this.endToEndId = endToEndId;
    }

    public PayaOutPutId() {
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public Long getMsgInternalKey() {
        return msgInternalKey;
    }

    public void setMsgInternalKey(Long msgInternalKey) {
        this.msgInternalKey = msgInternalKey;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getEndToEndId() {
        return endToEndId;
    }

    public void setEndToEndId(String endToEndId) {
        this.endToEndId = endToEndId;
    }
}
