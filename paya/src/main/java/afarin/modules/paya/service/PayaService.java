package afarin.modules.paya.service;

import afarin.modules.paya.exception.PayaTransactionException;
import afarin.modules.paya.model.dto.InputPrameterPayaDto;
import afarin.modules.paya.model.id.PayaOutPutId;
import afarin.modules.paya.model.id.PayaOutPutInqueryId;

import java.util.Date;

public interface PayaService {

    PayaOutPutId AchOutgoing(InputPrameterPayaDto inputPrameterPayaDto) throws PayaTransactionException;

    PayaOutPutInqueryId PayaTransactionInquery(String traceId, String oreginalServiceCode, String clientNo, String tranDate)throws PayaTransactionException;

    String getJalaliByDate(Date gregorianDate, String jalaliFormat);

    String getJalaliByString(String gregorianDate, String gregFormat, String jalaliFormat);

    String getGregorianDate(String jalaliDate, String jalaliFormat, String gregFormat);
}
