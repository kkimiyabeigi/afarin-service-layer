package afarin.modules.paya.service.impl;

import afarin.modules.paya.exception.PayaTransactionException;
import afarin.modules.paya.model.dto.InputPrameterPayaDto;
import afarin.modules.paya.model.id.PayaOutPutId;
import afarin.modules.paya.model.id.PayaOutPutInqueryId;
import afarin.modules.paya.repository.CalendarRepository;
import afarin.modules.paya.repository.PayaTransactionInqueryRepository;
import afarin.modules.paya.repository.PayaTransactionRepo;
import afarin.modules.paya.service.PayaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class PayaServiceImpl implements PayaService {

    @Autowired
    PayaTransactionRepo achTransactionRepo;

    @Autowired
    PayaTransactionInqueryRepository payaTransactionInqueryRepository;

    @Autowired
    CalendarRepository calendarRepository;

    private Logger logger = LoggerFactory.getLogger(PayaServiceImpl.class);

    @Transactional(readOnly = false,
            propagation = Propagation.REQUIRES_NEW,
            rollbackFor = {Exception.class, PayaTransactionException.class})
    public PayaOutPutId AchOutgoing(InputPrameterPayaDto inputPrameterPayaDto) throws PayaTransactionException {
        logger.debug(String.valueOf(inputPrameterPayaDto));

        PayaOutPutId payaOutPutId = achTransactionRepo.AchOutgoing(inputPrameterPayaDto);

        logger.debug(String.valueOf(payaOutPutId));
        if (!(payaOutPutId.getErrorCode().equals("000000") || payaOutPutId.getErrorCode().equals("0"))) {
            List<String> errors = new ArrayList<String>();
            errors.add(payaOutPutId.getErrorCode());
            errors.add(payaOutPutId.getErrorDesc());
            logger.debug(payaOutPutId.toString());
            throw new PayaTransactionException("Error in Paya Transaction", errors);
        }
        return payaOutPutId;
    }

    @Override
    public PayaOutPutInqueryId PayaTransactionInquery(String traceId, String oreginalServiceCode, String clientNo, String tranDate) throws PayaTransactionException {
        PayaOutPutInqueryId payaOutPutInqueryId = new PayaOutPutInqueryId();
        payaOutPutInqueryId = payaTransactionInqueryRepository.getPayaTransactionInquery(traceId, oreginalServiceCode, clientNo, tranDate);
        if (!(payaOutPutInqueryId.getErrorCode().equals("000000") || payaOutPutInqueryId.getErrorCode().equals("0"))) {
            List<String> errors = new ArrayList<String>();
            errors.add(payaOutPutInqueryId.getErrorCode());
            errors.add(payaOutPutInqueryId.getErrorDesc());
            logger.debug(payaOutPutInqueryId.toString());
            throw new PayaTransactionException("Error in Paya Transaction", errors);
        }
        return payaOutPutInqueryId;
    }

    @Override
    public String getJalaliByDate(Date gregorianDate, String jalaliFormat) {
        return calendarRepository.getJalaliByDate(gregorianDate, jalaliFormat);
    }

    @Override
    public String getJalaliByString(String gregorianDate, String gregFormat, String jalaliFormat) {
        return calendarRepository.getJalaliByString(gregorianDate, gregFormat, jalaliFormat);
    }

    @Override
    public String getGregorianDate(String jalaliDate, String jalaliFormat, String gregFormat) {
        return calendarRepository.getGregorianDate(jalaliDate, jalaliFormat, gregFormat);
    }
}
