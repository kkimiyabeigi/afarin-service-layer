package afarin.modules.paya.repository;

import afarin.modules.paya.model.id.PayaOutPutInqueryId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.*;

@Repository
public class PayaTransactionInqueryRepository {

    @Autowired
    EntityManagerFactory entityManagerFactory;

    public PayaOutPutInqueryId getPayaTransactionInquery(String traceId, String oreginalServiceCode, String clientNo, String tranDate) {

        PayaOutPutInqueryId payaOutPutInqueryId = new PayaOutPutInqueryId();
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            StoredProcedureQuery payaInquery = entityManager
                    .createStoredProcedureQuery("RB_ACH_INTF.GET_STATUS_DIRECTLY")
                    .registerStoredProcedureParameter(1, String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter(2, String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter(3, String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter(4, String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter(5, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(6, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(7, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(8, String.class, ParameterMode.IN);

            payaInquery.setParameter(5, traceId);
            payaInquery.setParameter(6, oreginalServiceCode);
            payaInquery.setParameter(7, clientNo);
            payaInquery.setParameter(8, tranDate);
            payaInquery.execute();

            payaOutPutInqueryId.setErrorCode((String) payaInquery.getOutputParameterValue(1));
            payaOutPutInqueryId.setErrorDesc((String) payaInquery.getOutputParameterValue(2));
            payaOutPutInqueryId.setTransactionId((String) payaInquery.getOutputParameterValue(3));
            payaOutPutInqueryId.setStatus((String) payaInquery.getOutputParameterValue(4));
            return payaOutPutInqueryId;
        } finally {
            entityManager.close();
        }

    }
}
