package afarin.modules.paya.repository;

import afarin.modules.paya.model.dto.InputPrameterPayaDto;
import afarin.modules.paya.model.id.PayaOutPutId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.*;

@Repository
public class PayaTransactionRepo {

    @Autowired
    EntityManagerFactory entityManagerFactory;

    public PayaOutPutId AchOutgoing(InputPrameterPayaDto inputPrameterPayaDto) {

        PayaOutPutId payaOutPutId = new PayaOutPutId();
        String dbtrId = "";
        String dbtrIdTopic = "";
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        try {
            StoredProcedureQuery achTransaction = entityManager
                    .createStoredProcedureQuery("RB_ACH_INTF.MAIN_PRC")
                    .registerStoredProcedureParameter(1, String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter(2, String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter(3, String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter(4, Long.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter(5, String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter(6, String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter(7, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(8, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(9, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(10, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(11, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(12, Long.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(13, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(14, String.class, ParameterMode.IN)

                    .registerStoredProcedureParameter(15, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(16, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(17, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(18, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(19, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(20, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(21, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(22, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(23, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(24, String.class, ParameterMode.IN);

            achTransaction.setParameter(7, inputPrameterPayaDto.getTransactionDate());
            achTransaction.setParameter(8, inputPrameterPayaDto.getBranchCode());
            achTransaction.setParameter(9, inputPrameterPayaDto.getOriginalServiceCode());
            achTransaction.setParameter(10, inputPrameterPayaDto.getTraceId());
            achTransaction.setParameter(11, inputPrameterPayaDto.getDestinationPaymentId());
            achTransaction.setParameter(12, inputPrameterPayaDto.getAmount());
            achTransaction.setParameter(13, inputPrameterPayaDto.getCcy());
            achTransaction.setParameter(14, inputPrameterPayaDto.getSourceAccount());
            achTransaction.setParameter(15, dbtrId);
            achTransaction.setParameter(16, dbtrIdTopic);
            achTransaction.setParameter(17, inputPrameterPayaDto.getUserId());

            achTransaction.setParameter(18, inputPrameterPayaDto.getDestinationIban());
            achTransaction.setParameter(19, inputPrameterPayaDto.getDestinationLastName() + " " + inputPrameterPayaDto.getDestinationName());
            achTransaction.setParameter(20, inputPrameterPayaDto.getDestinationBank());
            achTransaction.setParameter(21, inputPrameterPayaDto.getSourcePaymentId());
            achTransaction.setParameter(22, inputPrameterPayaDto.getTransactionDescription());
            achTransaction.setParameter(23, inputPrameterPayaDto.getReason());
            achTransaction.setParameter(24, inputPrameterPayaDto.getClientNo());

            achTransaction.execute();
            payaOutPutId.setErrorCode((String) achTransaction.getOutputParameterValue(1));
            payaOutPutId.setErrorDesc((String) achTransaction.getOutputParameterValue(2));
            payaOutPutId.setTransactionStatus((String) achTransaction.getOutputParameterValue(3));
            payaOutPutId.setEndToEndId((String) achTransaction.getOutputParameterValue(6));
            payaOutPutId.setTransactionId((String) achTransaction.getOutputParameterValue(5));

            return payaOutPutId;
        }finally {
            entityManager.close();
        }

    }

}
