package afarin.modules.paya.controller;

import afarin.modules.base.common.utility.AccessController;
import afarin.modules.paya.exception.PayaTransactionException;
import afarin.modules.paya.facade.PayaFacade;
import afarin.modules.paya.model.dto.InputPrameterPayaDto;
import afarin.modules.paya.model.dto.OutputParameterPayaDto;
import afarin.modules.paya.model.dto.OutputParameterPayaInqueryDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/paya/ver1")
public class PayaController {

    @Autowired
    PayaFacade payaFacade;

    private Logger logger = LoggerFactory.getLogger(PayaController.class);

    @PostMapping(path = "/transactions/singel-transfer")
    private ResponseEntity<OutputParameterPayaDto> achOutgoingTransfer(@RequestHeader Map<String, String> headers,
                                                                       @RequestBody InputPrameterPayaDto inputPrameterPayaDto) throws PayaTransactionException {
        logger.info("Incoming message Paya-AchOutgoing-transfer:" + headers + " InputPrameterPaya : " + inputPrameterPayaDto.toString());
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        inputPrameterPayaDto.setHeader(headers);
        OutputParameterPayaDto outputParameterPayaDto = payaFacade.getPayaTransfer(inputPrameterPayaDto);
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : " + outputParameterPayaDto.toString());
        return ResponseEntity.status(HttpStatus.OK).body(outputParameterPayaDto);
    }

    @GetMapping(path = "/inquery/paya-singel")
    private ResponseEntity<OutputParameterPayaInqueryDto> payaOutgoingInquery(@RequestHeader Map<String, String> headers,
                                                                              @RequestParam("traceId") String traceId,
                                                                              @RequestParam("oreginalServiceCode") String oreginalServiceCode,
                                                                              @RequestParam("clientNo") String clientNo,
                                                                              @RequestParam("transactionDate") String transactionDate) {
        logger.info("Incoming message Paya-AchOutgoing-Inquery:" + headers + " traceId : " + traceId + "oreginalServiceCode : "
                + oreginalServiceCode + "client Number : " + clientNo + "transaction Date : " + transactionDate);
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        OutputParameterPayaInqueryDto outputParameterPayaInqueryDto = payaFacade.getPayaInquery(traceId, oreginalServiceCode, clientNo, transactionDate);
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : " + outputParameterPayaInqueryDto.toString());
        return ResponseEntity.status(HttpStatus.OK).body(outputParameterPayaInqueryDto);
    }

}
