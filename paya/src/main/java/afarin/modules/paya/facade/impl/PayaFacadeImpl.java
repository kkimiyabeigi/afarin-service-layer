package afarin.modules.paya.facade.impl;

import afarin.modules.paya.exception.DontWithdrawConditionException;
import afarin.modules.paya.exception.PayaErrorException;
import afarin.modules.paya.exception.PayaTransactionException;
import afarin.modules.paya.facade.PayaFacade;
import afarin.modules.paya.model.dto.AllowedDto;
import afarin.modules.paya.model.dto.InputPrameterPayaDto;
import afarin.modules.paya.model.dto.OutputParameterPayaDto;
import afarin.modules.paya.model.dto.OutputParameterPayaInqueryDto;
import afarin.modules.paya.model.id.PayaOutPutId;
import afarin.modules.paya.model.id.PayaOutPutInqueryId;
import afarin.modules.paya.service.PayaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
public class PayaFacadeImpl implements PayaFacade {

    @Autowired
    PayaService payaService;

    @Autowired
    RestTemplate restTemplate;

    private Logger logger = LoggerFactory.getLogger(PayaFacadeImpl.class);

    @Override
    public OutputParameterPayaDto getPayaTransfer(InputPrameterPayaDto inputPrameterPayaDto) throws PayaTransactionException {
        HttpHeaders header = new HttpHeaders();
        for (Map.Entry<String, String> entry : inputPrameterPayaDto.getHeader().entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            header.add(key, value);
        }
        HttpEntity<String> request = new HttpEntity<>(header);
        String url = "http://RB-ACCOUNT-V1/rb-acct/ver1/accounts/" + inputPrameterPayaDto.getSourceAccount()
                + "/checkCondition?amount=" + inputPrameterPayaDto.getAmount() + "&signedBy="
                + inputPrameterPayaDto.getStringSigners() + "&transactionDate=" + inputPrameterPayaDto.getTransactionDate();
        try {
            ResponseEntity<AllowedDto> checkConditionResult = restTemplate.exchange(url, HttpMethod.GET, request, AllowedDto.class);
            AllowedDto allowedDto = checkConditionResult.getBody();
            if (!allowedDto.getAllowed().equals(("True")))
                throw new DontWithdrawConditionException();
        } catch (Exception exception) {
            throw new DontWithdrawConditionException(exception.getMessage(), null);
        }
        OutputParameterPayaDto outputParameterPayaDto = new OutputParameterPayaDto();
        try {
            String[] tranDate =inputPrameterPayaDto.getTransactionDate().split("/");
            String transactionDate = "";
            for (String s : tranDate)
                transactionDate = transactionDate.concat(s);
            inputPrameterPayaDto.setTransactionDate(transactionDate);
           // inputPrameterPayaDto.setTransactionDate(payaService.getGregorianDate(inputPrameterPayaDto.getTransactionDate(), "YYYY/MM/DD", "YYYYMMDD"));
            PayaOutPutId payaOutPutId = payaService.AchOutgoing(inputPrameterPayaDto);
            outputParameterPayaDto.setTraceId(inputPrameterPayaDto.getTraceId());
            outputParameterPayaDto.setEndToEndId(payaOutPutId.getEndToEndId());
            outputParameterPayaDto.setTransactionId(payaOutPutId.getTransactionId());
        } catch (PayaTransactionException stx) {
            throw new PayaErrorException("Error In Paya Transaction", stx.getErrorList());
        } catch (Exception ex) {
            logger.error(Arrays.toString(ex.getStackTrace()));
            List<String> errors = new ArrayList<>();
            errors.add(ex.getMessage());
            throw new PayaErrorException("Unhandeled Exception", errors);
        }
        return outputParameterPayaDto;

    }

    @Override
    public OutputParameterPayaInqueryDto getPayaInquery(String traceId, String oreginalServiceCode, String clientNo, String transactionDate) {
        OutputParameterPayaInqueryDto outputParameterPayaInqueryDto = new OutputParameterPayaInqueryDto();
        try {
            transactionDate = payaService.getGregorianDate(transactionDate, "YYYY/MM/DD", "YYYYMMDD");
            PayaOutPutInqueryId payaOutPutId = payaService.PayaTransactionInquery(traceId, oreginalServiceCode, clientNo, transactionDate);
            outputParameterPayaInqueryDto.setTransactionId(payaOutPutId.getTransactionId());
            outputParameterPayaInqueryDto.setStatus(payaOutPutId.getStatus());
        } catch (PayaTransactionException p) {
            throw new PayaErrorException("Error In PayaInquey", p.getErrorList());
        } catch (Exception ex) {
            logger.error(Arrays.toString(ex.getStackTrace()));
            List<String> errors = new ArrayList<>();
            errors.add(ex.getMessage());
            throw new PayaErrorException("Unhandeled Exception", errors);
        }

        return outputParameterPayaInqueryDto;
    }
}
