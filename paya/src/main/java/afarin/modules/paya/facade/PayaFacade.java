package afarin.modules.paya.facade;

import afarin.modules.paya.exception.PayaTransactionException;
import afarin.modules.paya.model.dto.InputPrameterPayaDto;
import afarin.modules.paya.model.dto.OutputParameterPayaDto;
import afarin.modules.paya.model.dto.OutputParameterPayaInqueryDto;

public interface PayaFacade {

    OutputParameterPayaDto getPayaTransfer(InputPrameterPayaDto inputPrameterPayaDto) throws PayaTransactionException;

    OutputParameterPayaInqueryDto getPayaInquery(String traceId, String oreginalServiceCode, String clientNo, String tranDate);
}
