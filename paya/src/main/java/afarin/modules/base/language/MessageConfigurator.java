package afarin.modules.base.language;

import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;

public interface MessageConfigurator {
    LocaleResolver localeResolver();
    ResourceBundleMessageSource messageSource();
    ResourceBundleMessageSource persianMessage();
    ResourceBundleMessageSource exceptionMessageSource();

}
