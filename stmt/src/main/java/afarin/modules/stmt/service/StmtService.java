package afarin.modules.stmt.service;

import afarin.modules.stmt.model.entity.StmtEntity;

import java.util.Date;
import java.util.List;

public interface StmtService {

    List<StmtEntity> getStmt(Long accountInternalKey, int resultCount, Date fromDate, Date toDate);

    Date getGregorianByDate(String jalaliDate, String gregFormat);

    String getJalaliByDate(Date gregorianDate, String jalaliFormat);

    boolean isExpire(Date fromDate , Date toDate);

    boolean checkStmtDate(Date fromDate ,Date toDate);
}
