package afarin.modules.stmt.service.impl;

import afarin.modules.stmt.model.entity.StmtEntity;
import afarin.modules.stmt.repository.StmtRepositoryImpl;
import afarin.modules.stmt.repository.procedure.CalendarRepository;
import afarin.modules.stmt.service.StmtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class StmtServiceImpl implements StmtService {

    @Autowired
    StmtRepositoryImpl stmtRepositoryImpl;

    @Autowired
    CalendarRepository calendarRepository;

    @Override
    public List<StmtEntity> getStmt(Long accountInternalKey, int resultCount, Date fromDate, Date toDate) {
        return stmtRepositoryImpl.findStmtWithLimit(accountInternalKey, resultCount, fromDate, toDate);
    }

    @Override
    public Date getGregorianByDate(String jalaliDate, String gregFormat) {
        return calendarRepository.getGregorianDate(jalaliDate, gregFormat);
    }

    @Override
    public String getJalaliByDate(Date gregorianDate, String jalaliFormat) {
        return calendarRepository.getJalaliByDate(gregorianDate, jalaliFormat);
    }

    public boolean isExpire(Date fromDate , Date toDate) {
        if (toDate == null || fromDate == null )
            return true;

        if (fromDate.compareTo(toDate) > 0) {// expired
            return true;
        } else if (fromDate.compareTo(toDate) == 0) {
            // expired
            return fromDate.getTime() > toDate.getTime();
        }
        return false;
    }

    @Override
    public boolean checkStmtDate(Date fromDate, Date toDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date today = new Date();
        System.out.println("today : " + sdf.format(today));         // 2020-02-24

        Calendar sixMonthsAgo = Calendar.getInstance();
        sixMonthsAgo.add(Calendar.MONTH, -6);             // 2020-01-25

        Date thirtyDaysAgoDate = sixMonthsAgo.getTime();
        System.out.println("thirtyDaysAgo : " + sdf.format(thirtyDaysAgoDate));

//        Date newDate = sdf.parse("2009-12-31");

    /*    if (newDate.before(thirtyDaysAgoDate)) {
            System.out.println(sdf.format(newDate) + " is older than 30 days!");
        }*/
        return false;

    }


}
