package afarin.modules.stmt.model.dto;

public class ReqStmtDto {
    private Long accountInternalKey;
    private int count;
    private String fromDate;
    private String toDate;

    public ReqStmtDto() {
    }

    public ReqStmtDto(Long accountInternalKey, int count, String fromDate, String toDate) {
        this.accountInternalKey = accountInternalKey;
        this.count = count;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public Long getAccountInternalKey() {
        return accountInternalKey;
    }

    public void setAccountInternalKey(Long accountInternalKey) {
        this.accountInternalKey = accountInternalKey;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
}
