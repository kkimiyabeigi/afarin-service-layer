package afarin.modules.stmt.model.id;

import java.math.BigDecimal;
import java.sql.Date;

public class StmtId {
    private BigDecimal internalKey;
    private Date fromDate;
    private Date toDate;
    private int count;

    public BigDecimal getInternalKey() {
        return internalKey;
    }

    public void setInternalKey(BigDecimal internalKey) {
        this.internalKey = internalKey;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
