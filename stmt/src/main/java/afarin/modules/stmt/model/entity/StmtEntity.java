package afarin.modules.stmt.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
//@Table(name = "RB_TRAN_STMT")
@Table(name = "RB_TRAN_HIST")
public class StmtEntity implements Serializable {
    @Id
    @Column(name = "SEQ_NO")
    private Long seqNo;

    @Column(name="INTERNAL_KEY")
    private Long internalKey;

    @Column(name="TRAN_DATE")
    private Date transactionDate;

    @Column(name="TRAN_AMT")
    private BigDecimal amount;

    @Column(name="TIME_STAMP")
    private Date timeStamp;

    @Column(name="CR_DR_MAINT_IND")
    private String CrDr;

    @Column(name="PREVIOUS_BAL_AMT")
    private BigDecimal previousBalance;

    @Column(name="ACTUAL_BAL_AMT")
    private BigDecimal actualBalance;

    @Column(name="BRANCH")
    private String branchCode;

    @Column(name="TRAN_DESC")
    private String transactionDescption;

    @Column(name="NARRATIVE")
    private String narrative;

    public Long getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Long seqNo) {
        this.seqNo = seqNo;
    }

    public Long getInternalKey() {
        return internalKey;
    }

    public void setInternalKey(Long internalKey) {
        this.internalKey = internalKey;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCrDr() {
        return CrDr;
    }

    public void setCrDr(String crDr) {
        CrDr = crDr;
    }

    public BigDecimal getPreviousBalance() {
        return previousBalance.multiply(new BigDecimal(-1));
    }

    public void setPreviousBalance(BigDecimal previousBalance) {
        this.previousBalance = previousBalance;
    }

    public BigDecimal getActualBalance() {
        return actualBalance.multiply(new BigDecimal(-1));
    }

    public void setActualBalance(BigDecimal actualBalance) {
        this.actualBalance = actualBalance;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getTransactionDescption() {
        return transactionDescption;
    }

    public void setTransactionDescption(String transactionDescption) {
        this.transactionDescption = transactionDescption;
    }

    public String getNarrative() {
        return narrative;
    }

    public void setNarrative(String narrative) {
        this.narrative = narrative;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}
