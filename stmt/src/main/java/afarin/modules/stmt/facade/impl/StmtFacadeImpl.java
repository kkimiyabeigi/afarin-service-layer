package afarin.modules.stmt.facade.impl;

import afarin.modules.stmt.exception.ExpireDateException;
import afarin.modules.stmt.facade.StmtFacade;
import afarin.modules.stmt.model.dto.ReqStmtDto;
import afarin.modules.stmt.model.dto.StmtDto;
import afarin.modules.stmt.model.entity.StmtEntity;
import afarin.modules.stmt.service.StmtService;
import afarin.modules.stmt.service.impl.StmtServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class StmtFacadeImpl implements StmtFacade {

    @Autowired
    StmtService stmtService;

    @Override
    public List<StmtDto> getStmt (ReqStmtDto reqStmtDto){
        Date greFromDate = stmtService.getGregorianByDate(reqStmtDto.getFromDate(),"YYYY/MM/DD");
        Date greToDate = stmtService.getGregorianByDate(reqStmtDto.getToDate(),"YYYY/MM/DD");
        /*boolean isExpire = stmtService.isExpire(greToDate,new Date());
        if (isExpire)
            throw new ExpireDateException("To Date not Valid");
        isExpire = stmtService.isExpire(greFromDate,greToDate);
        if (isExpire)
            throw new ExpireDateException("From Date not Valid");*/

        List<StmtEntity> stmtEntityList = stmtService.getStmt(reqStmtDto.getAccountInternalKey(), reqStmtDto.getCount(), greFromDate, greToDate);
        return convertStmtEntityListToStmtDtoList(stmtEntityList);
    }

    private List<StmtDto> convertStmtEntityListToStmtDtoList (List<StmtEntity> stmtEntityList){
        List<StmtDto> stmtDtoList = new ArrayList<>();
        for (StmtEntity stmtEntity : stmtEntityList){
            StmtDto stmtDto = new StmtDto();
            stmtDto.setAmount(stmtEntity.getAmount());
            stmtDto.setCreditDebit(stmtEntity.getCrDr());
            stmtDto.setPreviousBalance(stmtEntity.getPreviousBalance());
            stmtDto.setActualBalance(stmtEntity.getActualBalance());
            stmtDto.setBranchCode(stmtEntity.getBranchCode());
            stmtDto.setTransactionDescription(stmtEntity.getTransactionDescption());
            stmtDto.setNarrative(stmtEntity.getNarrative());

            String tranDate = stmtService.getJalaliByDate(stmtEntity.getTransactionDate(),"YYYY/MM/DD");
            stmtDto.setTransactionDate(tranDate);

            String sysDate = stmtService.getJalaliByDate(stmtEntity.getTimeStamp(),"YYYY/MM/DD");
            stmtDto.setSystemDate(sysDate);

            stmtDtoList.add(stmtDto);
        }
        return stmtDtoList;
    }
}
