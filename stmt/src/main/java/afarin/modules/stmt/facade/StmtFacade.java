package afarin.modules.stmt.facade;

import afarin.modules.stmt.model.dto.ReqStmtDto;
import afarin.modules.stmt.model.dto.StmtDto;
import java.util.List;

public interface StmtFacade {

    List<StmtDto> getStmt(ReqStmtDto reqStmtDto);
}
