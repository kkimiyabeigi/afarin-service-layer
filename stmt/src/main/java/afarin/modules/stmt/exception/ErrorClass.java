package afarin.modules.stmt.exception;

import java.util.List;

public class ErrorClass {

    private String       errorCode ="";
    private String       errorDesc;
    private List<String> errorDetails;

    public ErrorClass(String errorCode, String errorDesc, List<String> errorDetails) {
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
        this.errorDetails = errorDetails;
    }

    public ErrorClass() {
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public List<String> getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(List<String> errorDetails) {
        this.errorDetails = errorDetails;
    }

    @Override
    public String toString() {
        return "error {" +
                "errorCode='" + errorCode + '\'' +
                ", errorDesc='" + errorDesc + '\'' +
                ", errorDetails=" + errorDetails +
                '}';
    }
}
