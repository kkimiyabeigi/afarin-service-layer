package afarin.modules.stmt.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ExpireDateException extends GeneralRestException {

    public ExpireDateException() {
        super(HttpStatus.BAD_REQUEST);
    }

    public ExpireDateException(String message) {
        super(HttpStatus.NOT_FOUND,message);
    }

    public ExpireDateException(String errorDesc, List<String> errorDetails) {
        super(HttpStatus.BAD_REQUEST, errorDesc, errorDetails);
    }

}
