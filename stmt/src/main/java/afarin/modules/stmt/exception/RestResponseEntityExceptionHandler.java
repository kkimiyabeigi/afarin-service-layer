package afarin.modules.stmt.exception;

import afarin.modules.base.language.MessageApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Locale;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

   // @Autowired
    //Translator translator;
    Logger loger = LoggerFactory.getLogger (RestResponseEntityExceptionHandler.class);

   @Autowired
   MessageApi messageApi;

   private String language;

    //@Autowired
  //  ResourceBundleMessageSource exceptionMessageSource;

    @ExceptionHandler(value = { IllegalArgumentException.class, IllegalStateException.class })
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        String bodyOfResponse = "Error in Request Processing";
       // System.out.println ("in contorl Advisor unhandled exception" );
        loger.info(bodyOfResponse+" : " +ex.getMessage ());
       // ex.printStackTrace ();
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders (), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(value= { ClientNotFoundException.class, MandatoryFieldNotFound.class ,
            NoRecordFoundException.class, ConsumerInfoNotFoundException.class})
    protected ResponseEntity<ErrorClass> handledException(RuntimeException ex, WebRequest request) {
        String lang = request.getHeader ("Accept-Language");
        this.language= lang==null?"EN":lang;
        //System.out.println ("in the Control Advisor : " + ex.getClass ().getName ()+" lang is :"+lang);
        String bodyOfResponse = ex.toString ();

        GeneralRestException exc=(GeneralRestException)ex;
        ErrorClass errorClass=createErrorClass (exc);
        logger.info ("response is : "+ "http status : "+exc.getHttpStatus ()+" Body is : " + errorClass);

        return ResponseEntity.status (exc.getHttpStatus ()).body (errorClass);
           /* return handleExceptionInternal(ex, bodyOfResponse,
                    new HttpHeaders (), HttpStatus.CONFLICT, request);*/
    }


    public ErrorClass createErrorClass(GeneralRestException exObject){
        ErrorClass errorClass=new ErrorClass ();
        String errorCode="";
        String errorDesc=exObject.getErrorDesc ();
        if (exObject.getErrorCode ().equals ("")) {
          errorCode =findErrorCode (exObject.getClassName ())  ;
        }

        errorDesc =findErrorDesc (errorCode,exObject.getErrorDesc ());

        if(exObject.getErrorDetails()!=null) {
            errorClass.setErrorDetails(exObject.getErrorDetails());
        }

        errorClass.setErrorCode (errorCode);
        errorClass.setErrorDesc (errorDesc);
        return errorClass;

    }

    protected  String findErrorCode(String className){
       // System.out.println(className);
        String errorCode= messageApi.getErrorCodeByExcpetionName(className);
        return errorCode;
    }

    private String findErrorDesc(String errorCode,String errorDesc) {
        String translatedErrorCode= "No Translation Found";
        try {

            //  translatedErrorCode =translator.getMessage (errorCode,new Locale ("en"));
            // translatedErrorCode =messageSource.getMessage (errorCode,null,new Locale ("en"));
            if (this.language.equals ("fa")) {
                Locale locale1 = new Locale ("fa", "IR");
                String msg = messageApi.getPersianMessageByCode (errorCode, locale1);

                if (!msg.equals (""))
                    translatedErrorCode=msg;

            } else {
                Locale locale1 = new Locale (this.language);
                translatedErrorCode = messageApi.getMessageByCode (errorCode, locale1);
            }
        }
        catch(Exception ex) {
            ex.printStackTrace ();
            translatedErrorCode=errorDesc;
        }
        return translatedErrorCode;
    }

}