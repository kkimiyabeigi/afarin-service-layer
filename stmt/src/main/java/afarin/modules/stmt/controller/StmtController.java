package afarin.modules.stmt.controller;

import afarin.modules.base.common.utility.AccessController;
import afarin.modules.stmt.facade.StmtFacade;
import afarin.modules.stmt.model.dto.ReqStmtDto;
import afarin.modules.stmt.model.dto.StmtDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Locale;
import java.util.Map;

@RestController
@RequestMapping("stmt/ver1")
public class StmtController {

    @Autowired
    StmtFacade stmtFacade;

    private Logger logger = LoggerFactory.getLogger(StmtController.class);

    @GetMapping(path = "/bill/{internalKey}")
    private ResponseEntity<List<StmtDto>> getStatement(@RequestHeader Map<String, String> headers,
                                                       @PathVariable("internalKey") Long accountInternalKey,
                                                       @RequestParam("count") int count,
                                                       @RequestParam("fromDate") String fromDateStr,
                                                       @RequestParam("toDate") String toDateStr,
                                                       Locale locale) {
        logger.info("Incoming message Statement:" + headers + " internalKey : " + accountInternalKey
                + "count : " + count + "from Date : " + fromDateStr + "to Date : " + toDateStr);
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        ReqStmtDto reqStmtDto = new ReqStmtDto(accountInternalKey, count, fromDateStr, toDateStr);
        List<StmtDto> stmtDtoList = stmtFacade.getStmt(reqStmtDto);
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : ");
        for (int i = 0; i < stmtDtoList.size(); i++)
            logger.info("statement " + i + " : " + stmtDtoList.get(i).toString());
        return ResponseEntity.status(HttpStatus.OK).body(stmtDtoList);
    }


}
