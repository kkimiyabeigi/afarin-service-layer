package afarin.modules.stmt.repository;

import afarin.modules.stmt.model.entity.StmtEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public class StmtRepositoryImpl implements StmtRepository {

    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Override
    public <S extends StmtEntity> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public void flush() {
    }

    @Override
    public Page<StmtEntity> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends StmtEntity> S save(S s) {
        return null;
    }

    @Override
    public Optional<StmtEntity> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {
    }

    @Override
    public void delete(StmtEntity stmtEntity) {
    }

    @Override
    public void deleteAll(Iterable<? extends StmtEntity> iterable) {
    }

    @Override
    public void deleteAll() {
    }

    @Override
    public <S extends StmtEntity> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends StmtEntity> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends StmtEntity> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends StmtEntity> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public <S extends StmtEntity> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<StmtEntity> iterable) {
    }

    @Override
    public void deleteAllInBatch() {
    }

    @Override
    public StmtEntity getOne(Long aLong) {
        return null;
    }

    @Override
    public List<StmtEntity> findAll() {
        return null;
    }

    @Override
    public List<StmtEntity> findAll(Sort sort) {
        return null;
    }

    @Override
    public List<StmtEntity> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public <S extends StmtEntity> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends StmtEntity> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public List<StmtEntity> findStmtWithLimit(Long accountInternalKey, int limit, Date fromDate, Date toDate) {
        EntityManager em = entityManagerFactory.createEntityManager();
        List<StmtEntity> stmtList = new ArrayList<StmtEntity>();
        stmtList = em.createQuery("select a from StmtEntity a where a.internalKey=?1 and a.transactionDate between ?2 and ?3 order by a.seqNo desc", StmtEntity.class)
                .setParameter(1, accountInternalKey)
                .setParameter(2, fromDate)
                .setParameter(3, toDate)
                .setMaxResults(limit).getResultList();

        return stmtList;
    }

}
