package afarin.modules.stmt.repository;

import afarin.modules.stmt.model.entity.StmtEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;


public interface StmtRepository extends JpaRepository<StmtEntity, Long>{


    List<StmtEntity> findStmtWithLimit(Long accountInternalKey, int limit, Date fromDate, Date toDate);
}
