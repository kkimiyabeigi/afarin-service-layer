package afarin.modules.stmt.repository.procedure;

import afarin.modules.stmt.exception.NoRecordFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.Timestamp;
import java.util.Date;


@Repository
public class CalendarRepository {

    @Autowired
    EntityManagerFactory entityManagerFactory;


    public Date getGregorianDate(String jalaliDate, String jalaliFormat) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Date gregorianDate;
        try {
            Timestamp ts = (Timestamp) entityManager.createNativeQuery("select TO_DATE (:jalaliDate,:jalaliFormat,'nls_calendar=persian') from dual")
                    .setParameter("jalaliDate",jalaliDate)
                    .setParameter("jalaliFormat",jalaliFormat)
                    .getSingleResult();
            gregorianDate = new Date(ts.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            throw new NoRecordFoundException();
        }finally {
            entityManager.close();
        }
        return gregorianDate;
    }

    public String getJalaliByDate(Date gregorianDate, String jalaliFormat) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        String jalaliDate;
        try {
            jalaliDate = (String) entityManager.createNativeQuery("select TO_CHAR(:gregorianDate,:jalaliFormat,'nls_calendar=persian') from dual")
                    .setParameter("gregorianDate",gregorianDate)
                    .setParameter("jalaliFormat",jalaliFormat)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            throw new NoRecordFoundException();
        }finally {
            entityManager.close();
        }
        return jalaliDate;
    }

}
