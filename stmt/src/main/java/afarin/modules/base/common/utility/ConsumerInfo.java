package afarin.modules.base.common.utility;


import com.fasterxml.jackson.annotation.JsonProperty;


public class ConsumerInfo {
    @JsonProperty("applicationId")
    private String applicationId;
    @JsonProperty("moduleId")
    private String moduleId;
    @JsonProperty("messageLanguage")
    private String messageLanguage;

    public ConsumerInfo() {
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getMessageLanguage() {
        return messageLanguage;
    }

    public void setMessageLanguage(String messageLanguage) {
        this.messageLanguage = messageLanguage;
    }

    @Override
    public String toString() {
        return "ConsumerInfo{" +
                "applicationId='" + applicationId + '\'' +
                ", moduleId='" + moduleId + '\'' +
                ", messageLanguage='" + messageLanguage + '\'' +
                '}';
    }
}
