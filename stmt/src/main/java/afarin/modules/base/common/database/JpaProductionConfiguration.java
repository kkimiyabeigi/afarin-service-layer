package afarin.modules.base.common.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Profile({"production", "test"})
@Configuration
public class JpaProductionConfiguration {

    @Autowired
    private Environment env;

    Logger logger = LoggerFactory.getLogger(JpaProductionConfiguration.class);

    /* @Bean("dataSourceLive")
 public DataSource dataSource () throws NamingException
 {

     logger.info ("JNDI property is: "+env.getProperty ("datasource.jndi.url"));

     JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
     return dataSourceLookup.getDataSource("afarinds");
 }
*/

    @Bean("dataSourceLive")
    public DataSource datasource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));
        dataSource.setUrl(env.getProperty("spring.datasource.url"));
        dataSource.setUsername(env.getProperty("spring.datasource.username"));
        dataSource.setPassword(env.getProperty("spring.datasource.password"));

        return dataSource;

    }


}