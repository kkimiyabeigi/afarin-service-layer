package afarin.modules.client.repository.client;

import afarin.modules.client.model.client.entity.MobileServiceInfoEntity;
import afarin.modules.client.model.client.id.ClientMobileServiceInfoId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientMobileServiceRepository extends JpaRepository<MobileServiceInfoEntity, ClientMobileServiceInfoId> {
    @Query(value = "select a from MobileServiceInfoEntity a where a.mobileNumber =?1 and a.clientNumber =?2 and a.service=?3")
    MobileServiceInfoEntity checkClientMobileService(String mobileNo, String clientNo, String service);
}
