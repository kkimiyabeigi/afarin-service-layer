package afarin.modules.client.repository.limit.procedure;

import afarin.modules.client.exception.CheckLimitationException;
import afarin.modules.client.exception.ClientNotFoundException;
import afarin.modules.client.repository.limit.LimitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.*;

@Repository
public class LimitRepositoryImpl implements LimitRepository {

    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Override
    public String checkLimitation(String xmlCheckCondition) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        StoredProcedureQuery checkCondition =
                entityManager.createStoredProcedureQuery("RB_EXT_DEV_TRAN_STAT.GET_TOTAL_AMT_BYID_DIRECTLY")

                        .registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
                        .registerStoredProcedureParameter(2, String.class, ParameterMode.OUT)
                        .registerStoredProcedureParameter(3, String.class, ParameterMode.OUT);
         checkCondition.setParameter(1,xmlCheckCondition);
        checkCondition.execute();
        String errorNumber  = (String) checkCondition.getOutputParameterValue(2);
        String xmlMsgOut = (String) checkCondition.getOutputParameterValue(3);
        if (xmlMsgOut == null){
            throw new CheckLimitationException();
        }
        return xmlMsgOut;
    }
}
