package afarin.modules.client.repository.client;

import afarin.modules.client.model.client.entity.MobileInfoEntity;
import afarin.modules.client.model.client.id.ClientMobileInfoId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientMobileNoInfoRepository extends JpaRepository<MobileInfoEntity, ClientMobileInfoId> {
    @Query(value = "select a from MobileInfoEntity a where a.mobileNumber =?1 and a.clientIdentifier =?2 and a.status = 'A'")
    List<MobileInfoEntity> checkClientMobile(String mobileNo, String clientId, String service);
}
