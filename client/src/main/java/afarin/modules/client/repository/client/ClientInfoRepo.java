package afarin.modules.client.repository.client;

import afarin.modules.client.model.client.entity.ClientInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientInfoRepo extends JpaRepository<ClientInfoEntity, String> {

    @Query(value = "select clientInfo from ClientInfoEntity clientInfo where clientId=?1 and clientType=?2")
    List<ClientInfoEntity> getClientInfoById(String clientId, String clientType);

    @Query(value = "select a from ClientInfoEntity a where a.clientId=?1 and a.clientType=?2")
    List<ClientInfoEntity> findClientInfoById(String clientId, String clientType);

    @Query(value = "select a from ClientInfoEntity a where a.clientNO=?1")
    ClientInfoEntity findClientIdentity(String clientNo);

    @Query(value="select c from ClientInfoEntity c where c.clientNO=?1")
    ClientInfoEntity findClientByClientNumber(String clientNumber);

    @Query(value = "select c from ClientInfoEntity c where c.clientId =?1")
    List<ClientInfoEntity> findClientByIdNo(String clientId);

}
