package afarin.modules.client.repository.limit;

import afarin.modules.client.exception.ClientNotFoundException;

public interface LimitRepository {

    String checkLimitation(String xmlCheckCondition) throws ClientNotFoundException;
}
