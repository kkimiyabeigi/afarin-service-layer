package afarin.modules.client.repository.client;

import afarin.modules.client.model.client.entity.BankEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BankRepository extends JpaRepository<BankEntity, String> {

    @Query(value = "select b.bicCode from BankEntity b where b.bankCode =?1")
    String getBankBicCode(String bankCode);
}
