package afarin.modules.client.repository.client;

import afarin.modules.client.model.client.entity.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<ClientEntity,String> {

    @Query(value="select client from ClientEntity client where clientNumber=?1")
    ClientEntity findClientByClientNumber(String clientNumber);

    @Query(value = "select a.tranStatus from ClientEntity a where clientNumber=?1")
    String findClientStatus(String clientNumber);
}
