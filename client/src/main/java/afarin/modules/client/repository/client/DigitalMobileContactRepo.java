package afarin.modules.client.repository.client;

import afarin.modules.client.model.client.entity.DigitalMobileContactEntity;
import afarin.modules.client.model.client.id.DigitalMobilesContactId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DigitalMobileContactRepo extends JpaRepository<DigitalMobileContactEntity, DigitalMobilesContactId> {

    @Query(value = "select c from DigitalMobileContactEntity c where c.clientNo=?1 and c.digitalAppService=?2 and c.status='A'")
    DigitalMobileContactEntity findByClientNoAndDigitalAppService(String clientNo, String digitalAppService);
}
