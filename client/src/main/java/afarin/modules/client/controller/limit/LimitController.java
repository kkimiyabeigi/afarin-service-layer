package afarin.modules.client.controller.limit;

import afarin.modules.base.common.utility.AccessController;
import afarin.modules.client.exception.ClientNotFoundException;
import afarin.modules.client.exception.MandatoryFieldNotFound;
import afarin.modules.client.exception.NoRecordFoundException;
import afarin.modules.client.controller.client.ClientController;
import afarin.modules.client.facade.limit.LimitFacade;
import afarin.modules.client.model.limit.dto.InputCheckLimitationDto;
import afarin.modules.client.model.limit.dto.ResponseCheckLimitation;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;
import java.util.Map;

@RestController
@RequestMapping("/api/limit-management/v1")
public class LimitController {

    @Autowired
    LimitFacade limitFacade;

    HttpHeaders httpHeaders = new HttpHeaders();

    Logger logger = LoggerFactory.getLogger(LimitController.class);

    @GetMapping(path = "/limit/check-limitation")
    ResponseEntity<ResponseCheckLimitation> getCheckLimitation(@RequestHeader Map<String, String> headers,
                                                               @RequestParam("clientNo") String clientNo,
                                                               @RequestParam("clientType") String clientType,
                                                               @RequestParam("tranType") String tranType,
                                                               @RequestParam("transactionAmount") String transactionAmount,
                                                               @RequestParam("device") String device,
                                                               Locale locale)
            throws ClientNotFoundException, NoRecordFoundException, MandatoryFieldNotFound, JsonProcessingException {
        logger.info("Incoming message check-limitation:" + headers + " client Number : " + clientNo
                + "client Type" + clientType + "tran Type" + tranType + "transaction Amount" + transactionAmount + "device " + device);
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        InputCheckLimitationDto inputCheckLimitationDto = new InputCheckLimitationDto(clientNo, clientType, tranType, transactionAmount);
        ResponseCheckLimitation resultCheckLimitation = limitFacade.checkLimitation(inputCheckLimitationDto);
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : " + resultCheckLimitation.toString());
        return ResponseEntity.status(HttpStatus.OK).body(resultCheckLimitation);
    }
}
