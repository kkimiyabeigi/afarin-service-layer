package afarin.modules.client.controller.client;

import afarin.modules.base.common.utility.ConsumerInfo;
import afarin.modules.client.exception.ClientNotFoundException;
import afarin.modules.client.exception.MandatoryFieldNotFound;
import afarin.modules.client.exception.NoRecordFoundException;
import afarin.modules.client.facade.client.ClientFacade;
import afarin.modules.client.model.client.dto.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import afarin.modules.base.common.utility.AccessController;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotNull;
import java.util.*;

@RestController
@RequestMapping("/fm/ver1")
public class ClientController {

    private Logger logger = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    ClientFacade clientFacade;

    @Autowired
    RestTemplate restTemplate;

    @GetMapping(path = "/clients/{clientNumber}/owned-accounts")
    private ResponseEntity<List<AccountInfoDto>> getClientsOwnedAccounts(@RequestHeader Map<String, String> headers,
                                                                         @PathVariable("clientNumber") String clientNumber,
                                                                         Locale locale)
            throws ClientNotFoundException, NoRecordFoundException, MandatoryFieldNotFound, JsonProcessingException {

        logger.info("Incoming message Clients-owned-accounts:" + headers + " client number : " + clientNumber);
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        HttpHeaders header = new HttpHeaders();
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            header.add(key, value);
        }
        HttpEntity<String> request = new HttpEntity<>(header);
        String url = "http://RB-ACCOUNT-V1/rb-acct/ver1/client/" + clientNumber + "/owned-accounts";
        ResponseEntity<AccountInfoDto[]> accountEntity = restTemplate.exchange(url, HttpMethod.GET, request, AccountInfoDto[].class);
        AccountInfoDto[] accountArray = accountEntity.getBody();
        HttpStatus httpStatus = accountEntity.getStatusCode();
        List<AccountInfoDto> accountList;
        try {
            accountList = Arrays.asList(accountArray);
            if (accountList.size() == 0) {
                logger.error(clientNumber);
                throw new NoRecordFoundException(clientNumber, null);
            }
        } catch (NullPointerException e) {
            throw new NoRecordFoundException(clientNumber, null);
        }
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : ");
        for (int i = 0; i < accountList.size(); i++)
            logger.info("account " + i + " : " + accountList.get(i).toString());
        return ResponseEntity.status(httpStatus).body(accountList);
    }

    @GetMapping(path = "/customers/{customerId}")
    private ResponseEntity<List<ClientsDto>> getCustomerInfoById(@RequestHeader Map<String, String> headers,
                                                                 @PathVariable("customerId") String clientId,
                                                                 @RequestParam("customerType") String clientType,
                                                                 Locale locale)
            throws ClientNotFoundException, NoRecordFoundException, MandatoryFieldNotFound, JsonProcessingException {

        logger.info("Incoming message Customer-Info-By-Id : " + headers + " customer Id : " + clientId + "customer Type : " + clientType);
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        List<ClientsDto> clientsDtoList = clientFacade.getClientInfoById(clientId, clientType);
        if (clientsDtoList.size() == 0) {
            throw new ClientNotFoundException(clientId);
        }
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : ");
        for (int i = 0; i < clientsDtoList.size(); i++)
            logger.info("client " + i + " : " + clientsDtoList.get(i).toString());
        return ResponseEntity.status(HttpStatus.OK).body(clientsDtoList);
    }


    @GetMapping(path = "clients/{clientNo}")
    private ResponseEntity<ClientIdentityInfoDto> getClientIdentity(@RequestHeader Map<String, String> headers,
                                                                    @PathVariable("clientNo") String clientNo,
                                                                    @RequestParam("filterCriteria") String filterCriteria,
                                                                    Locale locale)
            throws ClientNotFoundException, NoRecordFoundException, MandatoryFieldNotFound, JsonProcessingException {
        logger.info("Incoming message Client-Identity :" + headers + " client number : " + clientNo + "filterCriteria : " + filterCriteria);
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();

        ClientIdentityInfoDto clientIdentityInfoDto = null;
        if (filterCriteria.equals("client-identity")) {
            try {
                clientIdentityInfoDto = clientFacade.getClientIdentity(clientNo);
            } catch (NullPointerException ex) {
                logger.error(clientNo);
                throw new ClientNotFoundException(clientNo);
            }
        }
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : " + clientIdentityInfoDto.toString());
        return ResponseEntity.status(HttpStatus.OK).body(clientIdentityInfoDto);
    }

    // todo
    // remove later
    @GetMapping(path = "/clients/{clientNumber}/shahab-code")
    private ResponseEntity<ClientIdTypeDto> getClientShahabCode(@RequestHeader Map<String, String> headers,
                                                                @PathVariable("clientNumber") String clientNumber,
                                                                Locale locale)
            throws ClientNotFoundException, NoRecordFoundException, MandatoryFieldNotFound, JsonProcessingException {
        logger.info("Incoming message Client-Shahab-Code:" + headers + " client number : " + clientNumber);
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        ClientIdTypeDto clientIdTypeDto = clientFacade.getClientShahabCode(clientNumber);
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : " + clientIdTypeDto.toString());
        return ResponseEntity.status(HttpStatus.OK).body(clientIdTypeDto);
    }

    @GetMapping(path = "/customers/{customerId}/verified-mobile")
    private ResponseEntity<AllowedDto> checkMobileVerification(@RequestHeader Map<String, String> headers,
                                                               @RequestParam("mobileNumber") String mobileNO,
                                                               @PathVariable("customerId") String clientId,
                                                               Locale locale)
            throws NoRecordFoundException, MandatoryFieldNotFound, JsonProcessingException {
        logger.info("Incoming message check-Mobile-Verification:" + headers + " mobile number : " + mobileNO + "customer Id : " + clientId);
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        ObjectMapper mapper = new ObjectMapper();
        ConsumerInfo consumerInfo = mapper.readValue((String) accessController.getHeaderData().get("consumerinfo"), ConsumerInfo.class);
        String result = clientFacade.checkClientMobile(mobileNO, clientId, consumerInfo.getModuleId());
        AllowedDto allowedDto = new AllowedDto(result);
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : " + allowedDto.toString());
        return ResponseEntity.status(HttpStatus.OK).body(allowedDto);
    }

    @GetMapping(path = "/clients/{bankCode}/BankBicCode")
    private ResponseEntity<String> getBankBicCode(@RequestHeader Map<String, String> headers,
                                                  @PathVariable("bankCode") String bankCode) {
        logger.info("Incoming message bank-bic-code:" + headers + " bank code : " + bankCode);
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        String result = clientFacade.getBankBicCode(bankCode);
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : " + result);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @RequestMapping(path = "/fm/ver1/clients/{clientNo}/authorized-mobile", method = RequestMethod.GET)
    private ResponseEntity<String> getClientTokenMoblie(@RequestHeader Map<String, String> headers,
                                                        @PathVariable("clientNo") @NotNull String clientNO,
                                                        @RequestParam("channelId") @NotNull String moduleId,
                                                        Locale locale)
            throws ClientNotFoundException, NoRecordFoundException, MandatoryFieldNotFound, JsonProcessingException {
        logger.info("Incoming message authorized-mobile:" + headers + " client no : " + clientNO + " channel id : " + moduleId);
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        String mobileNo = "";
        mobileNo = clientFacade.getClientMobileInfo(clientNO, moduleId);
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : " + mobileNo);
        return ResponseEntity.status(HttpStatus.OK).body(mobileNo);
    }
}
