package afarin.modules.client.service.limit.impl;

import afarin.modules.client.repository.limit.LimitRepository;
import afarin.modules.client.service.limit.LimitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class LimitServiceImpl implements LimitService {

    @Autowired
    LimitRepository limitRepository;

    @Override
    public String checkLimitation(String inputCheckCondition) {
        return limitRepository.checkLimitation(inputCheckCondition);
    }
}
