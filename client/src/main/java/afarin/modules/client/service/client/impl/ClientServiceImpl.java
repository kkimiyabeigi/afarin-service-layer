package afarin.modules.client.service.client.impl;

import afarin.modules.client.exception.MandatoryFieldNotFound;
import afarin.modules.client.exception.NoRecordFoundException;
import afarin.modules.client.model.client.entity.*;
import afarin.modules.client.repository.client.*;
import afarin.modules.client.repository.client.procedure.CalendarRepository;
import afarin.modules.client.service.client.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class ClientServiceImpl implements ClientService {

    @Autowired
    DigitalMobileContactRepo digitalMobileContactRepo;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    ClientInfoRepo clientInfoRepo;

    @Autowired
    ClientMobileNoInfoRepository clientMobileNoInfoRepository;

    @Autowired
    BankRepository bankRepository;

    @Autowired
    ClientMobileServiceRepository clientMobileServiceRepository;

    @Autowired
    CalendarRepository calendarRepository;

    @Override
    public String getClientMobileInfo(String clientNo, String digitalAppService)  {
        List<String> errors ;
        if (clientNo == null || clientNo.equals("")) {
            errors = Arrays.asList("clientNO: " + clientNo, "mobile NO: " + digitalAppService);

            throw new MandatoryFieldNotFound("clientNO can't be NULL", errors);
        }
        DigitalMobileContactEntity digitalMobileContact = digitalMobileContactRepo.findByClientNoAndDigitalAppService(clientNo, digitalAppService);
        if (digitalMobileContact == null) {
            errors = Arrays.asList("clientNO: " + clientNo, "Module Id: " + digitalAppService);
            throw new NoRecordFoundException("No Record Found", errors);
        }
        return digitalMobileContact.getMobileNo();
    }

    @Override
    public List<ClientEntity> getClients() {
        return clientRepository.findAll();
    }

    @Override
    public ClientInfoEntity getClientByNumber(String clientNumber) {
        return clientInfoRepo.findClientByClientNumber(clientNumber);
    }

    @Override
    public List<ClientInfoEntity> getClientInfos(String clientId, String clientType) {
        return clientInfoRepo.getClientInfoById(clientId, clientType);
    }

    @Override
    public List<ClientInfoEntity> getClientInfoById(String clientId, String clientType) {
//        return clientInfoRepo.findClientInfoById(clientId, clientType);
        List<ClientInfoEntity> clientInfoEntities = clientInfoRepo.findClientInfoById(clientId, clientType);
        return clientInfoEntities;
    }

    @Override
    public List<MobileInfoEntity> checkClientMobileIno(String mobileNo, String clientId, String service) {
        return clientMobileNoInfoRepository.checkClientMobile(mobileNo, clientId, service);
    }

    @Override
    public ClientInfoEntity getClientIdentity(String clientNo) {
        return clientInfoRepo.findClientIdentity(clientNo);
    }


    @Override
    public String getBankBicCode(String bankCode) {
        return bankRepository.getBankBicCode(bankCode);
    }

    @Override
    public String getClientStatus(String clientNo) {
        return clientRepository.findClientStatus(clientNo);
    }

    @Override
    public List<ClientInfoEntity> getClientByIdNo(String clientId) {
        List<ClientInfoEntity> clientInfoEntityList = clientInfoRepo.findClientByIdNo(clientId);
        return clientInfoEntityList;
    }

    @Override
    public MobileServiceInfoEntity checkClientMobileService(String mobileNo, String clientNo, String service) {
        return clientMobileServiceRepository.checkClientMobileService(mobileNo, clientNo, service);
    }

    @Override
    public String getJalaliByDate(Date gregorianDate, String jalaliFormat) {
        return calendarRepository.getJalaliByDate(gregorianDate, jalaliFormat);
    }

    @Override
    public String getJalaliByString(String gregorianDate, String gregFormat, String jalaliFormat) {
        return calendarRepository.getJalaliByString(gregorianDate, gregFormat, jalaliFormat);
    }

    @Override
    public String getGregorianDate(String jalaliDate, String jalaliFormat, String gregFormat) {
        return calendarRepository.getGregorianDate(jalaliDate, jalaliFormat, gregFormat);
    }
}
