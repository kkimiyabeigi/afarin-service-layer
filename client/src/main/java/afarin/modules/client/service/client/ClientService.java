package afarin.modules.client.service.client;

import afarin.modules.client.model.client.entity.ClientEntity;
import afarin.modules.client.model.client.entity.ClientInfoEntity;
import afarin.modules.client.model.client.entity.MobileInfoEntity;
import afarin.modules.client.model.client.entity.MobileServiceInfoEntity;

import java.util.Date;
import java.util.List;

public interface ClientService {

    String getClientMobileInfo(String clientNo, String digitalAppService);

    List<ClientEntity> getClients();

    ClientInfoEntity getClientByNumber(String clientNumber);

    List<ClientInfoEntity> getClientInfos(String clientId, String clientType);

    List<ClientInfoEntity> getClientInfoById(String clientId, String clientType);

    List<MobileInfoEntity> checkClientMobileIno(String mobileNo, String clientId, String service);

    ClientInfoEntity getClientIdentity(String clientNo);

    String getBankBicCode(String bankCode);

    String getClientStatus(String clientNo);

    List<ClientInfoEntity> getClientByIdNo(String clientId);

    MobileServiceInfoEntity checkClientMobileService(String mobileNo, String clientNo, String service);

    String getJalaliByDate(Date gregorianDate, String jalaliFormat);

    String getJalaliByString(String gregorianDate, String gregFormat, String jalaliFormat);

    String getGregorianDate(String jalaliDate, String jalaliFormat, String gregFormat);
}
