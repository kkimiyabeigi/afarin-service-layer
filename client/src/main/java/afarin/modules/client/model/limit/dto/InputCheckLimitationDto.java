package afarin.modules.client.model.limit.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "dailyTotalAmountByIdReq")
public class InputCheckLimitationDto {

    @JacksonXmlProperty(localName = "clientNo")
    private String clientNo;

    @JacksonXmlProperty(localName = "clientType")
    private String clientType;

    @JacksonXmlProperty(localName = "tranType")
    private String tranType;

    @JsonIgnore
    private String transactionAmount;

    public InputCheckLimitationDto() {
    }

    public InputCheckLimitationDto(String clientId, String clientType, String tranType, String transactionAmount) {
        this.clientNo = clientId;
        this.clientType = clientType;
        this.tranType = tranType;
        this.transactionAmount = transactionAmount;
    }

    public String getClientNo() {
        return clientNo;
    }

    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getTranType() {
        return tranType;
    }

    public void setTranType(String tranType) {
        this.tranType = tranType;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }
}
