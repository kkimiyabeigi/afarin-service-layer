package afarin.modules.client.model.client.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "FM_REF_CODE")
public class FieldDescEntity implements Serializable {

    @Id
    @Column(name = "FIELD_VALUE")
    private String fieldValue;

    @Column(name = "MEANING")
    private String meaning;

    public FieldDescEntity() {
    }

    public FieldDescEntity(String fieldvalue, String meaning) {
        this.fieldValue = fieldvalue;
        this.meaning = meaning;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }
}
