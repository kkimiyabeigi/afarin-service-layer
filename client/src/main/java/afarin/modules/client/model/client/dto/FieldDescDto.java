package afarin.modules.client.model.client.dto;

public class FieldDescDto {

    private String fieldValue;

    private String meaning;

    public FieldDescDto() {
    }

    public FieldDescDto(String fieldValue, String meaning) {
        this.fieldValue = fieldValue;
        this.meaning = meaning;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }
}
