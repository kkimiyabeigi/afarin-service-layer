package afarin.modules.client.model.client.dto;

public class AllowedDto {
    private String allowed ;

    public AllowedDto() {
    }

    public AllowedDto(String allowed) {
        this.allowed = allowed;
    }

    public String getAllowed() {
        return allowed;
    }

    public void setAllowed(String allowed) {
        this.allowed = allowed;
    }

    @Override
    public String toString() {
        return "AllowedDto{" +
                "allowed='" + allowed + '\'' +
                '}';
    }
}
