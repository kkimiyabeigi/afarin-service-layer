package afarin.modules.client.model.client.entity;

import afarin.modules.client.model.client.id.ClientMobileInfoId;
import afarin.modules.client.model.client.id.ClientMobileServiceInfoId;

import javax.persistence.*;

@Entity
@Table(name = "FM_CLIENT_MOBILE_SERVICES")
@IdClass(ClientMobileServiceInfoId.class)
public class MobileServiceInfoEntity {

    @Id
    @Column(name = "CLIENT_NO")
    private String clientNumber;

    @Id
    @Column(name = "MOBILE_NO")
    private String mobileNumber;

    @Id
    @Column(name = "SERVICE_NAME")
    private String service;

    @Column(name = "STATUS")
    private String status;

    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
