package afarin.modules.client.model.client.entity;

import afarin.modules.client.model.client.id.DigitalMobilesContactId;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "FM_CLIENT_MOBILE_SERVICES")
@IdClass(DigitalMobilesContactId.class)
public class DigitalMobileContactEntity implements Serializable {

    @Id
    @Column(name = "CLIENT_NO")
    private String clientNo;
    @Id
    @Column(name = "SERVICE_NAME")
    private String digitalAppService;

    @Column(name = "MOBILE_NO")
    private String mobileNo;

    @Column(name = "BRANCH")
    private String branchCOde;

    @Column(name = "USER_ID")
    private String userId;

    @Column(name = "VER_USER")
    private String verUser;

    @Column(name = "VALIDATION_CODE")
    private Long validationCode;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "DEVICE")
    private String device;

   /* @Column(name="INSERTED_CODE")
    private Date insertedCodeDate;
    @Column(name="REQ_DATE")
    private Date reqDate;
    @Column(name="CHECK_CODE_DATE")
    private Date checkCodeDate;
    @Column(name="GEN_CODE_DATE")
    private Date generatedCodeDate;
    @Column(name="VER_DATE")
    private Date verDate;*/

    public DigitalMobileContactEntity() {
    }

    public String getClientNo() {
        return clientNo;
    }

    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }

    public String getDigitalAppService() {
        return digitalAppService;
    }

    public void setDigitalAppService(String digitalAppService) {
        this.digitalAppService = digitalAppService;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getBranchCOde() {
        return branchCOde;
    }

    public void setBranchCOde(String branchCOde) {
        this.branchCOde = branchCOde;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getVerUser() {
        return verUser;
    }

    public void setVerUser(String verUser) {
        this.verUser = verUser;
    }

    public Long getValidationCode() {
        return validationCode;
    }

    public void setValidationCode(Long validationCode) {
        this.validationCode = validationCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }
}
