package afarin.modules.client.model.client.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "FM_CLIENT")
public class ClientEntity implements Serializable {

    @Id
    @Column(name = "CLIENT_NO")
    private String clientNumber;

    @Column(name = "CLIENT_NAME")
    private String clientName;

    @Column(name = "CLIENT_TYPE")
    private int clientType;

    @Column(name = "TRAN_STATUS")
    private String tranStatus;

    public ClientEntity() {
    }

    public ClientEntity(String clientNumber, String clientName, int clientType) {
        this.clientNumber = clientNumber;
        this.clientName = clientName;
        this.clientType = clientType;
    }

    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public int getClientType() {
        return clientType;
    }

    public void setClientType(int clientType) {
        this.clientType = clientType;
    }

    public String getTranStatus() {
        return tranStatus;
    }

    public void setTranStatus(String tranStatus) {
        this.tranStatus = tranStatus;
    }
}
