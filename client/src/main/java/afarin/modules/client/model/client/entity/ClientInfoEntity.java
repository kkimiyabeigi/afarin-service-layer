package afarin.modules.client.model.client.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "FM_SHAHAB")
public class ClientInfoEntity implements Serializable {

    @Id
    @Column(name = "CLIENT_NO")
    private String clientNO;

    @Column(name = "CLIENT_ID")
    private String clientId;

    @Column(name = "CLIENT_TYPE")
    private String clientType;


    @Column(name = "PERSIAN_NAME")
    private String clientName;

    @Column(name = "PERSIAN_SURNAME")
    private String clientLastName;

    @Column(name = "STATUS")
    private String status;

    public ClientInfoEntity() {
    }

    public String getClientNO() {
        return clientNO;
    }

    public void setClientNO(String clientNO) {
        this.clientNO = clientNO;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientLastName() {
        return clientLastName;
    }

    public void setClientLastName(String clientLastName) {
        this.clientLastName = clientLastName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
