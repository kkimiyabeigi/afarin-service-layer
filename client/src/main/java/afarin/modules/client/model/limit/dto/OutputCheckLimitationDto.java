package afarin.modules.client.model.limit.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "dailyTotalAmtByIdResp")
public class OutputCheckLimitationDto {

    @JacksonXmlProperty(localName = "totalTranAmt")
    private String totalWithdrawnAmount;

    public String getTotalWithdrawnAmount() {
        return totalWithdrawnAmount;
    }

    public void setTotalWithdrawnAmount(String totalWithdrawnAmount) {
        this.totalWithdrawnAmount = totalWithdrawnAmount;
    }
}
