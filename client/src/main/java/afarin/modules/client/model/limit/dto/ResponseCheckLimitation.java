package afarin.modules.client.model.limit.dto;

public class ResponseCheckLimitation {

    private boolean allowed;
    private int totalWithdrawnAmount;
    private int remainedAmount;
    private int transactionAmount;
    private int dailyAllowedAmount;

    public boolean isAllowed() {
        return allowed;
    }

    public void setAllowed(boolean allowed) {
        this.allowed = allowed;
    }

    public int getTotalWithdrawnAmount() {
        return totalWithdrawnAmount;
    }

    public void setTotalWithdrawnAmount(int totalWithdrawnAmount) {
        this.totalWithdrawnAmount = totalWithdrawnAmount;
    }

    public int getRemainedAmount() {
        return remainedAmount;
    }

    public void setRemainedAmount(int remainedAmount) {
        this.remainedAmount = remainedAmount;
    }

    public int getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(int transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public int getDailyAllowedAmount() {
        return dailyAllowedAmount;
    }

    public void setDailyAllowedAmount(int dailyAllowedAmount) {
        this.dailyAllowedAmount = dailyAllowedAmount;
    }

    @Override
    public String toString() {
        return "ResponseCheckLimitation{" +
                "allowed=" + allowed +
                ", totalWithdrawnAmount=" + totalWithdrawnAmount +
                ", remainedAmount=" + remainedAmount +
                ", transactionAmount=" + transactionAmount +
                ", dailyAllowedAmount=" + dailyAllowedAmount +
                '}';
    }
}
