package afarin.modules.client.model.client.dto;

public class DigitalMobileContactDto {

    private String clientNo;

    private String digitalAppService;

    private String mobileNo;

    private String branchCOde;

    private String userId;

    private String verUser;

    private Long validationCode;

    private String status;

    private String device;

    public DigitalMobileContactDto() {
    }

    public DigitalMobileContactDto(String clientNo, String digitalAppService, String mobileNo, String branchCOde, String userId, String verUser, Long validationCode, String status, String device) {
        this.clientNo = clientNo;
        this.digitalAppService = digitalAppService;
        this.mobileNo = mobileNo;
        this.branchCOde = branchCOde;
        this.userId = userId;
        this.verUser = verUser;
        this.validationCode = validationCode;
        this.status = status;
        this.device = device;
    }

    public String getClientNo() {
        return clientNo;
    }

    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }

    public String getDigitalAppService() {
        return digitalAppService;
    }

    public void setDigitalAppService(String digitalAppService) {
        this.digitalAppService = digitalAppService;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getBranchCOde() {
        return branchCOde;
    }

    public void setBranchCOde(String branchCOde) {
        this.branchCOde = branchCOde;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getVerUser() {
        return verUser;
    }

    public void setVerUser(String verUser) {
        this.verUser = verUser;
    }

    public Long getValidationCode() {
        return validationCode;
    }

    public void setValidationCode(Long validationCode) {
        this.validationCode = validationCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }
}
