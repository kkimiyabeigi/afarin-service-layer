package afarin.modules.client.model.enums;

public enum ClientTypeDesc {

    INDIVIDUAL_IRANIAN_CLIENT("مشتری حقیقی ایرانی"),
    CORPORATE_IRANIAN_CLIENT("مشتری حقوقی ایرانی"),
    INDIVIDUAL_FOREIGN_CLIENT("مشتری حقیقی خارجی"),
    CORPORATE_FOREIGN_CLIENT("مشتری حقوقی خارجی");

    public final String clientTypeDescCode;

    private ClientTypeDesc(String clientTypeDescCode) {
        this.clientTypeDescCode = clientTypeDescCode;
    }
}
