package afarin.modules.client.model.client.dto;

public class ClientIdentityInfoDto {
    private String clientName;
    private String clientLastName;
    private String clientId;
    private String clientType;
    private String clientTypeDescription;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientLastName() {
        return clientLastName;
    }

    public void setClientLastName(String clientLastName) {
        this.clientLastName = clientLastName;
    }

    public String getClientTypeDescription() {
        return clientTypeDescription;
    }

    public void setClientTypeDescription(String clientTypeDescription) {
        this.clientTypeDescription = clientTypeDescription;
    }

    public ClientIdentityInfoDto() {
    }

    @Override
    public String toString() {
        return "ClientIdentityInfoDto{" +
                "clientName='" + clientName + '\'' +
                ", clientLastName='" + clientLastName + '\'' +
                ", clientId='" + clientId + '\'' +
                ", clientType='" + clientType + '\'' +
                ", clientTypeDescription='" + clientTypeDescription + '\'' +
                '}';
    }
}
