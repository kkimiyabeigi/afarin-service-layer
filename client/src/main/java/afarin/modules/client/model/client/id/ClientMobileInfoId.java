package afarin.modules.client.model.client.id;

import java.io.Serializable;

public class ClientMobileInfoId implements Serializable {
    private String clientNumber;
    private String mobileNumber;

    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
