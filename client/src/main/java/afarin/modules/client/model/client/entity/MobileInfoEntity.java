package afarin.modules.client.model.client.entity;

import afarin.modules.client.model.client.id.ClientMobileInfoId;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "FM_CLIENT_MOBILE_NO_INFO")
@IdClass(ClientMobileInfoId.class)
public class MobileInfoEntity implements Serializable {

    @Id
    @Column(name = "CLIENT_NO")
    private String clientNumber;

    @Id
    @Column(name = "MOBILE_NO")
    private String mobileNumber;

    @Column(name = "CLIENT_ID")
    private String clientIdentifier;

    @Column(name = "STATUS")
    private String status;

    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getClientIdentifier() {
        return clientIdentifier;
    }

    public void setClientIdentifier(String clientIdentifier) {
        this.clientIdentifier = clientIdentifier;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
