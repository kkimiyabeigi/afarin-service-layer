package afarin.modules.client.model.client.id;

import java.io.Serializable;

public class ClientMobileServiceInfoId  implements Serializable {

    private String clientNumber;
    private String mobileNumber;
    private String service;

    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }
}
