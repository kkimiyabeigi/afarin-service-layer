package afarin.modules.client.model.client.dto;

public class ClientIdTypeDto {
    private String shahabCode;
    private String clientType;

    public String getShahabCode() {
        return shahabCode;
    }

    public void setShahabCode(String shahabCode) {
        this.shahabCode = shahabCode;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    @Override
    public String toString() {
        return "ClientIdTypeDto{" +
                "shahabCode='" + shahabCode + '\'' +
                ", clientType='" + clientType + '\'' +
                '}';
    }
}
