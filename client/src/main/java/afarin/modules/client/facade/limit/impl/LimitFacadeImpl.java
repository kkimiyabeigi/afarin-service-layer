package afarin.modules.client.facade.limit.impl;

import afarin.modules.client.exception.CheckLimitationException;
import afarin.modules.client.exception.ClientNotFoundException;
import afarin.modules.client.facade.limit.LimitFacade;
import afarin.modules.client.model.client.entity.ClientInfoEntity;
import afarin.modules.client.model.limit.dto.InputCheckLimitationDto;
import afarin.modules.client.model.limit.dto.OutputCheckLimitationDto;
import afarin.modules.client.model.limit.dto.ResponseCheckLimitation;
import afarin.modules.client.service.client.ClientService;
import afarin.modules.client.service.limit.LimitService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class LimitFacadeImpl implements LimitFacade {

    @Autowired
    LimitService limitService;

    @Autowired
    ClientService clientService;

    @Value("${lm.transactionLimitation}")
    private int transactionLimitation;

    @Override
    public ResponseCheckLimitation checkLimitation(InputCheckLimitationDto inputCheckConditionDto) {
        int dailyAllowedAmount = transactionLimitation;
        ClientInfoEntity clientInfoEntity = clientService.getClientByNumber(inputCheckConditionDto.getClientNo());

        if (clientInfoEntity == null)
            throw new ClientNotFoundException();

        ResponseCheckLimitation responseCheckLimitation = new ResponseCheckLimitation();
        StringBuilder xmlInputCheckLimit = new StringBuilder();
        xmlInputCheckLimit.append("<body>")
                .append(serializeToXML(inputCheckConditionDto))
                .append("</body>");

        String xmlOutputCheckLimit = limitService.checkLimitation(xmlInputCheckLimit.toString());

        if (xmlOutputCheckLimit != null) {
            try {
                OutputCheckLimitationDto outputCheckLimitation = deserializeFromXML(xmlOutputCheckLimit);
                int totalWithdrawnAmount = Integer.parseInt(outputCheckLimitation.getTotalWithdrawnAmount());
                int transactionAmount = Integer.parseInt(inputCheckConditionDto.getTransactionAmount());
                int remainedAmount = dailyAllowedAmount - totalWithdrawnAmount;

                responseCheckLimitation.setDailyAllowedAmount(dailyAllowedAmount);
                responseCheckLimitation.setTotalWithdrawnAmount(totalWithdrawnAmount);
                responseCheckLimitation.setTransactionAmount(transactionAmount);

                if ((dailyAllowedAmount - totalWithdrawnAmount) >= transactionAmount) {
                    responseCheckLimitation.setAllowed(true);
                } else {
                    responseCheckLimitation.setAllowed(false);
                }
                responseCheckLimitation.setRemainedAmount(Math.max(remainedAmount, 0));
            } catch (Exception e) {
                throw new CheckLimitationException();
            }
        }
        return responseCheckLimitation;
    }

    @Override
    public String serializeToXML(InputCheckLimitationDto inputCheckConditionDto) {
        String xmlString = "";
        try {
            XmlMapper xmlMapper = new XmlMapper();
            xmlString = xmlMapper.writeValueAsString(inputCheckConditionDto);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return xmlString;
    }

    @Override
    public OutputCheckLimitationDto deserializeFromXML(String outputXml) {
        OutputCheckLimitationDto outputCheckConditionDto = new OutputCheckLimitationDto();
        try {
            XmlMapper xmlMapper = new XmlMapper();
            outputCheckConditionDto = xmlMapper.readValue(outputXml, OutputCheckLimitationDto.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return outputCheckConditionDto;
    }

}
