package afarin.modules.client.facade.limit;

import afarin.modules.client.model.limit.dto.InputCheckLimitationDto;
import afarin.modules.client.model.limit.dto.OutputCheckLimitationDto;
import afarin.modules.client.model.limit.dto.ResponseCheckLimitation;

public interface LimitFacade {

    ResponseCheckLimitation checkLimitation(InputCheckLimitationDto inputCheckConditionDto);

    String serializeToXML(InputCheckLimitationDto inputCheckConditionDto);

    OutputCheckLimitationDto deserializeFromXML(String outputXml);
}
