package afarin.modules.client.facade.client;

import afarin.modules.client.model.client.dto.*;

import java.util.List;

public interface ClientFacade {
    String getClientMobileInfo(String clientNo, String digitalAppService);

    List<ClientInfoDto> getClientInfos(String clientId, String clientType);

    List<ClientsDto> getClientInfoById(String clientId, String clientType);

    String checkClientMobile(String mobileNumber, String clientId, String moduleId);

    ClientIdentityInfoDto getClientIdentity(String clientNo);

    ClientIdTypeDto getClientShahabCode(String clientNo);

    ClientInfoDto getClientByNumber(String clientNumber);

    String getBankBicCode(String bankCode);

}
