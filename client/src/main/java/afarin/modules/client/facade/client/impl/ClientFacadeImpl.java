package afarin.modules.client.facade.client.impl;

import afarin.modules.client.exception.MandatoryFieldNotFound;
import afarin.modules.client.exception.MobileNumberNotValidException;
import afarin.modules.client.exception.NoRecordFoundException;
import afarin.modules.client.facade.client.ClientFacade;
import afarin.modules.client.model.client.dto.*;
import afarin.modules.client.model.client.entity.ClientInfoEntity;
import afarin.modules.client.model.client.entity.MobileServiceInfoEntity;
import afarin.modules.client.model.enums.ClientTypeDesc;
import afarin.modules.client.service.client.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class ClientFacadeImpl implements ClientFacade {

    @Autowired
    ClientService clientService;

    @Override
    public String getClientMobileInfo(String clientNo, String digitalAppService) {
        List<String> errors = null;
        if (clientNo == null || clientNo.equals("")) {
            errors = Arrays.asList("clientNO: " + clientNo, "mobile NO: " + digitalAppService);
            throw new MandatoryFieldNotFound("clientNO can't be NULL", errors);
        }
        return clientService.getClientMobileInfo(clientNo, digitalAppService);
    }

    @Override
    public List<ClientsDto> getClientInfoById(String clientId, String clientType) {
        List<ClientInfoEntity> clients = clientService.getClientInfoById(clientId, clientType);

        List<ClientsDto> clientsDtoList = new ArrayList<>();
        String clientStatus;
        for(ClientInfoEntity clientInfoEntity : clients){

            clientStatus = clientService.getClientStatus(clientInfoEntity.getClientNO());
            if (clientStatus.equalsIgnoreCase("A") && clientInfoEntity.getStatus().equalsIgnoreCase("1")) {

                ClientsDto clientsDto = new ClientsDto();
                clientsDto.setClientNo(clientInfoEntity.getClientNO());
                clientsDto.setClientName(clientInfoEntity.getClientName());
                clientsDto.setClientLastName(clientInfoEntity.getClientLastName());

                clientsDtoList.add(clientsDto);
            }
        }

        return clientsDtoList;
    }

    @Override
    public ClientIdentityInfoDto getClientIdentity(String clientNo) {
        ClientIdentityInfoDto clientIdentityInfoDto = new ClientIdentityInfoDto();
        ClientInfoEntity clientInfoEntity = clientService.getClientIdentity(clientNo);
        clientIdentityInfoDto.setClientId(clientInfoEntity.getClientId());
        clientIdentityInfoDto.setClientName(clientInfoEntity.getClientName());
        clientIdentityInfoDto.setClientLastName(clientInfoEntity.getClientLastName());
        clientIdentityInfoDto.setClientType(clientInfoEntity.getClientType());

        switch (clientInfoEntity.getClientType()) {
            case "1":
                clientIdentityInfoDto.setClientTypeDescription(ClientTypeDesc.INDIVIDUAL_IRANIAN_CLIENT.clientTypeDescCode);
                break;
            case "2":
                clientIdentityInfoDto.setClientTypeDescription(ClientTypeDesc.CORPORATE_IRANIAN_CLIENT.clientTypeDescCode);
                break;
            case "3":
                clientIdentityInfoDto.setClientTypeDescription(ClientTypeDesc.INDIVIDUAL_FOREIGN_CLIENT.clientTypeDescCode);
                break;
            case "4":
                clientIdentityInfoDto.setClientTypeDescription(ClientTypeDesc.CORPORATE_FOREIGN_CLIENT.clientTypeDescCode);
                break;
        }

        return clientIdentityInfoDto;
    }

    @Override
    public ClientIdTypeDto getClientShahabCode(String clientNo) {
        ClientInfoEntity clientInfoEntity = clientService.getClientIdentity(clientNo);
        ClientIdTypeDto clientIdTypeDto = new ClientIdTypeDto();
        clientIdTypeDto.setClientType(clientInfoEntity.getClientType());
        clientIdTypeDto.setShahabCode(clientInfoEntity.getClientId());

        return clientIdTypeDto;
    }

    @Override
    public List<ClientInfoDto> getClientInfos(String clientId, String clientType) {
        List<ClientInfoEntity> clientInfoEntityList = clientService.getClientInfos(clientId, clientType);
        List<ClientInfoDto> clientInfoDtoList = new ArrayList<>();
        for(ClientInfoEntity clientInfoEntity : clientInfoEntityList){
            ClientInfoDto clientInfoDto = new ClientInfoDto();
            clientInfoDto.setClientId(clientInfoEntity.getClientId());
            clientInfoDto.setClientLastName(clientInfoEntity.getClientLastName());
            clientInfoDto.setClientName(clientInfoEntity.getClientName());
            clientInfoDto.setClientNO(clientInfoEntity.getClientNO());
            clientInfoDto.setClientType(clientInfoEntity.getClientType());
        }
        return clientInfoDtoList;
    }

    @Override
    public ClientInfoDto getClientByNumber(String clientNumber) {
        ClientInfoEntity clientInfoEntity = clientService.getClientByNumber(clientNumber);
        ClientInfoDto clientInfoDto = new ClientInfoDto();
        clientInfoDto.setClientId(clientInfoEntity.getClientId());
        clientInfoDto.setClientName(clientInfoEntity.getClientName());
        clientInfoDto.setClientLastName(clientInfoEntity.getClientLastName());
        clientInfoDto.setClientType(clientInfoEntity.getClientType());
        return clientInfoDto;
    }

    @Override
    public String checkClientMobile(String mobileNumber, String clientId, String moduleId) {

        List<ClientInfoEntity> clientInfoEntityList = new ArrayList<>();
        clientInfoEntityList = clientService.getClientByIdNo(clientId);
        String validator = "^(\\+98|0)?9\\d{9}$";
        if (!mobileNumber.matches(validator)){
            throw new MobileNumberNotValidException(mobileNumber);
        }
        if (clientInfoEntityList.size() != 0) {
            MobileServiceInfoEntity mobileServiceInfoEntity = new MobileServiceInfoEntity();
            for (ClientInfoEntity client : clientInfoEntityList) {
                try {
                    mobileServiceInfoEntity = clientService.checkClientMobileService(mobileNumber, client.getClientNO(), moduleId);
                    if (mobileServiceInfoEntity.getStatus().equalsIgnoreCase("A"))
                        return "True";
                }catch (Exception ex){
//                    ex.printStackTrace();
                }
            }
        }else {
            throw new NoRecordFoundException(clientId);
        }
        return "False";
    }

    @Override
    public String getBankBicCode(String bankCode) {
        return clientService.getBankBicCode(bankCode);
    }
}
