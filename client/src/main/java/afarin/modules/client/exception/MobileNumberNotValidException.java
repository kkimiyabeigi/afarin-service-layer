package afarin.modules.client.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class MobileNumberNotValidException extends GeneralRestException {
    public MobileNumberNotValidException() {
        super(HttpStatus.NOT_ACCEPTABLE);
    }

    public MobileNumberNotValidException(String clientNo) {
        super(HttpStatus.NOT_ACCEPTABLE, clientNo);
    }
}
