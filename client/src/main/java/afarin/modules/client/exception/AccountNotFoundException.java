package afarin.modules.client.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class AccountNotFoundException extends GeneralRestException {

    public AccountNotFoundException() {
        super(HttpStatus.NOT_FOUND);
    }

    public AccountNotFoundException(String accountNo) {
        super(HttpStatus.NOT_FOUND, accountNo);
    }
}
