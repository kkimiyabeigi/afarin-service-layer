package afarin.modules.client.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class CheckLimitationException extends GeneralRestException {

    public CheckLimitationException() {
        super(HttpStatus.NOT_ACCEPTABLE);
    }

    public CheckLimitationException(String clientNo) {
        super(HttpStatus.NOT_ACCEPTABLE, clientNo);
    }
}
