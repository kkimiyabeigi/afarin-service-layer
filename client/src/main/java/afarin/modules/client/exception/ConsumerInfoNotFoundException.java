package afarin.modules.client.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class ConsumerInfoNotFoundException extends GeneralRestException {
    public ConsumerInfoNotFoundException(){
        super(HttpStatus.FORBIDDEN,"UnAuthorized User");
    }

    public ConsumerInfoNotFoundException(String errorDesc) {
        super(HttpStatus.FORBIDDEN, errorDesc);
    }
}
