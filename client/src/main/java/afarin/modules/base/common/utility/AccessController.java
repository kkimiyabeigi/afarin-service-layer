package afarin.modules.base.common.utility;

import afarin.modules.client.exception.ConsumerInfoNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class AccessController {

    private Map<String, String> headerData = new HashMap<String, String>();

    public AccessController() {
    }

    public AccessController(Map<String, String> headers) {
        this.headerData = headers;
    }

    public void checkHeader() {
        ObjectMapper mapper = new ObjectMapper();
        ConsumerInfo consumerInfo = new ConsumerInfo();
        try {
            consumerInfo = mapper.readValue((String) headerData.get("consumerinfo"), ConsumerInfo.class);
            if (!consumerInfo.getApplicationId().equals("EB") || !consumerInfo.getModuleId().equals("MC")) {
                throw new ConsumerInfoNotFoundException();
            }
        } catch (Exception ex) {
            throw new ConsumerInfoNotFoundException();
        }
    }

    public Map<String, String> getHeaderData() {
        return headerData;
    }

    public void setHeaderData(Map<String, String> headerData) {
        this.headerData = headerData;
    }
}
