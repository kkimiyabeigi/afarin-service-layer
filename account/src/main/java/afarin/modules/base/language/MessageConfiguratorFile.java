package afarin.modules.base.language;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;

@Controller
@Profile({"production", "test"})
public class MessageConfiguratorFile implements MessageConfigurator {

    @Bean
    @Override
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.ENGLISH);
        return slr;
    }

    @Bean
    @Override
    public ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource rs = new ResourceBundleMessageSource();
        rs.setBasename("file:messages");
        rs.setUseCodeAsDefaultMessage(true);
        return rs;

    }

    @Bean
    @Override
    public ResourceBundleMessageSource persianMessage() {
        ResourceBundleMessageSource rs = new ResourceBundleMessageSource();
        rs.setBasename("file:persian_error_desc");
        rs.setDefaultEncoding("UTF8");
        rs.setUseCodeAsDefaultMessage(true);
        return rs;

    }


    @Bean
    @Override
    public ResourceBundleMessageSource exceptionMessageSource() {
        ResourceBundleMessageSource rs = new ResourceBundleMessageSource();
        rs.setBasename("file:exception");
        rs.setUseCodeAsDefaultMessage(true);
        return rs;

    }


}
