package afarin.modules.base.language;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class MessageApi {

    @Autowired
    MessageConfigurator messageConfigurator;


    public String getMessageByCode(String errorCode, Locale locale) {
        return messageConfigurator.messageSource().getMessage(errorCode, null, locale);
    }


    public String getErrorCodeByExcpetionName(String exceptionClassName) {
        Locale locale = new Locale("en");
        return messageConfigurator.exceptionMessageSource().getMessage(exceptionClassName, null, locale);
    }


    public String getPersianMessageByCode(String errorCode, Locale locale) {
        return messageConfigurator.persianMessage().getMessage(errorCode, null, locale);
    }
}
