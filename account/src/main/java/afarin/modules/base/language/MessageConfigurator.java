package afarin.modules.base.language;

import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;

public interface MessageConfigurator {
    public LocaleResolver localeResolver();

    public ResourceBundleMessageSource messageSource();

    public ResourceBundleMessageSource persianMessage();

    public ResourceBundleMessageSource exceptionMessageSource();

}
