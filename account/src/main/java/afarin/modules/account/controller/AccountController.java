package afarin.modules.account.controller;

import afarin.modules.account.exception.AccountNotFoundException;
import afarin.modules.account.exception.MandatoryFieldNotFound;
import afarin.modules.account.exception.NoRecordFoundException;
import afarin.modules.account.facade.AccountFacade;
import afarin.modules.account.facade.IbanFacade;
import afarin.modules.account.model.dto.account.*;
import afarin.modules.account.model.dto.iban.RequestIbanInfoDto;
import afarin.modules.account.model.dto.iban.ResponseIbanInfoDto;
import afarin.modules.account.model.dto.iban.ResponseLocalAccIbanDto;
import afarin.modules.base.common.utility.AccessController;
import afarin.modules.base.common.utility.ConsumerInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@RestController
@RequestMapping("/rb-acct/ver1")
public class AccountController {

    @Autowired
    AccountFacade accountFacade;

    @Autowired
    IbanFacade ibanFacade;

    HttpHeaders httpHeaders = new HttpHeaders();

    private Logger logger = LoggerFactory.getLogger(AccountController.class);

    @GetMapping(path = "/client/{clientNumber}/owned-accounts")
    private ResponseEntity<List<AccountInfoDto>> getAllAccountInfo(@RequestHeader Map<String, String> headers,
                                                                   @PathVariable("clientNumber") String clientNo) {
        logger.info("Incoming message Account-Info:" + headers + " client number : " + clientNo);
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        List<AccountInfoDto> accountInfoDtoList = accountFacade.getAllAccountInfo(clientNo);
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : ");
        for (int i = 0; i < accountInfoDtoList.size(); i++)
            logger.info("account " + i + " : " + accountInfoDtoList.get(i).toString());
        return ResponseEntity.status(HttpStatus.OK).body(accountInfoDtoList);
    }

    @GetMapping(path = "/accounts/{accountNumber}/checkCondition")
    private ResponseEntity<AllowedDto> checkWithdrawCondition(@RequestHeader Map<String, String> headers,
                                                              @PathVariable("accountNumber") @NotNull String accountNo,
                                                              @RequestParam("amount") @NotNull Long amount,
                                                              @RequestParam("transactionDate") @NotNull String terminalDate,
                                                              @RequestParam("signedBy") String[] clientList) {
        logger.info("Incoming message check-Withdraw-Condition:" + headers + " account Number : " + accountNo +
                "amount : " + amount + "transaction Date : " + terminalDate + "signed By : " + Arrays.toString(clientList));
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        List<String> clientNoAll = Arrays.asList(clientList);
        Boolean result = accountFacade.checkWithdrawCondition(accountNo, clientNoAll, amount, terminalDate);
        AllowedDto allowedDto;
        if (result) {
            allowedDto = new AllowedDto("True");
        } else {
            allowedDto = new AllowedDto("False");
        }
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : " + allowedDto.toString());
        return ResponseEntity.status(HttpStatus.OK).body(allowedDto);
    }

    @PostMapping(path = "/transactions/local-fund-transfer")
    private ResponseEntity<ResponseLocalFundDto> getLocalFundTransfer(@RequestHeader Map<String, String> headers,
                                                                      @RequestBody RequestLocalFundDto requestLocalFundDto)
            throws NoRecordFoundException, MandatoryFieldNotFound, JsonProcessingException {
        logger.info("Incoming message local-fund-transfer:" + headers + " RequestLocalFund : " + requestLocalFundDto.toString());
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        ObjectMapper mapper = new ObjectMapper();
        ConsumerInfo consumerInfo = mapper.readValue(accessController.getHeaderData().get("consumerinfo"), ConsumerInfo.class);
        requestLocalFundDto.setAcquiringTerminal(consumerInfo.getApplicationId() + consumerInfo.getModuleId());
        requestLocalFundDto.setApplicationID(consumerInfo.getApplicationId());
        requestLocalFundDto.setModuleID(consumerInfo.getModuleId());

        ResponseLocalFundDto responseLocalFundDto = accountFacade.getLocalFoundTransfer(requestLocalFundDto);
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : " + responseLocalFundDto.toString());
        return ResponseEntity.status(HttpStatus.OK).body(responseLocalFundDto);
    }

    //todo
    // find method with annotation for request param
    @GetMapping(path = "/accounts/{accountNumber}")
    private ResponseEntity<Object> getAccountDetails(@RequestHeader Map<String, String> headers,
                                                     @PathVariable("accountNumber") String accountNumber,
                                                     @RequestParam("filterCriteria") String filterCriteria,
                                                     Locale locale) {
        logger.info("Incoming message Account-Details:" + headers + " account Number : " + accountNumber + "filterCriteria : " + filterCriteria);
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();

        AccountBalanceDto accountBalanceDto = null;
        if (filterCriteria.equals("account-balance")) {
            accountBalanceDto = accountFacade.getAccountBalance(accountNumber);
            logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : " + accountBalanceDto.toString());
            return ResponseEntity.status(HttpStatus.OK).body(accountBalanceDto);
        } else if (filterCriteria.equals("account-info")) {
            AccountDto accountDto = new AccountDto();
            try {
                accountDto = accountFacade.getAccountByNumber(accountNumber);
            } catch (NullPointerException ex) {
                throw new AccountNotFoundException();
            }
            logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : " + accountDto.toString());
            return ResponseEntity.status(HttpStatus.OK).body(accountDto);
        }
        throw new NoRecordFoundException("bad parameter", null);

    }

    @GetMapping(path = "/accounts/{accountNumber}/sign-conditions")
    private ResponseEntity<List<ConditionDto>> getSignConditions(@RequestHeader Map<String, String> headers,
                                                                 @PathVariable("accountNumber") String accountNumber,
                                                                 Locale locale) {
        logger.info("Incoming message sign-conditions:" + headers + " account Number : " + accountNumber);
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        List<ConditionDto> conditionDtoList;
        try {
            conditionDtoList = accountFacade.getSignConditions(headers, accountNumber);
        } catch (NullPointerException ex) {
            throw new AccountNotFoundException();
        }
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : ");
        for (int i = 0; i < conditionDtoList.size(); i++)
            logger.info("condition " + i + ": " + conditionDtoList.get(i).toString());
        return ResponseEntity.status(HttpStatus.OK).body(conditionDtoList);
    }

    @GetMapping(path = "/accounts/iban-info")
    private ResponseEntity<ResponseIbanInfoDto> getIbanInfo(@RequestHeader Map<String, String> headers,
                                                            @RequestParam("iban") String iban,
                                                            @RequestParam("paymentId") String paymentId,
                                                            @RequestParam("amount") String amount) {
        logger.info("Incoming message iban-info:" + headers + " iban : " + iban + "payment Id : " + paymentId + "amount : " + amount);
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        RequestIbanInfoDto requestIbanInfoDto = new RequestIbanInfoDto(iban, paymentId, amount, headers);
        ResponseIbanInfoDto responseIbanInfoDto = ibanFacade.centralBankIbanInfo(requestIbanInfoDto);
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : " + responseIbanInfoDto.toString());
        return ResponseEntity.status(HttpStatus.OK).body(responseIbanInfoDto);
    }

    @GetMapping(path = "/accounts/{accountNo}/iban-number")
    private ResponseEntity<ResponseLocalAccIbanDto> getLocalAccountIban(@RequestHeader Map<String, String> headers,
                                                                        @PathVariable("accountNo") String accountNo) {
        logger.info("Incoming message iban-number:" + headers + " account number : " + accountNo);
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        ResponseLocalAccIbanDto responseLocalAccountIbanDto = ibanFacade.getLocalAccountIban(accountNo);
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : " + responseLocalAccountIbanDto.toString());
        return ResponseEntity.status(HttpStatus.OK).body(responseLocalAccountIbanDto);
    }

    @GetMapping(path = "/inquery/local-fund-transfer/{traceId}")
    private ResponseEntity<AccountTransferInqueryDto> getTransferInquery(@RequestHeader Map<String, String> headers,
                                                                         @PathVariable("traceId") String traceId) {
        logger.info("Incoming message local-fund-transfer-Inquery:" + headers + " traceId : " + traceId);
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        AccountTransferInqueryDto localFoundTransferDto = accountFacade.getAccountTransferAudit(traceId);
        logger.info("response is : " + "http status : " + HttpStatus.OK + " Body is : " + localFoundTransferDto.toString());
        return ResponseEntity.status(HttpStatus.OK).body(localFoundTransferDto);
    }

    @GetMapping(path = "/accounts/{accountNumber}/statement")
    private ResponseEntity<List<StmtDto>> getAccountStatement(@RequestHeader Map<String, String> headers,
                                                              @PathVariable("accountNumber") String accountNumber,
                                                              @RequestParam("count") int count,
                                                              @RequestParam("fromDate") String fromDateStr,
                                                              @RequestParam("toDate") String toDateStr,
                                                              Locale locale) {
        logger.info("Incoming message Account-statement:" + headers + " account Number : " + accountNumber
                + "count" + count + "fromDate" + fromDateStr + "toDate" + toDateStr);
        AccessController accessController = new AccessController(headers);
        accessController.checkHeader();
        RequestAccStmt requestAccStmt = new RequestAccStmt(accountNumber, count, fromDateStr, toDateStr, headers);

        ResponseAccStmt responseAccStmt = accountFacade.getAccountStatement(requestAccStmt);
        logger.info("response is : " + "http status : " + responseAccStmt.getHttpStatus() + " Body is : ");
        for (int i = 0; i < responseAccStmt.getStmtDtoList().size(); i++)
            logger.info("statement " + i + " : " + responseAccStmt.getStmtDtoList().get(i).toString());
        return ResponseEntity.status(responseAccStmt.getHttpStatus()).body(responseAccStmt.getStmtDtoList());
    }
}