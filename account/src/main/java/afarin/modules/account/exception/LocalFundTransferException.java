package afarin.modules.account.exception;

public class LocalFundTransferException extends Exception {

    public LocalFundTransferException() {
    }

    public LocalFundTransferException(String message) {
        super(message);
    }
}
