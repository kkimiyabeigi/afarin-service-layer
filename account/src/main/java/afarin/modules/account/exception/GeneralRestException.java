package afarin.modules.account.exception;


import org.springframework.http.HttpStatus;

import java.util.List;

public abstract class GeneralRestException extends RuntimeException {


    // ResourceBundleMessageSource msgSource;

    private String errorCode = "";
    private String errorDesc;
    private List<String> errorDetails;

    private String className;

    private HttpStatus httpStatus = HttpStatus.CONFLICT;

    private String customeErrorCode = "";

    private void setExceptionValues(HttpStatus httpStatus, String errorDesc, List<String> errorDetail) {

        this.httpStatus = httpStatus;
        this.className = this.getClass().getName();

        this.errorDetails = errorDetail;
        this.errorDesc = errorDesc;

        if (!customeErrorCode.equals("")) {
            this.errorCode = customeErrorCode;
        }

    }

    public GeneralRestException() {
    }


    public GeneralRestException(HttpStatus httpStatus) {
        super();

        setExceptionValues(httpStatus, "", null);
    }

    public GeneralRestException(HttpStatus httpStatus, String errorDesc) {
        super(errorDesc);
        setExceptionValues(httpStatus, errorDesc, null);

    }

    public GeneralRestException(HttpStatus httpStatus, String errorDesc, List<String> errorDetail) {
        super(errorDesc);
        setExceptionValues(httpStatus, errorDesc, errorDetail);
    }

    public GeneralRestException(HttpStatus httpStatus, String errorCode, String errorDesc, List<String> errorDetail) {
        super(errorDesc);
        this.customeErrorCode = errorCode;
        setExceptionValues(httpStatus, errorDesc, errorDetail);

    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public List<String> getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(List<String> errorDetails) {
        this.errorDetails = errorDetails;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getCustomeErrorCode() {
        return customeErrorCode;
    }

    public void setCustomeErrorCode(String customeErrorCode) {
        this.customeErrorCode = customeErrorCode;
    }


}
