package afarin.modules.account.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NoRecordFoundException extends GeneralRestException {

    public NoRecordFoundException() {
        super(HttpStatus.NOT_FOUND);
    }

    public NoRecordFoundException(String errorDesc, List<String> errorDetail) {

        super(HttpStatus.NOT_FOUND, errorDesc, errorDetail);
    }


}
