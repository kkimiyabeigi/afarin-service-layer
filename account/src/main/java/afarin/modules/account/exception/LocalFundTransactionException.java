package afarin.modules.account.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class LocalFundTransactionException extends GeneralRestException {

    public LocalFundTransactionException() {
        super(HttpStatus.NOT_ACCEPTABLE);
    }

    public LocalFundTransactionException(String errorDesc, List<String> errorDetails) {

        super(HttpStatus.NOT_ACCEPTABLE, errorDesc, errorDetails);
    }

    public LocalFundTransactionException(String errorCode, String errorDesc, List<String> errorDetails) {
        super(HttpStatus.NOT_ACCEPTABLE, errorCode, errorDesc, errorDetails);

    }
}
