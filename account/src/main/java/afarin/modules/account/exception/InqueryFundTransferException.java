package afarin.modules.account.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InqueryFundTransferException extends GeneralRestException {
    public InqueryFundTransferException() {
        super(HttpStatus.NOT_FOUND);
    }

    public InqueryFundTransferException(String errorDesc, List<String> errorDetail) {

        super(HttpStatus.NOT_FOUND, errorDesc, errorDetail);
    }
}
