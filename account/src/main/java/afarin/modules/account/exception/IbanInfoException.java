package afarin.modules.account.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class IbanInfoException extends GeneralRestException {

    public IbanInfoException() {
        super(HttpStatus.NOT_FOUND);
    }

    public IbanInfoException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }

    public IbanInfoException(String errorDesc, List<String> errorDetail) {
        super(HttpStatus.NOT_FOUND, errorDesc, errorDetail);
    }

    public IbanInfoException(HttpStatus httpStatus, String errorDesc, List<String> errorDetail) {
        super(httpStatus, errorDesc, errorDetail);
    }
}
