package afarin.modules.account.repository.procedure;

import afarin.modules.account.exception.LocalFundTransferException;
import afarin.modules.account.model.dto.account.AccountAvailableBalDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import java.math.BigDecimal;

@Repository
public class AccountInfoRepository {

    @Autowired
    EntityManagerFactory entityManagerFactory;

    private Logger logger = LoggerFactory.getLogger(AccountInfoRepository.class);

    public String getLocalFoundTransfer(String localFundTransferXml) throws LocalFundTransferException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        try {
            StoredProcedureQuery foundTransfer = entityManager
                    .createStoredProcedureQuery("EB_ACCT_INTERFACE.MAIN_PROCESS_DIRECTLY")
                    .registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(2, String.class, ParameterMode.OUT)
                    .registerStoredProcedureParameter(3, String.class, ParameterMode.OUT);

            foundTransfer.setParameter(1, localFundTransferXml);
            foundTransfer.execute();

            String xmlMsgOut= (String) foundTransfer.getOutputParameterValue(2);
            String errorNumber = (String) foundTransfer.getOutputParameterValue(3);

            if (!(errorNumber.equals("000000") || errorNumber.equals("0"))){
                logger.debug(errorNumber);
                throw new LocalFundTransferException(xmlMsgOut);
            }
            return xmlMsgOut;
        }finally {
            entityManager.close();
        }

    }

    public String checkWithdrawCondition(String accountNumber, String clientNoAll, Long amount, String terminalDate) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            StoredProcedureQuery checkCondition =
                    entityManager.createStoredProcedureQuery("CHECK_TRAN_LIMITATION.WITHDRAWAL_CONDITIONS")
                            .registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
                            .registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
                            .registerStoredProcedureParameter(3, Long.class, ParameterMode.IN)
                            .registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
                            .registerStoredProcedureParameter(5, String.class, ParameterMode.OUT);


            checkCondition.setParameter(1, accountNumber);
            checkCondition.setParameter(2, clientNoAll);
            checkCondition.setParameter(3, amount);
            checkCondition.setParameter(4, terminalDate);
            checkCondition.execute();
            String resultCheck = (String) checkCondition.getOutputParameterValue(5);

            return resultCheck;
        }finally {
            entityManager.close();
        }
    }

    public AccountAvailableBalDto getAvailableBalance(String accountNumber){

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            AccountAvailableBalDto accountAvailableBalDto=new AccountAvailableBalDto();
            StoredProcedureQuery availableBalance =
                    entityManager.createStoredProcedureQuery("RBSTOR.CALC_AVAIL_BAL")
                            .registerStoredProcedureParameter(1, String.class,ParameterMode.IN)
                            .registerStoredProcedureParameter(2, BigDecimal.class,ParameterMode.INOUT)
                            .registerStoredProcedureParameter(3, BigDecimal.class,ParameterMode.INOUT)
                            .registerStoredProcedureParameter(4, String.class, ParameterMode.INOUT)
                            .registerStoredProcedureParameter(5, String.class, ParameterMode.INOUT);
            availableBalance.setParameter(1,accountNumber);
            availableBalance.execute();

            accountAvailableBalDto.setAvailBalance((BigDecimal) availableBalance.getOutputParameterValue(2));
            accountAvailableBalDto.setLedgerBalance((BigDecimal) availableBalance.getOutputParameterValue(3));

            return accountAvailableBalDto;
        }finally {
            entityManager.close();
        }

    }
}
