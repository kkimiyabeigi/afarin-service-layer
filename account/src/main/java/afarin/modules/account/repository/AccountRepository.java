package afarin.modules.account.repository;

import afarin.modules.account.model.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<AccountEntity, Long> {

    @Query(value = "select a from AccountEntity a where a.accountStatus='A' ")
    List<AccountEntity> findAllActiveAccount();

    @Query(value = "select a from AccountEntity a where a.accountNumber=?1 ")
    AccountEntity findByAccountNumber(String accountNumber);

    @Query(value = "select a from AccountEntity a where a.accountNumber=?1")
    AccountEntity findAccountInternalKey(String accountNumber);

    @Query(value = "select a from AccountEntity a where a.accountNumber=?1")
    AccountEntity findAccountBalance(String accountNumber);

    @Query( value = "select ai from AccountEntity ai where ai.clientNo=?1 and ai.accountStatus<>'C' and ai.currency='IRR' and ai.depositType<>'T' and ai.internalInd<>'Y' and ai.accountType not in(select a.accountType from AccountTypeLimitEntity a)")
    List<AccountEntity> getAllAccountInfo(String clientNo);

    @Query( value = "select ai from AccountEntity ai where ai.internalKey=?1 and ai.accountStatus<>'C' and ai.currency='IRR' and ai.depositType<>'T'")
    AccountEntity getAllAccountInfoByRole(Long internalKey);

    @Query(value = "select a from AccountEntity a where a.internalKey=?1 and a.clientNo=?2")
    AccountEntity getAccountByClientNo(Long internalKey, String clientNo);


}
