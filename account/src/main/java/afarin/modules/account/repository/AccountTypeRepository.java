package afarin.modules.account.repository;

import afarin.modules.account.model.entity.AccountTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountTypeRepository extends JpaRepository<AccountTypeEntity,String> {

    @Query(value = "select a from AccountTypeEntity a where a.accountType=?1")
    AccountTypeEntity getAccountTypeDesc(String accountType);
}
