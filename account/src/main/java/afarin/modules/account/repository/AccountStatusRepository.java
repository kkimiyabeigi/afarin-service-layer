package afarin.modules.account.repository;

import afarin.modules.account.model.entity.AccountStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountStatusRepository extends JpaRepository<AccountStatusEntity,String> {

    @Query(value = "select a from AccountStatusEntity a where a.accountStatus=?1")
    AccountStatusEntity getAccountStatus(String accountStatus);
}
