package afarin.modules.account.repository;

import afarin.modules.account.model.entity.AccountTransferAuditEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FundTransferInqueryRepository extends JpaRepository<AccountTransferAuditEntity, Long> {

    @Query(value = "select a from AccountTransferAuditEntity a where a.traceId=?1")
    AccountTransferAuditEntity getFundTransferInqury(String traceId);
}
