package afarin.modules.account.repository;

import afarin.modules.account.model.entity.AccountSignerEntity;
import afarin.modules.account.model.id.SignerId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountSignerRepository extends JpaRepository<AccountSignerEntity, SignerId> {

    @Query(value = "select a from AccountSignerEntity a where a.internalKey =?1 and a.conditionSequence = ?2")
    List<AccountSignerEntity> findAccountConditionSigner(Long internalKey, int conditionSequence);

    @Query(value = "select a from AccountSignerEntity a where  a.internalKey=?1 and a.clientNo=?2")
    List<AccountSignerEntity> getAcctInfoMastDetail(Long internalKey, String clientNo);

    @Query(value = "select a from AccountSignerEntity a where a.clientNo=?1")
    List<AccountSignerEntity> getAcctInfoMastDetailByClientNo(String clientNo);
}
