package afarin.modules.account.repository;

import afarin.modules.account.model.entity.DigitalMobileContactEntity;
import afarin.modules.account.model.id.DigitalMobilesContactId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DigitalMobileContactRepo extends JpaRepository<DigitalMobileContactEntity, DigitalMobilesContactId> {

    @Query(value = "select dmc from DigitalMobileContactEntity dmc where dmc.clientNo=?1 and dmc.digitalAppService=?2 and dmc.status='A'")
    DigitalMobileContactEntity findByClientNoAndDigitalAppService(String clientNo, String digitalAppService);

}
