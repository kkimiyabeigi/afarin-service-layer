package afarin.modules.account.repository;

import afarin.modules.account.model.entity.AccountJointEntity;
import afarin.modules.account.model.id.JointId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountJointRepository extends JpaRepository<AccountJointEntity, JointId> {

    @Query(value = "select a from AccountJointEntity a where a.internalKey=?1 and a.clientNo=?2")
    AccountJointEntity getAccountJoint(Long internalKey, String clientNo);

    @Query(value = "select a from AccountJointEntity a where a.clientNo=?1")
    List<AccountJointEntity> getAccountJointInfo(String clientNo);
}
