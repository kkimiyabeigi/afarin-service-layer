package afarin.modules.account.repository;

import afarin.modules.account.model.entity.AccountConditionEntity;
import afarin.modules.account.model.id.ConditionId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountConditionRepository extends JpaRepository<AccountConditionEntity, ConditionId> {

    @Query(value = "select a from AccountConditionEntity a where a.internalKey =?1")
    List<AccountConditionEntity> findAccountCondition(Long internalKey);

    @Query(value = "select a from AccountConditionEntity a where a.internalKey=?1")
    AccountConditionEntity getAccountInfoMast(Long internalKey);
}
