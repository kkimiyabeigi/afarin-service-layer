package afarin.modules.account.repository.procedure;

import afarin.modules.account.exception.NoRecordFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Date;


@Repository
public class CalendarRepository {

    @Autowired
    EntityManagerFactory entityManagerFactory;

    private Logger logger = LoggerFactory.getLogger(CalendarRepository.class);

    public String getJalaliByDate(Date gregorianDate, String jalaliFormat) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        String jalaliDate;
        try {
            jalaliDate = (String) entityManager.createNativeQuery("select TO_CHAR(:gregorianDate,:jalaliFormat,'nls_calendar=persian') from dual")
                    .setParameter("gregorianDate",gregorianDate)
                    .setParameter("jalaliFormat",jalaliFormat)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            throw new NoRecordFoundException();
        }finally {
            entityManager.close();
        }
        return jalaliDate;
    }

    public String getJalaliByString(String gregorianDate, String gregFormat, String jalaliFormat) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        String jalaliDate;
        try {
            jalaliDate = (String) entityManager.createNativeQuery("select TO_CHAR(TO_DATE(:gregorianDate,:gregFormat,'nls_calendar=gregorian'),:jalaliFormat,'nls_calendar=persian') from dual")
                    .setParameter("gregorianDate",gregorianDate)
                    .setParameter("gregFormat",gregFormat)
                    .setParameter("jalaliFormat",jalaliFormat)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            throw new NoRecordFoundException();
        }finally {
            entityManager.close();
        }
        return jalaliDate;
    }

    public String getGregorianDate(String jalaliDate, String jalaliFormat, String gregFormat) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        String gregorianDate;
        try {
            gregorianDate = (String) entityManager.createNativeQuery("select TO_CHAR (TO_DATE (:jalaliDate,:jalaliFormat,'nls_calendar=persian'),:gregFormat,'nls_calendar=gregorian') from dual")
                    .setParameter("jalaliDate",jalaliDate)
                    .setParameter("jalaliFormat",jalaliFormat)
                    .setParameter("gregFormat",gregFormat)
                    .getSingleResult();

        } catch (Exception e) {
            e.printStackTrace();
            throw new NoRecordFoundException();
        }finally {
            entityManager.close();
        }
        return gregorianDate;
    }

}
