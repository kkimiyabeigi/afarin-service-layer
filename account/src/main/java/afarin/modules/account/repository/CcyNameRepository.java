package afarin.modules.account.repository;

import afarin.modules.account.model.entity.CcyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CcyNameRepository extends JpaRepository<CcyEntity,String> {

    @Query(value = "select a from CcyEntity a where a.ccy=?1")
    CcyEntity getCcyName(String ccy);
}
