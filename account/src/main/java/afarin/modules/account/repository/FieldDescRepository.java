package afarin.modules.account.repository;

import afarin.modules.account.model.entity.FieldDescEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FieldDescRepository extends JpaRepository<FieldDescEntity,String> {

   @Query(value = "select a from FieldDescEntity a where DOMAIN='RB_ACCT.OWNERSHIP_TYPE' and a.reflang='FARSI' and a.fieldValue=?1")
   FieldDescEntity getFieldDesc(String fieldvalue);

    @Query(value = "select a from FieldDescEntity a where DOMAIN='RB_ACCT_INFO.SIGNATORY_RELATIONSHIP' and a.reflang='FARSI' and a.fieldValue=?1")
    FieldDescEntity getSignerDesc(String fieldvalue);

    @Query(value = "select a from FieldDescEntity a where DOMAIN='RB_ACCT_TYPE.DEPOSIT_TYPE' and a.reflang='FARSI' and a.fieldValue=?1")
    FieldDescEntity getFieldDescDepositType(String fieldvalue);
}
