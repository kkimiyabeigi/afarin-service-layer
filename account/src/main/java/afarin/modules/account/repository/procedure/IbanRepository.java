package afarin.modules.account.repository.procedure;

import afarin.modules.account.exception.NoRecordFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

@Repository
public class IbanRepository {

    @Autowired
    EntityManagerFactory entityManagerFactory;

    private Logger logger = LoggerFactory.getLogger(IbanRepository.class);

    public String getLocalAccountIban(String accountNo) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        String result;
        try {
            result = (String) entityManager.createNativeQuery(
                    " select RB_IBAN_ACCT.make_local_iban_account(:accountno) from dual ")
                    .setParameter("accountno", accountNo)
                    .getSingleResult();
            if (result.length() != 26) {
                throw new NoRecordFoundException();
            }
        }catch (NullPointerException e){
            throw new NoRecordFoundException();
        }finally {
            entityManager.close();
        }
        return result;
    }
}
