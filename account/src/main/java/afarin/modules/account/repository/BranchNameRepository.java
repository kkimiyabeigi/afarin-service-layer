package afarin.modules.account.repository;

import afarin.modules.account.model.entity.BranchEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BranchNameRepository extends JpaRepository<BranchEntity,String> {

    @Query(value = "select a from BranchEntity a where a.branch=?1")
    BranchEntity getBranchDesc(String branch);


}
