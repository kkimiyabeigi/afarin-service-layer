package afarin.modules.account.facade;

import afarin.modules.account.model.dto.iban.RequestIbanInfoDto;
import afarin.modules.account.model.dto.iban.ResponseIbanInfoDto;
import afarin.modules.account.model.dto.iban.ResponseLocalAccIbanDto;
import afarin.modules.account.model.dto.iban.socket.iban.ReqIbanSocket;
import afarin.modules.account.model.dto.iban.socket.payment.ReqPaymentSocket;

import java.util.Map;

public interface IbanFacade {

    ResponseIbanInfoDto centralBankIbanInfo(RequestIbanInfoDto requestIbanInfoDto);

    ResponseIbanInfoDto getShebaValidate(RequestIbanInfoDto requestIbanInfoDto);

    ResponseIbanInfoDto gtShebaPaymentID(RequestIbanInfoDto requestIbanInfoDto);

    ReqIbanSocket createReqIbanSocket(RequestIbanInfoDto requestIbanInfoDto);

    ReqPaymentSocket createReqPaymentSocket(RequestIbanInfoDto requestIbanInfoDto);

    String getBankBicCode(Map<String, String> headers, String bankCode);

    ResponseLocalAccIbanDto getLocalAccountIban(String accountNo);
}
