package afarin.modules.account.facade;

import afarin.modules.account.model.dto.account.*;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface AccountFacade {

    AccountDto getAccountByNumber(String accountNumber);

    List<AccountInfoDto> getAllAccountInfo(String clientNo);

    String getClientRole(long internalKey, String clientNo);

    Boolean getWithdrawCheck(Long internalKey);

    Boolean checkWithdrawCondition(String accountNo, List<String> clientNoAll, Long amount, String terminalDate);

    ResponseLocalFundDto getLocalFoundTransfer(RequestLocalFundDto requestLocalFundDto);

    AccountBalanceDto getAccountBalance(String accountNumber);

    List<ConditionDto> getSignConditions(Map<String,String> headers,String accountNo);

    ResponseAccStmt getAccountStatement(RequestAccStmt requestAccStmt);

    AccountTransferInqueryDto getAccountTransferAudit(String traceId);

}
