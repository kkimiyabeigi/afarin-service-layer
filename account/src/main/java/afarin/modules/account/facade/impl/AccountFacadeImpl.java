package afarin.modules.account.facade.impl;

import afarin.modules.account.exception.*;
import afarin.modules.account.facade.AccountFacade;
import afarin.modules.account.model.dto.account.*;
import afarin.modules.account.model.dto.account.enums.CrDrDescription;
import afarin.modules.account.model.entity.*;
import afarin.modules.account.repository.BranchNameRepository;
import afarin.modules.account.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
public class AccountFacadeImpl implements AccountFacade {

    @Autowired
    AccountService accountService;

    @Autowired
    BranchNameRepository branchNameRepository;

    @Autowired
    RestTemplate restTemplate;

    private Logger logger = LoggerFactory.getLogger(AccountFacadeImpl.class);

    @Override
    public AccountDto getAccountByNumber(String accountNumber) {
        AccountEntity accountEntity = accountService.getAccountByNumber(accountNumber);
        AccountDto accountDto = new AccountDto();
        accountDto.setAccountDescription(accountEntity.getAccountDescription());
        return accountDto;
    }

    @Override
    public List<AccountInfoDto> getAllAccountInfo(String clientNo) {
        List<AccountInfoDto> accountInfoDtoList = new ArrayList<>();
        try {
            List<AccountEntity> allAccountEntities = accountService.getAllAccountInfo(clientNo);

            List<AccountJointEntity> accountJointEntityList = accountService.getAccountJointInternalKey(clientNo);
            for (AccountJointEntity accountJointEntity : accountJointEntityList) {
                AccountEntity accountEntitiesJoints = accountService.getAllAccountInfoByRole(accountJointEntity.getInternalKey());
                if (!(allAccountEntities.contains(accountEntitiesJoints)) && accountEntitiesJoints != null)
                    allAccountEntities.add(accountEntitiesJoints);
            }

            List<AccountSignerEntity> accountSignerEntities = accountService.getAccountInfoMastDetailByClientNo(clientNo);
            for (AccountSignerEntity accountSignerEntity : accountSignerEntities) {
                AccountEntity accountEntitiesSigners = accountService.getAllAccountInfoByRole(accountSignerEntity.getInternalKey());
                if (!(allAccountEntities.contains(accountEntitiesSigners)) && accountEntitiesSigners != null)
                    allAccountEntities.add(accountEntitiesSigners);
            }

            for (AccountEntity accountEntity : allAccountEntities) {
                AccountInfoDto accountInfoDto = new AccountInfoDto();
                accountInfoDto.setAccountDescription(accountEntity.getAccountDescription());
                accountInfoDto.setAccountNo(accountEntity.getAccountNumber());
                accountInfoDto.setDepositType(accountEntity.getDepositType());
                accountInfoDto.setAccountOwnerShipType(accountEntity.getAccountOwnerShipType());
                accountInfoDto.setCcy(accountEntity.getCurrency());
                accountInfoDto.setAccountStatus(accountEntity.getAccountStatus());
                accountInfoDto.setAccountType(accountEntity.getAccountType());
                accountInfoDto.setBranchCode(accountEntity.getBranch());

                FieldDescEntity fieldDescEntityDepositType = accountService.getDepositTypeDEsc(accountEntity.getDepositType());
                accountInfoDto.setDepositTypeDescription(fieldDescEntityDepositType.getMeaning());

                AccountStatusEntity accountStatusEntity = accountService.getAccountStatus(accountEntity.getAccountStatus());
                accountInfoDto.setAccountStatusDescription(accountStatusEntity.getAccountStatusDesc());

                AccountTypeEntity accountTypeEntity = accountService.getAccountTypeDesc(accountEntity.getAccountType());
                accountInfoDto.setAccountTypeDescription(accountTypeEntity.getAccountTypeDesc());

                BranchEntity branchEntity = accountService.getBranchDesc(accountEntity.getBranch());
                accountInfoDto.setBranchCodeDescription(branchEntity.getBranchName());

                CcyEntity ccyEntity = accountService.getCcyName(accountEntity.getCurrency());
                accountInfoDto.setCcyDescription(ccyEntity.getCcydesc());

                FieldDescEntity fieldDescEntity = accountService.getFieldDesc(accountEntity.getAccountOwnerShipType());
                accountInfoDto.setAccountOwnerShipTypeDescription(fieldDescEntity.getMeaning());

                accountInfoDto.setJointType(getClientRole(accountEntity.getInternalKey(), clientNo));

                accountInfoDto.setAllowed(getWithdrawCheck(accountEntity.getInternalKey()));

                accountInfoDtoList.add(accountInfoDto);

            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug(e.getMessage());
        }
        return accountInfoDtoList;
    }

    @Override
    public String getClientRole(long internalKey, String clientNo) {
        String result = "";
        try {
            AccountJointEntity accountJointEntity = accountService.getAccountJoint(internalKey, clientNo);
            AccountEntity accountEntity = accountService.getAccountByClientNo(internalKey, clientNo);
            if (accountJointEntity != null || accountEntity != null) {
                result = "O";
            }
            List<AccountSignerEntity> accountInfoMastDetailEntity = accountService.getAcctInfoMastDetail(internalKey, clientNo);
            if (!result.equals("O") && accountInfoMastDetailEntity.size() != 0)
                result = "S";
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex.getMessage());
        }
        return result;
    }

    @Override
    public Boolean getWithdrawCheck(Long internalKey) {
        boolean result = false;
        try {
            AccountConditionEntity accountConditionEntity = accountService.getAccountInfoMast(internalKey);
            result = accountConditionEntity != null;
        } catch (NullPointerException ex) {
            ex.printStackTrace();
            logger.debug(ex.getMessage());
        }
        return result;
    }

    @Override
    public Boolean checkWithdrawCondition(String accountNo, List<String> clientNoAll, Long amount, String transactionDate) {
        AccountEntity accountEntity = accountService.getAccountByNumber(accountNo);
        if (accountEntity == null)
            throw new AccountNotFoundException();

        StringBuilder sb = new StringBuilder();
        boolean resultCheck = false;
        if (clientNoAll != null) {
            sb.append("<clientNoAll>");
            for (String row : clientNoAll) {
                sb.append("<clientNo>").append(row).append("</clientNo>");
            }
            sb.append("</clientNoAll>");

            if (amount != null && transactionDate != null) {
                transactionDate = accountService.getGregorianDate(transactionDate, "YYYY/MM/DD", "YYYYMMDD");
                String result = accountService.checkWithdrawCondition(accountNo, sb.toString(), amount, transactionDate);
                if (result.equals("True"))
                    resultCheck = true;
            } else {
                throw new MandatoryFieldNotFound();
            }
        }
        return resultCheck;
    }

    @Override
    public ResponseLocalFundDto getLocalFoundTransfer(RequestLocalFundDto requestLocalFund) {
        ResponseLocalFundDto output = new ResponseLocalFundDto();
        InputDbLocalFundDto input = new InputDbLocalFundDto();

        Boolean resultCheck = checkWithdrawCondition(requestLocalFund.getSourceAccount(), requestLocalFund.getSignedBy()
                , Long.valueOf(requestLocalFund.getAmount()), requestLocalFund.getTransactionDate());
        if (resultCheck) {
            input.setAcquiringTerminal(requestLocalFund.getAcquiringTerminal());
            input.setAmount(requestLocalFund.getAmount());
            input.setApplicationID(requestLocalFund.getApplicationID());
            input.setBranchCode(requestLocalFund.getBranchCode());
            input.setClientNo(requestLocalFund.getClientNo());
            input.setTransactionType(requestLocalFund.getTransactionType());
            input.setDestinationPaymentId(requestLocalFund.getDestinationPaymentId());
            input.setDestinationAccount(requestLocalFund.getDestinationAccount());
            input.setModuleID(requestLocalFund.getModuleID());
            input.setSourceAccount(requestLocalFund.getSourceAccount());
            input.setSourcePaymentId(requestLocalFund.getSourcePaymentId());
            input.setTraceId(requestLocalFund.getTraceId());
            input.setTransactionDate(accountService.getGregorianDate(requestLocalFund.getTransactionDate(), "YYYY/MM/DD", "YYYYMMDD"));
            input.setDestinationAccount(requestLocalFund.getDestinationAccount());
            input.setTransactionDescription(requestLocalFund.getTransactionDescription());
            input.setDestinationTransactionDescription(requestLocalFund.getDestinationTransactionDescription());
            OutputDbLocalFundDto localFoundTransferDto = accountService.getLocalFoundTransfer(input);
            output.setTraceId(localFoundTransferDto.getReference());
            output.setTransactionId(localFoundTransferDto.getSequenceNo());
        } else {
            throw new DontWithdrawConditionException();
        }
        return output;
    }

    @Override
    public AccountBalanceDto getAccountBalance(String accountNumber) {
        AccountEntity accountEntity = accountService.getAccountBalance(accountNumber);
        AccountAvailableBalDto accountAvailableBalDto = accountService.getAvailableBalance(accountNumber);

        if (accountEntity != null) {
            AccountBalanceDto accountBalanceDto = new AccountBalanceDto();
            accountBalanceDto.setActualBalance(accountEntity.getActualBalance());
            accountBalanceDto.setPledgeAmount(accountEntity.getPledgedAmount());
            accountBalanceDto.setLedgerBalance(accountAvailableBalDto.getLedgerBalance());

            if (accountAvailableBalDto.getAvailBalance().intValue() <= 0) {
                accountBalanceDto.setAvailableBalance(new BigDecimal(0));
            }else {
                accountBalanceDto.setAvailableBalance(accountAvailableBalDto.getAvailBalance());
            }

            return accountBalanceDto;

        } else {
            throw new NoRecordFoundException();
        }
    }

    @Override
    public List<ConditionDto> getSignConditions(Map<String,String> headers,String accountNo) {

        Long internalKey = accountService.getAccountInternalKey(accountNo);
        List<AccountConditionEntity> conditionEntityList = accountService.getAccountCondition(internalKey);

        List<ConditionDto> conditionDtoList = new ArrayList<>();
        for (AccountConditionEntity conditionEntity : conditionEntityList) {
            ConditionDto conditionDto = new ConditionDto();

            conditionDto.setAmount(conditionEntity.getAmount());
            conditionDto.setConditionNo(conditionEntity.getConditionSequence());
            conditionDto.setMinOptionalSign(conditionEntity.getMinOptionalSign());

            conditionDto.setExpiryDate(accountService.getJalaliByDate(conditionEntity.getExpiryDate(), "YYYY/MM/DD"));

            List<AccountSignerEntity> signerEntityList = accountService.getAccountConditionSigner(internalKey, conditionDto.getConditionNo());

            HttpHeaders header = new HttpHeaders();
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                header.add(key,value);
            }
            HttpEntity<String> request = new HttpEntity<>(header);

            List<SignerDto> signerDtoList = new ArrayList<>();
            for (AccountSignerEntity signerEntity : signerEntityList) {
                SignerDto signerDto = new SignerDto();
                signerDto.setClientNo(signerEntity.getClientNo());
                signerDto.setRole(signerEntity.getRole());
                signerDto.setMandatory(signerEntity.getMandatory());

                signerDto.setRoleDescription(accountService.getSignerDesc(signerEntity.getRole()).getMeaning());
                try {
                    String url = "http://FM-CLIENT-V1/fm/ver1/clients/" + signerDto.getClientNo() + "/shahab-code";
                    ResponseEntity<ClientIdTypeDto> clientIdTypeDto = restTemplate.exchange(url, HttpMethod.GET, request, ClientIdTypeDto.class);

                    signerDto.setClientId(clientIdTypeDto.getBody().getShahabCode());
                    signerDto.setClientType(clientIdTypeDto.getBody().getClientType());
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.debug(e.getMessage());
                    throw new ClientNotFoundException();
                }
                signerDtoList.add(signerDto);
            }
            conditionDto.setSigner(signerDtoList);

            conditionDtoList.add(conditionDto);
        }

        return conditionDtoList;
    }

    @Override
    public AccountTransferInqueryDto getAccountTransferAudit(String traceId) {
        AccountTransferAuditEntity accountTransferAuditEntity = accountService.getAccountTransferInquery(traceId);
        AccountTransferInqueryDto accountTransferInqueryDto = new AccountTransferInqueryDto();
        try {
            if (accountTransferAuditEntity.getErrorNo().equals("000000") || accountTransferAuditEntity.getErrorNo().equals("0")) {
                accountTransferInqueryDto.setTransactionId(accountTransferAuditEntity.getTransactionId());
            } else {
                throw new InqueryFundTransferException();
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug(e.getMessage());
            throw new InqueryFundTransferException();
        }

        return accountTransferInqueryDto;
    }

    @Override
    public ResponseAccStmt getAccountStatement(RequestAccStmt requestAccStmt) {
        AccountEntity accountEntity = accountService.getAccountByNumber(requestAccStmt.getAccountNumber());
        if (accountEntity == null)
            throw new AccountNotFoundException();

        Long internalKey = accountService.getAccountInternalKey(requestAccStmt.getAccountNumber());

        HttpHeaders header = new HttpHeaders();
        for (Map.Entry<String, String> entry : requestAccStmt.getHeader().entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            header.add(key,value);
        }
        HttpEntity<String> request = new HttpEntity<>(header);

        String url = "http://STMT-V1/stmt/ver1/bill/" + internalKey + "?count=" + requestAccStmt.getCount() + "&fromDate=" + requestAccStmt.getFromDate() + "&toDate=" + requestAccStmt.getToDate();
        ResponseEntity<StmtDto[]> stmtDto = restTemplate.exchange(url, HttpMethod.GET, request, StmtDto[].class);

        StmtDto[] stmtDtoArray = stmtDto.getBody();
        HttpStatus httpStatus = stmtDto.getStatusCode();

        try {
            for (StmtDto stDto : stmtDtoArray) {
                stDto.setBranchCodeDescription(accountService.getBranchDesc(stDto.getBranchCode()).getBranchName());
                switch (stDto.getCreditDebit()) {
                    case "D":
                        stDto.setCreditDebitDescription(CrDrDescription.DR_DESCRIPTION.crDrDescCode);
                        break;
                    case "C":
                        stDto.setCreditDebitDescription(CrDrDescription.CR_DESCRIPTION.crDrDescCode);
                        break;
                }
            }
        } catch (Exception e) {
            logger.debug(e.getMessage());
            throw new NoRecordFoundException();
        }

        List<StmtDto> stmtDtoList = Arrays.asList(stmtDtoArray);
        ResponseAccStmt responseAccStmt = new ResponseAccStmt(stmtDtoList,httpStatus);
        return responseAccStmt;
    }
}
