package afarin.modules.account.facade.impl;

import afarin.modules.account.exception.AccountNotFoundException;
import afarin.modules.account.exception.IbanInfoException;
import afarin.modules.account.exception.NoRecordFoundException;
import afarin.modules.account.facade.IbanFacade;
import afarin.modules.account.model.dto.account.ClientIdTypeDto;
import afarin.modules.account.model.dto.account.SignerDto;
import afarin.modules.account.model.dto.iban.AccountOwner;
import afarin.modules.account.model.dto.iban.RequestIbanInfoDto;
import afarin.modules.account.model.dto.iban.ResponseIbanInfoDto;
import afarin.modules.account.model.dto.iban.ResponseLocalAccIbanDto;
import afarin.modules.account.model.dto.iban.enums.IbanErrorDesc;
import afarin.modules.account.model.dto.iban.enums.IbanStatusDesc;
import afarin.modules.account.model.dto.iban.socket.Content;
import afarin.modules.account.model.dto.iban.socket.iban.ReqIbanMessage;
import afarin.modules.account.model.dto.iban.socket.iban.ReqIbanSocket;
import afarin.modules.account.model.dto.iban.socket.payment.ReqPaymentMessage;
import afarin.modules.account.model.dto.iban.socket.payment.ReqPaymentSocket;
import afarin.modules.account.model.entity.AccountEntity;
import afarin.modules.account.model.entity.AccountSignerEntity;
import afarin.modules.account.service.AccountService;
import afarin.modules.account.service.IbanService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Component
public class IbanFacadeImpl implements IbanFacade {

    private Logger logger = LoggerFactory.getLogger(IbanFacadeImpl.class);

    @Autowired
    IbanService ibanService;

    @Autowired
    AccountService accountService;

    @Autowired
    RestTemplate restTemplate;

    public ResponseIbanInfoDto centralBankIbanInfo(RequestIbanInfoDto requestIbanInfoDto) {
        String bankCode = requestIbanInfoDto.getIban().substring(4, 7);
        if (bankCode.equals("010")) {
            return gtShebaPaymentID(requestIbanInfoDto);
        } else {
            return getShebaValidate(requestIbanInfoDto);
        }
    }

    @Override
    public ResponseIbanInfoDto getShebaValidate(RequestIbanInfoDto requestIbanInfoDto) {

        ResponseIbanInfoDto responseIbanInfoDto = new ResponseIbanInfoDto();
        try {
            Socket socket = new Socket("10.5.18.24", 8081);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream(), "CP1256"));

            ReqIbanSocket reqIbanSocket = createReqIbanSocket(requestIbanInfoDto);
            String xmlString = "";
            try {
                XmlMapper xmlMapper = new XmlMapper();
                xmlString = xmlMapper.writeValueAsString(reqIbanSocket);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            out.println(xmlString);

            String answer = input.readLine();
            List<AccountOwner> nameList = new ArrayList<>();
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            try {
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                Document doc = documentBuilder.parse(new InputSource(new StringReader(answer)));
                doc.getDocumentElement().normalize();
                String requestedIBan = doc.getElementsByTagName("RequestedIBan").item(0).getTextContent();
                String accountComment = doc.getElementsByTagName("AccountComment").item(0).getTextContent();
                String accountNumber = doc.getElementsByTagName("AccountNumber").item(0).getTextContent();
                String accountStatus = doc.getElementsByTagName("AccountStatus").item(0).getTextContent();
                String errorCode = doc.getElementsByTagName("ErrorCode").item(0).getTextContent();
                String paymentCode = doc.getElementsByTagName("PaymentCode").item(0).getTextContent();
                String paymentCodeValid = doc.getElementsByTagName("PaymentCodeValid").item(0).getTextContent();
                NodeList nodeList = doc.getElementsByTagName("AccountOwner");

                responseIbanInfoDto.setStatus(accountStatus);

                switch (accountStatus) {
                    case "02":
                        responseIbanInfoDto.setStatusDescription(IbanStatusDesc.ACTIVE_ACCOUNT.ibanStatusDescCode);
                        break;
                    case "03":
                        responseIbanInfoDto.setStatusDescription(IbanStatusDesc.BLOCKED_ACCOUNT_WITH_DEPOSIT.ibanStatusDescCode);
                        break;
                    case "04":
                        responseIbanInfoDto.setStatusDescription(IbanStatusDesc.BLOCKED_ACCOUNT_WITHOUT_DEPOSIT.ibanStatusDescCode);
                        break;
                    case "05":
                        responseIbanInfoDto.setStatusDescription(IbanStatusDesc.STAGNANT_ACCOUNT.ibanStatusDescCode);
                        break;
                    case "06":
                        responseIbanInfoDto.setStatusDescription(IbanStatusDesc.ERROR_IN_RESPONSE.ibanStatusDescCode);
                        break;
                    case "07":
                        responseIbanInfoDto.setStatusDescription(IbanStatusDesc.OTHER_CASES.ibanStatusDescCode);
                        break;
                }
                if (accountStatus.equals("") || accountStatus.equals("06")) {
                    List<String> errors = new ArrayList<>();
                    errors.add(errorCode);
                    switch (errorCode) {
                        case "00":
                            errors.add(IbanErrorDesc.NO_ERROR.ibanErrorDescCode);
                            break;
                        case "01":
                            errors.add(IbanErrorDesc.INVALID_IBAN.ibanErrorDescCode);
                            throw new IbanInfoException("IBAN Info Exception", errors);
                        case "02":
                            errors.add(IbanErrorDesc.INVALID_REQUEST.ibanErrorDescCode);
                            throw new IbanInfoException(HttpStatus.BAD_REQUEST, "IBAN Info Exception", errors);
                        case "03":
                            errors.add(IbanErrorDesc.MESSAGE_AUTHENTICATION_FAILED.ibanErrorDescCode);
                            throw new IbanInfoException(HttpStatus.NOT_ACCEPTABLE, "IBAN Info Exception", errors);
                        case "04":
                            errors.add(IbanErrorDesc.INVALID_BANK_BIC_CODE.ibanErrorDescCode);
                            throw new IbanInfoException("IBAN Info Exception", errors);
                        case "05":
                            errors.add(IbanErrorDesc.DESTINATION_TIME_OUT.ibanErrorDescCode);
                            throw new IbanInfoException(HttpStatus.REQUEST_TIMEOUT, "IBAN Info Exception", errors);
                        case "06":
                            errors.add(IbanErrorDesc.DESTINATION_NOT_FOUND.ibanErrorDescCode);
                            throw new IbanInfoException("IBAN Info Exception", errors);
                    }
                }

                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node node = nodeList.item(i);
                    NodeList childNodes = node.getChildNodes();
                    String firstName = "";
                    for (int j = 0; j < childNodes.getLength(); j++) {
                        String childName = childNodes.item(j).getNodeName();
                        String childValue = childNodes.item(j).getTextContent();
                        if (childName.equals("FirstName"))
                            firstName = childValue;
                        if (childName.equals("LastName")) {
                            AccountOwner accountOwner = new AccountOwner();
                            accountOwner.setFirstName(firstName);
                            accountOwner.setLastName(childValue);
                            nameList.add(accountOwner);
                            firstName = "";
                        }
                    }
                }
                responseIbanInfoDto.setAccountOwners(nameList);
            } catch (ParserConfigurationException | SAXException ex) {
                ex.printStackTrace();
                logger.error(ex.getMessage());
                throw new NoRecordFoundException();
            }
            input.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            throw new NoRecordFoundException();
        }
        return responseIbanInfoDto;
    }

    @Override
    public ResponseIbanInfoDto gtShebaPaymentID(RequestIbanInfoDto requestIbanInfoDto) {
        ResponseIbanInfoDto responseIbanInfoDto = new ResponseIbanInfoDto();
        try {
            Socket socket = new Socket("10.5.18.24", 4747);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream(), "CP1256"));

            ReqPaymentSocket reqPaymentSocket = createReqPaymentSocket(requestIbanInfoDto);
            String xmlString = "";
            try {
                XmlMapper xmlMapper = new XmlMapper();
                xmlString = xmlMapper.writeValueAsString(reqPaymentSocket);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            out.println(xmlString);
            String answer = input.readLine();

            List<AccountOwner> nameList = new ArrayList<>();
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            try {
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                Document doc = documentBuilder.parse(new InputSource(new StringReader(answer)));
                doc.getDocumentElement().normalize();
                String requestedIBan = doc.getElementsByTagName("RequestedIBan").item(0).getTextContent();
                String accountComment = doc.getElementsByTagName("AccountComment").item(0).getTextContent();
                String accountNumber = doc.getElementsByTagName("AccountNumber").item(0).getTextContent();
                String accountStatus = doc.getElementsByTagName("AccountStatus").item(0).getTextContent();
                String errorCode = doc.getElementsByTagName("ErrorCode").item(0).getTextContent();
                String paymentId = doc.getElementsByTagName("PaymentID").item(0).getTextContent();
                NodeList nodeList = doc.getElementsByTagName("AccountOwners");

                responseIbanInfoDto.setStatus(accountStatus);

                switch (accountStatus) {
                    case "02":
                        responseIbanInfoDto.setStatusDescription(IbanStatusDesc.ACTIVE_ACCOUNT.ibanStatusDescCode);
                        break;
                    case "03":
                        responseIbanInfoDto.setStatusDescription(IbanStatusDesc.BLOCKED_ACCOUNT_WITH_DEPOSIT.ibanStatusDescCode);
                        break;
                    case "04":
                        responseIbanInfoDto.setStatusDescription(IbanStatusDesc.BLOCKED_ACCOUNT_WITHOUT_DEPOSIT.ibanStatusDescCode);
                        break;
                    case "05":
                        responseIbanInfoDto.setStatusDescription(IbanStatusDesc.STAGNANT_ACCOUNT.ibanStatusDescCode);
                        break;
                    case "06":
                        responseIbanInfoDto.setStatusDescription(IbanStatusDesc.ERROR_IN_RESPONSE.ibanStatusDescCode);
                        break;
                    case "07":
                        responseIbanInfoDto.setStatusDescription(IbanStatusDesc.OTHER_CASES.ibanStatusDescCode);
                        break;
                }
                if (accountStatus.equals("") || accountStatus.equals("06")) {
                    List<String> errors = new ArrayList<>();
                    errors.add(errorCode);
                    switch (errorCode) {
                        case "00":
                            errors.add(IbanErrorDesc.NO_ERROR.ibanErrorDescCode);
                            break;
                        case "01":
                            errors.add(IbanErrorDesc.INVALID_IBAN.ibanErrorDescCode);
                            throw new IbanInfoException("IBAN Info Exception", errors);
                        case "02":
                            errors.add(IbanErrorDesc.INVALID_REQUEST.ibanErrorDescCode);
                            throw new IbanInfoException(HttpStatus.BAD_REQUEST, "IBAN Info Exception", errors);
                        case "03":
                            errors.add(IbanErrorDesc.MESSAGE_AUTHENTICATION_FAILED.ibanErrorDescCode);
                            throw new IbanInfoException(HttpStatus.NOT_ACCEPTABLE, "IBAN Info Exception", errors);
                        case "04":
                            errors.add(IbanErrorDesc.INVALID_BANK_BIC_CODE.ibanErrorDescCode);
                            throw new IbanInfoException("IBAN Info Exception", errors);
                        case "05":
                            errors.add(IbanErrorDesc.DESTINATION_TIME_OUT.ibanErrorDescCode);
                            throw new IbanInfoException(HttpStatus.REQUEST_TIMEOUT, "IBAN Info Exception", errors);
                        case "06":
                            errors.add(IbanErrorDesc.DESTINATION_NOT_FOUND.ibanErrorDescCode);
                            throw new IbanInfoException("IBAN Info Exception", errors);
                        case "07":
                            errors.add(IbanErrorDesc.INVALID_PAYMENT_ID.ibanErrorDescCode);
                            throw new IbanInfoException("IBAN Info Exception", errors);
                    }
                }

                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node node = nodeList.item(i);
                    NodeList childNodes = node.getChildNodes();
                    String firstName = "";
                    for (int j = 0; j < childNodes.getLength(); j++) {
                        String childName = childNodes.item(j).getNodeName();
                        String childValue = (childNodes.item(j)).getTextContent();
                        if (childName.equals("FirstName"))
                            firstName = childValue;
                        if (childName.equals("LastName")) {
                            AccountOwner accountOwner = new AccountOwner();
                            accountOwner.setFirstName(firstName);
                            accountOwner.setLastName(childValue);
                            nameList.add(accountOwner);
                            firstName = "";
                        }
                    }
                }
                responseIbanInfoDto.setAccountOwners(nameList);
            } catch (ParserConfigurationException | SAXException ex) {
                ex.printStackTrace();
                logger.error(ex.getMessage());
                throw new NoRecordFoundException();
            }
            input.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            throw new NoRecordFoundException();
        }
        return responseIbanInfoDto;
    }

    @Override
    public String getBankBicCode(Map<String, String> headers, String bankCode) {
        HttpHeaders header = new HttpHeaders();
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            header.add(key, value);
        }
        HttpEntity<String> request = new HttpEntity<>(header);
        String url = "http://FM-CLIENT-V1/fm/ver1/clients/" + bankCode + "/BankBicCode";
        ResponseEntity<String> bankBicCode = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
        return bankBicCode.getBody();
    }

    @Override
    public ReqIbanSocket createReqIbanSocket(RequestIbanInfoDto requestIbanInfoDto) {
        ReqIbanSocket reqIbanSocket = new ReqIbanSocket();
        reqIbanSocket.setCmd("GetShebaValidate");
        reqIbanSocket.setAppname("MobileCorporait");
        reqIbanSocket.setJobid(createJobIdRandom());
        reqIbanSocket.setMsgid(String.valueOf(Math.random()));
        reqIbanSocket.setBranchid("009000");
        reqIbanSocket.setUserid("MC");

        ReqIbanMessage reqIbanMessage = new ReqIbanMessage();
        reqIbanMessage.setIban(requestIbanInfoDto.getIban());
        reqIbanMessage.setOriginalKey(String.valueOf(Math.random()));
        reqIbanMessage.setPaymentCode(requestIbanInfoDto.getPaymentId());
        String bankCode = requestIbanInfoDto.getIban().substring(5, 7);
        reqIbanMessage.setApplicantBicCode(getBankBicCode(requestIbanInfoDto.getHeaders(),bankCode));
        Content<ReqIbanMessage> content = new Content<>();
        content.setMessage(reqIbanMessage);
        reqIbanSocket.setContent(content);

        return reqIbanSocket;
    }

    @Override
    public ReqPaymentSocket createReqPaymentSocket(RequestIbanInfoDto requestIbanInfoDto) {
        ReqPaymentSocket reqPaymentSocket = new ReqPaymentSocket();
        reqPaymentSocket.setCmd("GetshebaPaymentID");
        reqPaymentSocket.setAppname("MobileCorporait");
        reqPaymentSocket.setJobid(createJobIdRandom());
        reqPaymentSocket.setMsgid(String.valueOf(Math.random()));
        reqPaymentSocket.setBranchid("009000");
        reqPaymentSocket.setUserid("MC");

        ReqPaymentMessage reqPaymentMessage = new ReqPaymentMessage();
        reqPaymentMessage.setiBAN(requestIbanInfoDto.getIban());
        reqPaymentMessage.setAmount(requestIbanInfoDto.getAmount());
        reqPaymentMessage.setPaymentId(requestIbanInfoDto.getPaymentId());
        String bankCode = requestIbanInfoDto.getIban().substring(5, 7);
        reqPaymentMessage.setApplicantBicCode(getBankBicCode(requestIbanInfoDto.getHeaders(),bankCode));

        Content<ReqPaymentMessage> content = new Content<>();
        content.setMessage(reqPaymentMessage);
        reqPaymentSocket.setContent(content);

        return reqPaymentSocket;
    }

    public String createJobIdRandom() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        System.out.println(generatedString);
        return generatedString;
    }

    @Override
    public ResponseLocalAccIbanDto getLocalAccountIban(String accountNo) {
        ResponseLocalAccIbanDto responseLocalAccountIbanDto = new ResponseLocalAccIbanDto();
        responseLocalAccountIbanDto.setIban(ibanService.getLocalAccountIban(accountNo));
        try {
            AccountEntity accountEntity = accountService.getAccountByNumber(accountNo);
            responseLocalAccountIbanDto.setAccountDescription(accountEntity.getAccountDescription());

        } catch (NullPointerException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            throw new AccountNotFoundException();
        }
        return responseLocalAccountIbanDto;
    }
}
