package afarin.modules.account.model.dto.account;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName ="response")
public class OutputDbLocalFundDto {

    @JacksonXmlProperty(localName = "result")
    private String result;
    @JacksonXmlProperty(localName = "errno")
    private String errno;
    @JacksonXmlProperty(localName = "hostdatetime")
    private String hostDateTime;
    @JacksonXmlProperty(localName = "Reference")
    private String Reference;
    @JacksonXmlProperty(localName = "dateandtime")
    private String dateAndTime;
    @JacksonXmlProperty(localName = "desc")
    private String desc;
    @JacksonXmlProperty(localName = "fdesc")
    private String fDesc;
    @JacksonXmlProperty(localName = "cmd")
    private String cmd;
    @JacksonXmlProperty(localName = "SequenceNo")
    private String SequenceNo;
    @JacksonXmlProperty(localName = "msgid")
    private String msgId;
    @JacksonXmlProperty(localName = "ApplicationID")
    private String ApplicationID;
    @JacksonXmlProperty(localName = "ModuleID")
    private String ModuleID;
    @JacksonXmlProperty(localName = "jobid")
    private String jobId;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getErrno() {
        return errno;
    }

    public void setErrno(String errno) {
        this.errno = errno;
    }

    public String getHostDateTime() {
        return hostDateTime;
    }

    public void setHostDateTime(String hostDateTime) {
        this.hostDateTime = hostDateTime;
    }

    public String getReference() {
        return Reference;
    }

    public void setReference(String reference) {
        Reference = reference;
    }

    public String getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(String dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getfDesc() {
        return fDesc;
    }

    public void setfDesc(String fDesc) {
        this.fDesc = fDesc;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getSequenceNo() {
        return SequenceNo;
    }

    public void setSequenceNo(String SequenceNo) {
        this.SequenceNo = SequenceNo;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getApplicationID() {
        return ApplicationID;
    }

    public void setApplicationID(String applicationID) {
        ApplicationID = applicationID;
    }

    public String getModuleID() {
        return ModuleID;
    }

    public void setModuleID(String moduleID) {
        ModuleID = moduleID;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }
}
