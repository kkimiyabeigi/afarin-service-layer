package afarin.modules.account.model.id;

import java.io.Serializable;

public class JointId implements Serializable {

    private Long internalKey;

    private String clientNo;

    public Long getInternalKey() {
        return internalKey;
    }

    public void setInternalKey(Long internalKey) {
        this.internalKey = internalKey;
    }

    public String getClientNo() {
        return clientNo;
    }

    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }

}
