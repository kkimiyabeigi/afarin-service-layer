package afarin.modules.account.model.dto.account;

import java.math.BigDecimal;
import java.util.List;

public class ConditionDto {

    private int conditionNo;
    private String expiryDate;
    private BigDecimal amount;
    private int minOptionalSign;
    private List<SignerDto> signer;

    public int getConditionNo() {
        return conditionNo;
    }

    public void setConditionNo(int conditionNo) {
        this.conditionNo = conditionNo;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public List<SignerDto> getSigner() {
        return signer;
    }

    public void setSigner(List<SignerDto> signer) {
        this.signer = signer;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public int getMinOptionalSign() {
        return minOptionalSign;
    }

    public void setMinOptionalSign(int minOptionalSign) {
        this.minOptionalSign = minOptionalSign;
    }

    @Override
    public String toString() {
        return "ConditionDto{" +
                "conditionNo=" + conditionNo +
                ", expiryDate='" + expiryDate + '\'' +
                ", amount=" + amount +
                ", minOptionalSign=" + minOptionalSign +
                ", signer=" + signer.toString() +
                '}';
    }
}
