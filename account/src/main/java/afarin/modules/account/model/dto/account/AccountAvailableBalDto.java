package afarin.modules.account.model.dto.account;

import java.math.BigDecimal;

public class AccountAvailableBalDto {

    private BigDecimal availBalance;
    private BigDecimal ledgerBalance;

    public AccountAvailableBalDto() {
    }

    public AccountAvailableBalDto(BigDecimal availBalance, BigDecimal ledgerBalance) {
        this.availBalance = availBalance;
        this.ledgerBalance = ledgerBalance;
    }

    public BigDecimal getAvailBalance() {
        return availBalance;
    }

    public void setAvailBalance(BigDecimal availBalance) {
        this.availBalance = availBalance;
    }

    public BigDecimal getLedgerBalance() {
        return ledgerBalance;
    }

    public void setLedgerBalance(BigDecimal ledgerBalance) {
        this.ledgerBalance = ledgerBalance;
    }
}
