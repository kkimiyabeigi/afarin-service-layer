package afarin.modules.account.model.id;

import java.io.Serializable;
import java.util.Objects;

public class DigitalMobilesContactId implements Serializable {

    private String clientNo;

    private String digitalAppService;

    public DigitalMobilesContactId() {
    }

    public DigitalMobilesContactId(String clientNo, String digitalAppService) {
        this.clientNo = clientNo;
        this.digitalAppService = digitalAppService;
    }

    public String getClientNo() {
        return clientNo;
    }

    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }

    public String getDigitalAppService() {
        return digitalAppService;
    }

    public void setDigitalAppService(String digitalAppService) {
        this.digitalAppService = digitalAppService;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DigitalMobilesContactId that = (DigitalMobilesContactId) o;
        return clientNo.equals(that.clientNo) &&
                digitalAppService.equals(that.digitalAppService);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientNo, digitalAppService);
    }
}
