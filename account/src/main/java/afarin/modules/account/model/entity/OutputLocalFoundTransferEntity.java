package afarin.modules.account.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class OutputLocalFoundTransferEntity implements Serializable {
    @Id
    @Column(name = "P_ERROR_CODE")
    private String errorNo;

    @Column(name = "P_MSG_OUT")
    private String LocalFoundTransferXml;

    public String getErrorNo() {
        return errorNo;
    }

    public void setErrorNo(String errorNo) {
        this.errorNo = errorNo;
    }

    public String getLocalFoundTransferXml() {
        return LocalFoundTransferXml;
    }

    public void setLocalFoundTransferXml(String localFoundTransferXml) {
        LocalFoundTransferXml = localFoundTransferXml;
    }
}
