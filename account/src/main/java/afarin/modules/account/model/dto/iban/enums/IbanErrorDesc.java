package afarin.modules.account.model.dto.iban.enums;

public enum IbanErrorDesc {

    NO_ERROR("No Error"),
    INVALID_IBAN("Invalid IBAN"),
    INVALID_REQUEST("Invalid Request"),
    MESSAGE_AUTHENTICATION_FAILED("Message Authentication Failed"),
    INVALID_BANK_BIC_CODE("Invalid Bank Bic Code"),
    DESTINATION_TIME_OUT("Destination Time Out"),
    DESTINATION_NOT_FOUND("Destination Not Found"),
    INVALID_PAYMENT_ID("Invalid payment Id");

    public final String ibanErrorDescCode;

    IbanErrorDesc(String ibanErrorDescCode) {
        this.ibanErrorDescCode = ibanErrorDescCode;
    }
}
