package afarin.modules.account.model.dto.iban.socket.payment;

import afarin.modules.account.model.dto.iban.socket.Content;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName ="request")
public class ReqPaymentSocket {

    @JacksonXmlProperty
    private String cmd;

    @JacksonXmlProperty
    private String appname;

    @JacksonXmlProperty
    private String jobid;

    @JacksonXmlProperty
    private String msgid;

    @JacksonXmlProperty
    private String branchid;

    @JacksonXmlProperty
    private String userid;

    @JacksonXmlProperty
    private Content<ReqPaymentMessage> content;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getJobid() {
        return jobid;
    }

    public void setJobid(String jobid) {
        this.jobid = jobid;
    }

    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid;
    }

    public String getBranchid() {
        return branchid;
    }

    public void setBranchid(String branchid) {
        this.branchid = branchid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Content<ReqPaymentMessage> getContent() {
        return content;
    }

    public void setContent(Content<ReqPaymentMessage> content) {
        this.content = content;
    }
}
