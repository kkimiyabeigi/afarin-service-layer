package afarin.modules.account.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="FM_REF_CODE")
public class FieldDescEntity implements Serializable {

    @Id
    @Column(name="FIELD_VALUE")
    private String fieldValue;

    @Column(name="MEANING")
    private String meaning;

    @Column(name="REF_LANG")
    private String reflang;

    public String getReflang() {
        return reflang;
    }

    public void setReflang(String reflang) {
        this.reflang = reflang;
    }

    public FieldDescEntity() {
    }

    public FieldDescEntity(String fieldValue, String meaning) {
        this.fieldValue = fieldValue;
        this.meaning = meaning;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }
}
