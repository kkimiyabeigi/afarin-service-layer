package afarin.modules.account.model.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "FM_BRANCH")
public class BranchEntity {

    @Id
    @Column(name="BRANCH")
    private String branch;

    @Column(name="BRANCH_NAME")
    private String branchName;

    public BranchEntity() {
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }
}
