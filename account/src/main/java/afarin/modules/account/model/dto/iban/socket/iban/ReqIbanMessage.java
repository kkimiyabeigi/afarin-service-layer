package afarin.modules.account.model.dto.iban.socket.iban;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName ="Message01")
public class ReqIbanMessage {

    @JacksonXmlProperty(localName ="ApplicantBicCode")
    private String applicantBicCode;

    @JacksonXmlProperty(localName ="IBAN")
    private String iban;

    @JacksonXmlProperty(localName ="OriginalKey")
    private String originalKey;

    @JacksonXmlProperty(localName ="PaymentCode")
    private String paymentCode;

    public String getApplicantBicCode() {
        return applicantBicCode;
    }

    public void setApplicantBicCode(String applicantBicCode) {
        this.applicantBicCode = applicantBicCode;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getOriginalKey() {
        return originalKey;
    }

    public void setOriginalKey(String originalKey) {
        this.originalKey = originalKey;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }
}
