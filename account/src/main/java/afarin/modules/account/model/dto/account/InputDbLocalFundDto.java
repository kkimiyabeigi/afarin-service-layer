package afarin.modules.account.model.dto.account;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName ="localFoundTransfer")
public class InputDbLocalFundDto {

    @JacksonXmlProperty(localName = "cmd")
    private String transactionType;

    @JacksonXmlProperty(localName = "ApplicationID")
    private String applicationID;

    @JacksonXmlProperty(localName = "ModuleID")
    private String moduleID;

    @JacksonXmlProperty(localName = "Reference")
    private String traceId;

    @JacksonXmlProperty(localName = "branchid")
    private String branchCode;

    @JacksonXmlProperty(localName = "AcquiringTerminal")
    private String acquiringTerminal;

    @JacksonXmlProperty(localName = "Amount")
    private String amount;

    @JacksonXmlProperty(localName = "AccountNumber")
    private String sourceAccount;

    @JacksonXmlProperty(localName = "AccountNumberDest")
    private String destinationAccount;

    @JacksonXmlProperty(localName = "SettlementDate")
    private String transactionDate;

    @JacksonXmlProperty(localName = "PaymentId")
    private String sourcePaymentId;

    @JacksonXmlProperty(localName = "desc")
    private String transactionDescription;

    @JacksonXmlProperty(localName = "DestinationDesc")
    private String destinationTransactionDescription;

    @JacksonXmlProperty(localName = "CrPaymentId")
    private String destinationPaymentId;

    @JacksonXmlProperty(localName = "clientNo")
    private String clientNo;

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getAcquiringTerminal() {
        return acquiringTerminal;
    }

    public void setAcquiringTerminal(String acquiringTerminal) {
        this.acquiringTerminal = acquiringTerminal;
    }

    public String getApplicationID() {
        return applicationID;
    }

    public void setApplicationID(String applicationID) {
        this.applicationID = applicationID;
    }

    public String getModuleID() {
        return moduleID;
    }

    public void setModuleID(String moduleID) {
        this.moduleID = moduleID;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getSourceAccount() {
        return sourceAccount;
    }

    public void setSourceAccount(String sourceAccount) {
        this.sourceAccount = sourceAccount;
    }

    public String getDestinationAccount() {
        return destinationAccount;
    }

    public void setDestinationAccount(String destinationAccount) {
        this.destinationAccount = destinationAccount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransactionDescription() {
        return transactionDescription;
    }

    public void setTransactionDescription(String transactionDescription) {
        this.transactionDescription = transactionDescription;
    }

    public String getDestinationTransactionDescription() {
        return destinationTransactionDescription;
    }

    public void setDestinationTransactionDescription(String destinationTransactionDescription) {
        this.destinationTransactionDescription = destinationTransactionDescription;
    }

    public String getSourcePaymentId() {
        return sourcePaymentId;
    }

    public void setSourcePaymentId(String sourcePaymentId) {
        this.sourcePaymentId = sourcePaymentId;
    }

    public String getDestinationPaymentId() {
        return destinationPaymentId;
    }

    public void setDestinationPaymentId(String destinationPaymentId) {
        this.destinationPaymentId = destinationPaymentId;
    }

    public String getClientNo() {
        return clientNo;
    }

    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
}
