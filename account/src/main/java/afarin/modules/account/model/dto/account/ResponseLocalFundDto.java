package afarin.modules.account.model.dto.account;

public class ResponseLocalFundDto {

    private String traceId;

    private String transactionId;

    public ResponseLocalFundDto(String traceId, String transactionId) {
        this.traceId = traceId;
        this.transactionId = transactionId;
    }

    public ResponseLocalFundDto() {
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public String toString() {
        return "ResponseLocalFundDto{" +
                "traceId='" + traceId + '\'' +
                ", transactionId='" + transactionId + '\'' +
                '}';
    }
}
