package afarin.modules.account.model.entity;


import afarin.modules.account.model.id.ConditionId;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
@Table(name = "RB_ACCT_INFO_MAST")
@IdClass(ConditionId.class)
public class AccountConditionEntity  implements Serializable {

    @Id
    @Column(name = "INTERNAL_KEY")
    private Long internalKey;

    @Id
    @Column(name = "CONDITION_SEQ")
    private int conditionSequence;

    @Column(name = "NOT_MANDATORY_NO")
    private int minOptionalSign;

    @Column(name = "END_DATE")
    private Date expiryDate;

    @Column(name = "LIMIT")
    private BigDecimal amount;

    public AccountConditionEntity() {
    }

    public Long getInternalKey() {
        return internalKey;
    }

    public void setInternalKey(Long internalKey) {
        this.internalKey = internalKey;
    }

    public int getConditionSequence() {
        return conditionSequence;
    }

    public void setConditionSequence(int conditionSequence) {
        this.conditionSequence = conditionSequence;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public int getMinOptionalSign() {
        return minOptionalSign;
    }

    public void setMinOptionalSign(int minOptionalSign) {
        this.minOptionalSign = minOptionalSign;
    }
}
