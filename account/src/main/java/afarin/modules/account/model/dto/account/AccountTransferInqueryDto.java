package afarin.modules.account.model.dto.account;
public class AccountTransferInqueryDto {

    private Long transactionId;

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public String toString() {
        return "AccountTransferInqueryDto{" +
                "transactionId=" + transactionId +
                '}';
    }
}
