package afarin.modules.account.model.dto.iban.enums;

public enum IbanStatusDesc {

    ACTIVE_ACCOUNT("حساب فعال است"),
    BLOCKED_ACCOUNT_WITH_DEPOSIT("حساب مسدود با قابلیت واریز"),
    BLOCKED_ACCOUNT_WITHOUT_DEPOSIT("حساب مسدود بدون قابلیت واریز"),
    STAGNANT_ACCOUNT("حساب راكد است"),
    ERROR_IN_RESPONSE("بروز خطا در پاسخ دهی"),
    OTHER_CASES("سایر موارد");

    public final String ibanStatusDescCode;

    IbanStatusDesc(String ibanStatusDescCode) {
        this.ibanStatusDescCode = ibanStatusDescCode;
    }
}
