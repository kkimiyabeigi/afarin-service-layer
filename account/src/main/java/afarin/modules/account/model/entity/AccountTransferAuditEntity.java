package afarin.modules.account.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EB_INFO_AUDIT")
public class AccountTransferAuditEntity {

    @Id
    @Column(name = "SEQ_NO")
    private Long transactionId;

    @Column(name = "REFERENCE")
    private String traceId;

    @Column(name = "ERROR_NO")
    private String errorNo;

    @Column(name = "STATUS")
    private String status;

    public AccountTransferAuditEntity(Long transactionId, String traceId, String errorNo, String status) {
        this.transactionId = transactionId;
        this.traceId = traceId;
        this.errorNo = errorNo;
        this.status = status;
    }

    public AccountTransferAuditEntity() {
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getErrorNo() {
        return errorNo;
    }

    public void setErrorNo(String errorNo) {
        this.errorNo = errorNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
