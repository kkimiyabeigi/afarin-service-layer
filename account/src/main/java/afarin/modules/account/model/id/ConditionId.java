package afarin.modules.account.model.id;

import java.io.Serializable;
import java.util.Objects;

public class ConditionId implements Serializable {

    private Long internalKey;

    private int conditionSequence;

    public ConditionId() {
    }

    public ConditionId(Long internalKey, int conditionSequence) {
        this.internalKey = internalKey;
        this.conditionSequence = conditionSequence;
    }

    public Long getInternalKey() {
        return internalKey;
    }

    public void setInternalKey(Long internalKey) {
        this.internalKey = internalKey;
    }

    public int getConditionSequence() {
        return conditionSequence;
    }

    public void setConditionSequence(int conditionSequence) {
        this.conditionSequence = conditionSequence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConditionId that = (ConditionId) o;
        return conditionSequence == that.conditionSequence &&
                Objects.equals(internalKey, that.internalKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(internalKey, conditionSequence);
    }
}
