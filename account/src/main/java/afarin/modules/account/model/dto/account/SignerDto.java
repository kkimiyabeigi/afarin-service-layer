package afarin.modules.account.model.dto.account;

public class SignerDto {

    private String clientNo;
    private String role;
    private String roleDescription;
    private String mandatory;
    private String clientId;
    private String clientType;

    public String getClientNo() {
        return clientNo;
    }

    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    @Override
    public String toString() {
        return "SignerDto{" +
                "clientNo='" + clientNo + '\'' +
                ", role='" + role + '\'' +
                ", roleDescription='" + roleDescription + '\'' +
                ", mandatory='" + mandatory + '\'' +
                ", clientId='" + clientId + '\'' +
                ", clientType='" + clientType + '\'' +
                '}';
    }
}
