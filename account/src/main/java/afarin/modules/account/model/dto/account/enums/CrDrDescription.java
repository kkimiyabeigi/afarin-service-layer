package afarin.modules.account.model.dto.account.enums;

public enum CrDrDescription {

    DR_DESCRIPTION("واریز"),
    CR_DESCRIPTION("برداشت");

    public final String crDrDescCode;

    private CrDrDescription(String crDrDescCode) {
        this.crDrDescCode = crDrDescCode;
    }
}
