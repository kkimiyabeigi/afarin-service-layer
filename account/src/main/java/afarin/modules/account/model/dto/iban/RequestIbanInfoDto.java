package afarin.modules.account.model.dto.iban;

import java.util.Map;

public class RequestIbanInfoDto {

    private String iban;
    private String paymentId;
    private String amount;
    private Map<String, String> headers;

    public RequestIbanInfoDto() {
    }

    public RequestIbanInfoDto(String iban, String paymentId, String amount, Map<String, String> headers) {
        this.iban = iban;
        this.paymentId = paymentId;
        this.amount = amount;
        this.headers = headers;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}