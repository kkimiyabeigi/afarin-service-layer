package afarin.modules.account.model.dto.iban;

public class ResponseLocalAccIbanDto {
    private String iban;
    private String accountDescription;

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getAccountDescription() {
        return accountDescription;
    }

    public void setAccountDescription(String accountDescription) {
        this.accountDescription = accountDescription;
    }

    @Override
    public String toString() {
        return "ResponseLocalAccIbanDto{" +
                "iban='" + iban + '\'' +
                ", accountDescription='" + accountDescription + '\'' +
                '}';
    }
}
