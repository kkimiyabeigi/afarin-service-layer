package afarin.modules.account.model.id;

import java.io.Serializable;
import java.util.Objects;

public class SignerId implements Serializable {

    private Long internalKey;
    private String clientNo;
    private int signatoryNo;
    private int conditionSequence;

    public SignerId() {
    }

    public SignerId(Long internalKey, String clientNo, int signatoryNo, int conditionSequence) {
        this.internalKey = internalKey;
        this.clientNo = clientNo;
        this.signatoryNo = signatoryNo;
        this.conditionSequence = conditionSequence;
    }

    public Long getInternalKey() {
        return internalKey;
    }

    public void setInternalKey(Long internalKey) {
        this.internalKey = internalKey;
    }

    public String getClientNo() {
        return clientNo;
    }

    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }

    public int getSignatoryNo() {
        return signatoryNo;
    }

    public void setSignatoryNo(int signatoryNo) {
        this.signatoryNo = signatoryNo;
    }

    public int getConditionSequence() {
        return conditionSequence;
    }

    public void setConditionSequence(int conditionSequence) {
        this.conditionSequence = conditionSequence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SignerId signerId = (SignerId) o;
        return signatoryNo == signerId.signatoryNo &&
                conditionSequence == signerId.conditionSequence &&
                Objects.equals(internalKey, signerId.internalKey) &&
                Objects.equals(clientNo, signerId.clientNo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(internalKey, clientNo, signatoryNo, conditionSequence);
    }
}
