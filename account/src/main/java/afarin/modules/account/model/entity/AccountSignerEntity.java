package afarin.modules.account.model.entity;


import afarin.modules.account.model.id.SignerId;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "RB_ACCT_INFO")
@IdClass(SignerId.class)
public class AccountSignerEntity  implements Serializable {

    @Id
    @Column(name = "INTERNAL_KEY")
    private Long internalKey;

    @Id
    @Column(name = "CLIENT_NO")
    private String clientNo;

    @Id
    @Column(name = "SIGNATORY_NO")
    private int signatoryNo;

    @Id
    @Column(name = "CONDITION_SEQ")
    private int conditionSequence;

    @Column(name = "SIGNATORY_RELATIONSHIP")
    private String role;

    @Column(name = "MANDATORY_IND")
    private String mandatory;

    public Long getInternalKey() {
        return internalKey;
    }

    public void setInternalKey(Long internalKey) {
        this.internalKey = internalKey;
    }

    public String getClientNo() {
        return clientNo;
    }

    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }

    public int getSignatoryNo() {
        return signatoryNo;
    }

    public void setSignatoryNo(int signatoryNo) {
        this.signatoryNo = signatoryNo;
    }

    public int getConditionSequence() {
        return conditionSequence;
    }

    public void setConditionSequence(int conditionSequence) {
        this.conditionSequence = conditionSequence;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }
}
