package afarin.modules.account.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "RB_ACCT_TYPE_EXT_LIMIT")
public class AccountTypeLimitEntity {

    @Id
    @Column(name = "ACCT_TYPE")
    private String accountType;

    @Column(name = "START_DATE")
    private Date startDate;

    public AccountTypeLimitEntity() {
    }

    public AccountTypeLimitEntity(String accountType, Date startDate) {
        this.accountType = accountType;
        this.startDate = startDate;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}
