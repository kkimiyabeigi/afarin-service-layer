package afarin.modules.account.model.dto.account;

public class AccountDto {

    private String accountDescription;

    public String getAccountDescription() {
        return accountDescription;
    }

    public void setAccountDescription(String accountDescription) {
        this.accountDescription = accountDescription;
    }

    @Override
    public String toString() {
        return "AccountDto{" +
                "accountDescription='" + accountDescription + '\'' +
                '}';
    }
}
