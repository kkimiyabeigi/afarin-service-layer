package afarin.modules.account.model.dto.account;

import java.math.BigDecimal;

public class AccountBalanceDto {

    private BigDecimal actualBalance;
    private BigDecimal ledgerBalance;
    private BigDecimal availableBalance;
    private BigDecimal pledgeAmount;

    public AccountBalanceDto() {
    }

    public BigDecimal getActualBalance() {
        return actualBalance;
    }

    public void setActualBalance(BigDecimal actualBalance) {
        this.actualBalance = actualBalance;
    }

    public BigDecimal getLedgerBalance() {
        return ledgerBalance;
    }

    public void setLedgerBalance(BigDecimal ledgerBalance) {
        this.ledgerBalance = ledgerBalance;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public BigDecimal getPledgeAmount() {
        return pledgeAmount;
    }

    public void setPledgeAmount(BigDecimal pledgeAmount) {
        this.pledgeAmount = pledgeAmount;
    }

    @Override
    public String toString() {
        return "AccountBalanceDto{" +
                "actualBalance=" + actualBalance +
                ", ledgerBalance=" + ledgerBalance +
                ", availableBalance=" + availableBalance +
                ", pledgeAmount=" + pledgeAmount +
                '}';
    }
}
