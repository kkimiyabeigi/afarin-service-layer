package afarin.modules.account.model.dto.account;

import java.util.List;

public class RequestLocalFundDto {

    private String transactionType;

    private String acquiringTerminal;

    private String applicationID;

    private String moduleID;

    private String traceId;

    private String transactionDate;

    private String sourceAccount;

    private String destinationAccount;

    private String amount;

    private String sourceTransactionDescription;

    private String transactionDescription;

    private String destinationTransactionDescription;

    private String sourcePaymentId;

    private String destinationPaymentId;



    private String branchCode;

    private String clientNo;

    private List<String> signedBy;

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getAcquiringTerminal() {
        return acquiringTerminal;
    }

    public void setAcquiringTerminal(String acquiringTerminal) {
        this.acquiringTerminal = acquiringTerminal;
    }

    public String getApplicationID() {
        return applicationID;
    }

    public void setApplicationID(String applicationID) {
        this.applicationID = applicationID;
    }

    public String getModuleID() {
        return moduleID;
    }

    public void setModuleID(String moduleID) {
        this.moduleID = moduleID;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getSourceAccount() {
        return sourceAccount;
    }

    public void setSourceAccount(String sourceAccount) {
        this.sourceAccount = sourceAccount;
    }

    public String getDestinationAccount() {
        return destinationAccount;
    }

    public void setDestinationAccount(String destinationAccount) {
        this.destinationAccount = destinationAccount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransactionDescription() {
        return transactionDescription;
    }

    public void setTransactionDescription(String transactionDescription) {
        this.transactionDescription = transactionDescription;
    }

    public String getDestinationTransactionDescription() {
        return destinationTransactionDescription;
    }

    public void setDestinationTransactionDescription(String destinationTransactionDescription) {
        this.destinationTransactionDescription = destinationTransactionDescription;
    }

    public String getSourcePaymentId() {
        return sourcePaymentId;
    }

    public void setSourcePaymentId(String sourcePaymentId) {
        this.sourcePaymentId = sourcePaymentId;
    }


    public String getClientNo() {
        return clientNo;
    }

    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }

    public List<String> getSignedBy() {
        return signedBy;
    }

    public void setSignedBy(List<String> signedBy) {
        this.signedBy = signedBy;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getSourceTransactionDescription() {
        return sourceTransactionDescription;
    }

    public void setSourceTransactionDescription(String sourceTransactionDescription) {
        this.sourceTransactionDescription = sourceTransactionDescription;
    }

    public String getDestinationPaymentId() {
        return destinationPaymentId;
    }

    public void setDestinationPaymentId(String destinationPaymentId) {
        this.destinationPaymentId = destinationPaymentId;
    }

    @Override
    public String toString() {
        return "RequestLocalFoundDto{" +
                "transactionType='" + transactionType + '\'' +
                ", acquiringTerminal='" + acquiringTerminal + '\'' +
                ", applicationID='" + applicationID + '\'' +
                ", moduleID='" + moduleID + '\'' +
                ", traceId='" + traceId + '\'' +
                ", transactionDate='" + transactionDate + '\'' +
                ", sourceAccount='" + sourceAccount + '\'' +
                ", destinationAccount='" + destinationAccount + '\'' +
                ", amount='" + amount + '\'' +
                ", sourceTransactionDescription='" + sourceTransactionDescription + '\'' +
                ", transactionDescription='" + transactionDescription + '\'' +
                ", destinationTransactionDescription='" + destinationTransactionDescription + '\'' +
                ", sourcePaymentId='" + sourcePaymentId + '\'' +
                ", destinationPaymentId='" + destinationPaymentId + '\'' +
                ", branchCode='" + branchCode + '\'' +
                ", clientNo='" + clientNo + '\'' +
                ", signedBy=" + signedBy +
                '}';
    }
}
