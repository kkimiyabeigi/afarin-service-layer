package afarin.modules.account.model.dto.account;

import java.math.BigDecimal;

public class StmtDto {

    private String transactionDate;

    private BigDecimal amount;

    private String systemDate;

    private String creditDebit;

    private BigDecimal previousBalance;

    private BigDecimal actualBalance;

    private String branchCode;

    private String branchCodeDescription ;

    private String transactionDescription;

    private String narrative;

    private String paymentId;

    private String creditDebitDescription;

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getSystemDate() {
        return systemDate;
    }

    public void setSystemDate(String systemDate) {
        this.systemDate = systemDate;
    }

    public String getCreditDebit() {
        return creditDebit;
    }

    public void setCreditDebit(String creditDebit) {
        this.creditDebit = creditDebit;
    }

    public BigDecimal getPreviousBalance() {
        return previousBalance;
    }

    public void setPreviousBalance(BigDecimal previousBalance) {
        this.previousBalance = previousBalance;
    }

    public BigDecimal getActualBalance() {
        return actualBalance;
    }

    public void setActualBalance(BigDecimal actualBalance) {
        this.actualBalance = actualBalance;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBranchCodeDescription() {
        return branchCodeDescription;
    }

    public void setBranchCodeDescription(String branchCodeDescription) {
        this.branchCodeDescription = branchCodeDescription;
    }

    public String getTransactionDescription() {
        return transactionDescription;
    }

    public void setTransactionDescription(String transactionDescription) {
        this.transactionDescription = transactionDescription;
    }

    public String getNarrative() {
        return narrative;
    }

    public void setNarrative(String narrative) {
        this.narrative = narrative;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getCreditDebitDescription() {
        return creditDebitDescription;
    }

    public void setCreditDebitDescription(String creditDebitDescription) {
        this.creditDebitDescription = creditDebitDescription;
    }

    @Override
    public String toString() {
        return "StmtDto{" +
                "transactionDate='" + transactionDate + '\'' +
                ", amount=" + amount +
                ", systemDate='" + systemDate + '\'' +
                ", creditDebit='" + creditDebit + '\'' +
                ", previousBalance=" + previousBalance +
                ", actualBalance=" + actualBalance +
                ", branchCode='" + branchCode + '\'' +
                ", branchCodeDescription='" + branchCodeDescription + '\'' +
                ", transactionDescription='" + transactionDescription + '\'' +
                ", narrative='" + narrative + '\'' +
                ", paymentId='" + paymentId + '\'' +
                ", creditDebitDescription='" + creditDebitDescription + '\'' +
                '}';
    }
}
