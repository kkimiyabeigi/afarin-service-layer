package afarin.modules.account.model.dto.account;

import java.util.Map;

public class RequestAccStmt {

    private String accountNumber;
    private int count;
    private String fromDate;
    private String toDate;
    private Map<String,String> header;

    public RequestAccStmt() {
    }

    public RequestAccStmt(String accountNumber, int count, String fromDate, String toDate, Map<String, String> header) {
        this.accountNumber = accountNumber;
        this.count = count;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.header = header;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public Map<String, String> getHeader() {
        return header;
    }

    public void setHeader(Map<String, String> header) {
        this.header = header;
    }

}
