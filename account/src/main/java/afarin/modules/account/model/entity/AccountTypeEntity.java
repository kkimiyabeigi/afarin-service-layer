package afarin.modules.account.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "RB_ACCT_TYPE")
public class AccountTypeEntity implements Serializable {

    @Id
    @Column(name = "ACCT_TYPE")
    private String accountType;

    @Column(name = "ACCT_TYPE_DESC")
    private String accountTypeDesc;

    public AccountTypeEntity() {
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountTypeDesc() {
        return accountTypeDesc;
    }

    public void setAccountTypeDesc(String accountTypeDesc) {
        this.accountTypeDesc = accountTypeDesc;
    }
}
