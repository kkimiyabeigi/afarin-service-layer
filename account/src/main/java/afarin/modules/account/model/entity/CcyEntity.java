package afarin.modules.account.model.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "FM_CURRENCY")
public class CcyEntity {

   @Id
   @Column(name="CCY")
    private String ccy;

    @Column(name="CCY_DESC")
    private String ccydesc;

    public CcyEntity() {
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getCcydesc() {
        return ccydesc;
    }

    public void setCcydesc(String ccydesc) {
        this.ccydesc = ccydesc;
    }
}
