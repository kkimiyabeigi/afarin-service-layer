package afarin.modules.account.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "RB_ACCT")
public class AccountEntity implements Serializable {

    @Id
    @Column(name = "INTERNAL_KEY")
    private Long internalKey;

    @Column(name = "ACCT_NO")
    private String accountNumber;

    @Column(name = "ACCT_DESC")
    private String accountDescription;

    @Column(name = "BRANCH")
    private String branch;

    @Column(name = "CCY")
    private String currency;

    @Column(name = "ACTUAL_BAL")
    private BigDecimal actualBalance;

    @Column(name = "LEDGER_BAL")
    private BigDecimal ledgerBalance;

    @Column(name = "ACCT_STATUS")
    private String accountStatus;

    @Column(name = "DEPOSIT_TYPE")
    private String depositType;

    @Column(name = "ACCT_TYPE")
    private String accountType;

    @Column(name = "CLIENT_NO")
    private String clientNo;

    @Column(name = "TOTAL_PLEDGED_AMT")
    private BigDecimal pledgedAmount;

    @Column(name = "OWNERSHIP_TYPE")
    private String accountOwnerShipType;

    @Column(name = "INTERNAL_IND")
    private String internalInd;

    public Long getInternalKey() {
        return internalKey;
    }

    public void setInternalKey(Long internalKey) {
        this.internalKey = internalKey;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountDescription() {
        return accountDescription;
    }

    public void setAccountDescription(String accountDescription) {
        this.accountDescription = accountDescription;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getDepositType() {
        return depositType;
    }

    public void setDepositType(String depositType) {
        this.depositType = depositType;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getClientNo() {
        return clientNo;
    }

    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }

    public BigDecimal getPledgedAmount() {
        return pledgedAmount;
    }

    public void setPledgedAmount(BigDecimal pledgedAmount) {
        this.pledgedAmount = pledgedAmount;
    }

    public String getAccountOwnerShipType() {
        return accountOwnerShipType;
    }

    public void setAccountOwnerShipType(String accountOwnerShipType) {
        this.accountOwnerShipType = accountOwnerShipType;
    }

    public BigDecimal getActualBalance() {
        return actualBalance.multiply(new BigDecimal(-1));
    }

    public void setActualBalance(BigDecimal actualBalance) {
        this.actualBalance = actualBalance;
    }

    public BigDecimal getLedgerBalance() {
        return ledgerBalance.multiply(new BigDecimal(-1));
    }

    public void setLedgerBalance(BigDecimal ledgerBalance) {
        this.ledgerBalance = ledgerBalance;
    }

    public String getInternalInd() {
        return internalInd;
    }

    public void setInternalInd(String internalInd) {
        this.internalInd = internalInd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountEntity)) return false;
        AccountEntity that = (AccountEntity) o;
        return internalKey.equals(that.internalKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(internalKey);
    }
}
