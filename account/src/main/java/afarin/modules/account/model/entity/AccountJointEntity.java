package afarin.modules.account.model.entity;

import afarin.modules.account.model.id.JointId;

import javax.persistence.*;

@Entity
@Table(name = "RB_JOINT_ACCT")
@IdClass(JointId.class)
public class AccountJointEntity {

    @Id
    @Column(name="INTERNAL_KEY")
    private Long internalKey;

    @Id
    @Column(name="CLIENT_NO")
    private String clientNo;

    @Column(name="CLIENT_SHORT")
    private String clientShort;

    @Column(name="PERCENTAGE")
    private String percntage;

    public AccountJointEntity(Long internalKey, String clientNo, String clientShort, String percntage) {
        this.internalKey = internalKey;
        this.clientNo = clientNo;
        this.clientShort = clientShort;
        this.percntage = percntage;
    }

    public AccountJointEntity() {
    }

    public Long getInternalKey() {
        return internalKey;
    }

    public void setInternalKey(Long internalKey) {
        this.internalKey = internalKey;
    }

    public String getClientNo() {
        return clientNo;
    }

    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }

    public String getClientShort() {
        return clientShort;
    }

    public void setClientShort(String clientShort) {
        this.clientShort = clientShort;
    }

    public String getPercntage() {
        return percntage;
    }

    public void setPercntage(String percntage) {
        this.percntage = percntage;
    }
}
