package afarin.modules.account.model.dto.iban.socket.payment;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName ="Message01")
public class ReqPaymentMessage {

    @JacksonXmlProperty(localName ="ApplicantBicCode")
    private String applicantBicCode;

    @JacksonXmlProperty(localName ="IBAN")
    private String iBAN;

    @JacksonXmlProperty(localName ="PaymentID")
    private String paymentId;

    @JacksonXmlProperty(localName ="Amount")
    private String amount;

    public String getApplicantBicCode() {
        return applicantBicCode;
    }

    public void setApplicantBicCode(String applicantBicCode) {
        this.applicantBicCode = applicantBicCode;
    }

    public String getiBAN() {
        return iBAN;
    }

    public void setiBAN(String iBAN) {
        this.iBAN = iBAN;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
