package afarin.modules.account.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "GL_ACCT_STATUS")
public class AccountStatusEntity implements Serializable {

    @Id
    @Column(name = "ACCT_STATUS")
    private String accountStatus;

    @Column(name = "ACCT_STATUS_DESC")
    private String accountStatusDesc;


    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getAccountStatusDesc() {
        return accountStatusDesc;
    }

    public void setAccountStatusDesc(String accountStatusDesc) {
        this.accountStatusDesc = accountStatusDesc;
    }
}
