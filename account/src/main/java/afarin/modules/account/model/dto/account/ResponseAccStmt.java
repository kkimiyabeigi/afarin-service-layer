package afarin.modules.account.model.dto.account;

import org.springframework.http.HttpStatus;

import java.util.List;

public class ResponseAccStmt {

    private List<StmtDto> stmtDtoList;
    private HttpStatus httpStatus;

    public ResponseAccStmt() {
    }

    public ResponseAccStmt(List<StmtDto> stmtDtoList, HttpStatus httpStatus) {
        this.stmtDtoList = stmtDtoList;
        this.httpStatus = httpStatus;
    }

    public List<StmtDto> getStmtDtoList() {
        return stmtDtoList;
    }

    public void setStmtDtoList(List<StmtDto> stmtDtoList) {
        this.stmtDtoList = stmtDtoList;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
