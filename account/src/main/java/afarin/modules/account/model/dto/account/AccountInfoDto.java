package afarin.modules.account.model.dto.account;

public class AccountInfoDto {

    private String accountNo;

    private String ccy;

    private String depositType;

    private String accountStatusDescription;

    private String accountStatus;

    private String accountType;

    private String accountTypeDescription;

    private String branchCode;

    private String branchCodeDescription;

    private String ccyDescription;

    private String accountOwnerShipTypeDescription;

    private String accountDescription;

    private String accountOwnerShipType;

    private  String jointType;

    private Boolean allowed;

    private String depositTypeDescription;

    public Boolean getAllowed() {
        return allowed;
    }

    public void setAllowed(Boolean allowed) {
        this.allowed = allowed;
    }

    public String getJointType() {
        return jointType;
    }

    public void setJointType(String jointType) {
        this.jointType = jointType;
    }

    public String getAccountOwnerShipType() {
        return accountOwnerShipType;
    }

    public void setAccountOwnerShipType(String accountOwnerShipType) {
        this.accountOwnerShipType = accountOwnerShipType;
    }

    public String getAccountOwnerShipTypeDescription() {
        return accountOwnerShipTypeDescription;
    }

    public void setAccountOwnerShipTypeDescription(String accountOwnerShipTypeDescription) {
        this.accountOwnerShipTypeDescription = accountOwnerShipTypeDescription;
    }

    public String getCcyDescription() {
        return ccyDescription;
    }

    public void setCcyDescription(String ccyDescription) {
        this.ccyDescription = ccyDescription;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBranchCodeDescription() {
        return branchCodeDescription;
    }

    public void setBranchCodeDescription(String branchCodeDescription) {
        this.branchCodeDescription = branchCodeDescription;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getAccountDescription() {
        return accountDescription;
    }

    public void setAccountDescription(String accountDescription) {
        this.accountDescription = accountDescription;
    }

    public String getAccountStatusDescription() {
        return accountStatusDescription;
    }

    public void setAccountStatusDescription(String accountStatusDescription) {
        this.accountStatusDescription = accountStatusDescription;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getAccountTypeDescription() {
        return accountTypeDescription;
    }

    public void setAccountTypeDescription(String accountTypeDescription) {
        this.accountTypeDescription = accountTypeDescription;
    }

    public String getDepositType() {
        return depositType;
    }

    public void setDepositType(String depositType) {
        this.depositType = depositType;
    }

    public String getDepositTypeDescription() {
        return depositTypeDescription;
    }

    public void setDepositTypeDescription(String depositTypeDescription) {
        this.depositTypeDescription = depositTypeDescription;
    }

    @Override
    public String toString() {
        return "AccountInfoDto{" +
                "accountNo='" + accountNo + '\'' +
                ", ccy='" + ccy + '\'' +
                ", depositType='" + depositType + '\'' +
                ", accountStatusDescription='" + accountStatusDescription + '\'' +
                ", accountStatus='" + accountStatus + '\'' +
                ", accountType='" + accountType + '\'' +
                ", accountTypeDescription='" + accountTypeDescription + '\'' +
                ", branchCode='" + branchCode + '\'' +
                ", branchCodeDescription='" + branchCodeDescription + '\'' +
                ", ccyDescription='" + ccyDescription + '\'' +
                ", accountOwnerShipTypeDescription='" + accountOwnerShipTypeDescription + '\'' +
                ", accountDescription='" + accountDescription + '\'' +
                ", accountOwnerShipType='" + accountOwnerShipType + '\'' +
                ", jointType='" + jointType + '\'' +
                ", allowed=" + allowed +
                ", depositTypeDescription='" + depositTypeDescription + '\'' +
                '}';
    }
}

