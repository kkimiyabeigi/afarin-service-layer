package afarin.modules.account.model.dto.iban;

import java.util.List;

public class ResponseIbanInfoDto {

    private List<AccountOwner> accountOwners;
    private String status;
    private String statusDescription;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public List<AccountOwner> getAccountOwners() {
        return accountOwners;
    }

    public void setAccountOwners(List<AccountOwner> accountOwners) {
        this.accountOwners = accountOwners;
    }

    @Override
    public String toString() {
        return "ResponseIbanInfoDto{" +
                "accountOwners=" + accountOwners +
                ", status='" + status + '\'' +
                ", statusDescription='" + statusDescription + '\'' +
                '}';
    }
}
