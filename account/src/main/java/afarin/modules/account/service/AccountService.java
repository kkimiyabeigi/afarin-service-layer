package afarin.modules.account.service;

import afarin.modules.account.model.dto.account.AccountAvailableBalDto;
import afarin.modules.account.model.dto.account.InputDbLocalFundDto;
import afarin.modules.account.model.dto.account.OutputDbLocalFundDto;
import afarin.modules.account.model.entity.*;

import java.util.Date;
import java.util.List;

public interface AccountService {

    AccountEntity getAccountByNumber(String accountNumber);

    List<AccountEntity> getAllAccountInfo(String clientNo);

    AccountStatusEntity getAccountStatus(String accountStatus);

    AccountTypeEntity getAccountTypeDesc(String accountType);

    BranchEntity getBranchDesc(String branch);

    CcyEntity getCcyName(String ccy);

    FieldDescEntity getFieldDesc(String fieldValue);

    AccountJointEntity getAccountJoint(Long internalKey, String clientNo) ;

    List<AccountSignerEntity> getAcctInfoMastDetail(Long internalKey, String clientNo);

    AccountConditionEntity getAccountInfoMast(Long internalKey);

    String checkWithdrawCondition(String accountNo, String clientNoAll, Long amount, String terminalDate);

    OutputDbLocalFundDto getLocalFoundTransfer(InputDbLocalFundDto input);

    AccountEntity getAccountBalance(String accountNumber);

    Long getAccountInternalKey(String accountNumber);

    List<AccountSignerEntity> getAccountConditionSigner(Long internalKey, int conditionSequence);

    List<AccountConditionEntity> getAccountCondition(Long internalKey);

    AccountTransferAuditEntity getAccountTransferInquery(String traceId);

    String serializeToXML(InputDbLocalFundDto localFoundTransferDto);

    OutputDbLocalFundDto deserializeFromXML(String output);

    FieldDescEntity getSignerDesc(String fieldValue);

    List<AccountJointEntity> getAccountJointInternalKey(String clientNo);

    AccountEntity getAllAccountInfoByRole(Long internalKey);

    List<AccountSignerEntity> getAccountInfoMastDetailByClientNo(String clientNo);

    FieldDescEntity getDepositTypeDEsc(String fieldValue);

    String getJalaliByDate(Date gregorianDate, String jalaliFormat);

    String getJalaliByString(String gregorianDate, String gregFormat, String jalaliFormat);

    String getGregorianDate(String jalaliDate, String jalaliFormat, String gregFormat);

    AccountEntity getAccountByClientNo(Long internalKey, String clientNo);

    AccountAvailableBalDto getAvailableBalance(String accountNumber);
}

