package afarin.modules.account.service.impl;

import afarin.modules.account.repository.procedure.IbanRepository;
import afarin.modules.account.service.IbanService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class IbanServiceImpl implements IbanService {

    private Logger logger = LoggerFactory.getLogger(IbanServiceImpl.class);

    @Autowired
    IbanRepository accountRepository;

    @Override
    public String getLocalAccountIban(String accountNumber) {
        return accountRepository.getLocalAccountIban(accountNumber);
    }

}
