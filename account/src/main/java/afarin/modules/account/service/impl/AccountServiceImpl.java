package afarin.modules.account.service.impl;

import afarin.modules.account.exception.ClientNotFoundException;
import afarin.modules.account.exception.LocalFundTransactionException;
import afarin.modules.account.exception.LocalFundTransferException;
import afarin.modules.account.model.dto.account.AccountAvailableBalDto;
import afarin.modules.account.model.dto.account.InputDbLocalFundDto;
import afarin.modules.account.model.dto.account.OutputDbLocalFundDto;
import afarin.modules.account.model.entity.*;
import afarin.modules.account.repository.*;
import afarin.modules.account.repository.procedure.AccountInfoRepository;
import afarin.modules.account.repository.procedure.CalendarRepository;
import afarin.modules.account.service.AccountService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class AccountServiceImpl implements AccountService {

    private Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    AccountStatusRepository accountStatusRepository;

    @Autowired
    AccountTypeRepository accountTypeRepository;

    @Autowired
    BranchNameRepository branchNameRepository;

    @Autowired
    CcyNameRepository ccyNameRepository;

    @Autowired
    FieldDescRepository fieldDescRepository;

    @Autowired
    AccountJointRepository accountJointRepository;

    @Autowired
    AccountInfoRepository accountInfoRepository;

    @Autowired
    AccountConditionRepository accountConditionRepository;

    @Autowired
    AccountSignerRepository accountSignerRepository;

    @Autowired
    FundTransferInqueryRepository fundTransferInqueryRepository;

    @Autowired
    CalendarRepository calendarRepository;

    @Override
    public AccountEntity getAccountByNumber(String accountNumber) {
        return accountRepository.findByAccountNumber(accountNumber);
    }

    @Override
    public List<AccountEntity> getAllAccountInfo(String clientNo) {
        return accountRepository.getAllAccountInfo(clientNo);
    }

    @Override
    public AccountStatusEntity getAccountStatus(String accountStatus) {
        return accountStatusRepository.getAccountStatus(accountStatus);
    }

    @Override
    public AccountTypeEntity getAccountTypeDesc(String accountType) {
        return accountTypeRepository.getAccountTypeDesc(accountType);
    }

    @Override
    public BranchEntity getBranchDesc(String branch) {
        return branchNameRepository.getBranchDesc(branch);
    }

    @Override
    public CcyEntity getCcyName(String ccy) {
        return ccyNameRepository.getCcyName(ccy);
    }

    @Override
    public FieldDescEntity getFieldDesc(String fieldValue) {
        return fieldDescRepository.getFieldDesc(fieldValue);
    }

    @Override
    public AccountJointEntity getAccountJoint(Long internalKey, String clientNo) throws ClientNotFoundException {
        return accountJointRepository.getAccountJoint(internalKey, clientNo);
    }

    @Override
    public List<AccountSignerEntity> getAcctInfoMastDetail(Long internalKey, String clientNo) throws ClientNotFoundException {
        return accountSignerRepository.getAcctInfoMastDetail(internalKey, clientNo);
    }

    @Override
    public AccountConditionEntity getAccountInfoMast(Long internalKey) {
        return accountConditionRepository.getAccountInfoMast(internalKey);
    }

    @Override
    public String checkWithdrawCondition(String accountNo, String clientNoAll, Long amount, String terminalDate) {
        return accountInfoRepository.checkWithdrawCondition(accountNo, clientNoAll, amount, terminalDate);
    }

    @Override
    @Transactional(readOnly = false,
            propagation = Propagation.REQUIRES_NEW,
            rollbackFor = Exception.class)
    public OutputDbLocalFundDto getLocalFoundTransfer(InputDbLocalFundDto input) {
        OutputDbLocalFundDto localFundTransferDto;
        try {
            String localFundTransferXml = serializeToXML(input);
            String outLocalFundTransferXml = accountInfoRepository.getLocalFoundTransfer(localFundTransferXml);
            localFundTransferDto = deserializeFromXML(outLocalFundTransferXml);
        } catch (LocalFundTransferException e) {
            localFundTransferDto = deserializeFromXML(e.getMessage());
            List<String> errorDetail = new ArrayList<>();
            errorDetail.add(localFundTransferDto.getfDesc());
            throw new LocalFundTransactionException(localFundTransferDto.getErrno(), localFundTransferDto.getDesc(), null);
        }
        return localFundTransferDto;
    }

    @Override
    public AccountEntity getAccountBalance(String accountNumber) {
        return accountRepository.findAccountBalance(accountNumber);
    }

    @Override
    public Long getAccountInternalKey(String accountNumber) {
        return accountRepository.findAccountInternalKey(accountNumber).getInternalKey();
    }

    @Override
    public List<AccountSignerEntity> getAccountConditionSigner(Long internalKey, int conditionSequence) {
        return accountSignerRepository.findAccountConditionSigner(internalKey, conditionSequence);
    }

    @Override
    public List<AccountConditionEntity> getAccountCondition(Long internalKey) {
        return accountConditionRepository.findAccountCondition(internalKey);
    }

    @Override
    public AccountTransferAuditEntity getAccountTransferInquery(String traceId) {
        return fundTransferInqueryRepository.getFundTransferInqury(traceId);
    }

    @Override
    public String serializeToXML(InputDbLocalFundDto localFoundTransferDto) {
        String xmlString = "";
        try {
            XmlMapper xmlMapper = new XmlMapper();
            xmlString = xmlMapper.writeValueAsString(localFoundTransferDto);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return xmlString;
    }

    @Override
    public OutputDbLocalFundDto deserializeFromXML(String output) {
        OutputDbLocalFundDto localFoundTransferDto = new OutputDbLocalFundDto();
        try {
            XmlMapper xmlMapper = new XmlMapper();
            localFoundTransferDto = xmlMapper.readValue(output, OutputDbLocalFundDto.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return localFoundTransferDto;
    }

    @Override
    public FieldDescEntity getSignerDesc(String fieldValue) {
        return fieldDescRepository.getSignerDesc(fieldValue);
    }

    @Override
    public List<AccountJointEntity> getAccountJointInternalKey(String clientNo) {
        return accountJointRepository.getAccountJointInfo(clientNo);
    }

    @Override
    public AccountEntity getAllAccountInfoByRole(Long internalKey) {
        return accountRepository.getAllAccountInfoByRole(internalKey);
    }

    @Override
    public List<AccountSignerEntity> getAccountInfoMastDetailByClientNo(String clientNo) {
        return accountSignerRepository.getAcctInfoMastDetailByClientNo(clientNo);
    }

    @Override
    public FieldDescEntity getDepositTypeDEsc(String fieldValue) {
        return fieldDescRepository.getFieldDescDepositType(fieldValue);
    }

    @Override
    public AccountEntity getAccountByClientNo(Long internalKey,String clientNo){
        return accountRepository.getAccountByClientNo(internalKey,clientNo);
    }

    @Override
    public String getJalaliByDate(Date gregorianDate, String jalaliFormat) {
        return calendarRepository.getJalaliByDate(gregorianDate, jalaliFormat);
    }

    @Override
    public String getJalaliByString(String gregorianDate,String gregorianFormat, String jalaliFormat) {
        return calendarRepository.getJalaliByString(gregorianDate,gregorianFormat, jalaliFormat);
    }

    @Override
    public String getGregorianDate(String jalaliDate, String jalaliFormat, String gregFormat) {
        return calendarRepository.getGregorianDate(jalaliDate, jalaliFormat, gregFormat);
    }

    @Override
    public AccountAvailableBalDto getAvailableBalance(String accountNumber) {
        return accountInfoRepository.getAvailableBalance(accountNumber);
    }
}
