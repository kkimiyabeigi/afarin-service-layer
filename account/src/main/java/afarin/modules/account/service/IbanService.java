package afarin.modules.account.service;

public interface IbanService {

    String getLocalAccountIban(String accountNumber);
}
