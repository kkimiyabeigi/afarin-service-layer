package afarin.modules.gateway.model.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(
    name = "tbl_role",
    uniqueConstraints = {@UniqueConstraint(name = "uc_user_role", columnNames = {"role", "user_id"})})
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Getter
@Setter
@ToString
public class Role {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_role")
  @SequenceGenerator(sequenceName = "seq_role", allocationSize = 1, name = "seq_role")
  private Long id;

  @NotNull
  @Column(name = "role", length = 50, nullable = false)
  private String role;

  @ManyToOne
  @JoinColumn
  private User user;


}
