package afarin.modules.gateway.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@Entity
@Table(
    name = "tbl_user",
    uniqueConstraints = {@UniqueConstraint(name = "uc_user_username", columnNames = "username")})
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@ToString(exclude = "password")
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_user")
  @SequenceGenerator(sequenceName = "seq_user", allocationSize = 1, name = "seq_user")
  private Long id;

  @NotNull
  @Column(name = "username", length = 50, nullable = false)
  private String username;

  @NotNull
  @Column(name = "ip", length = 15, nullable = false)
  private String ip;

  @JsonIgnore
  @NotNull
  @Column(name = "password", length = 100, nullable = false)
  private String password;

  @ColumnDefault("1")
  @Column(name = "is_active")
  private Boolean isActive = true;

  @OneToMany(mappedBy = "user")
  private Collection<Role> roles;


}
