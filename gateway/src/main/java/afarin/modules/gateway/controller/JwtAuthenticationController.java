package afarin.modules.gateway.controller;

import afarin.modules.gateway.model.dto.JwtRequestDTO;
import afarin.modules.gateway.model.dto.JwtResponseDTO;
import afarin.modules.gateway.service.JwtAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;

@RestController
@RequestMapping("/gateway")
public class JwtAuthenticationController {

  private final JwtAuthenticationService jwtAuthenticationService;

  @Autowired
  public JwtAuthenticationController(JwtAuthenticationService jwtAuthenticationService) {
    this.jwtAuthenticationService = jwtAuthenticationService;
  }

  /**
   * createAuthenticationToken(/api/authenticate)
   *
   * @param jwtRequestDTO
   * @return JwtResponseDTO
   * @throws Exception
   */
  @PostMapping("/login")
  public ResponseEntity<JwtResponseDTO> createAuthenticationToken(
      @RequestBody JwtRequestDTO jwtRequestDTO)
      throws BadCredentialsException, DisabledException {

    try {
      JwtResponseDTO authenticationToken =
          jwtAuthenticationService.createAuthenticationToken(jwtRequestDTO);
      return ResponseEntity.ok().body(authenticationToken);
    } catch (BadCredentialsException e) {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
  }
}
