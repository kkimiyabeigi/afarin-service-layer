package afarin.modules.gateway.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FallBackController {

    @RequestMapping(path="/server-failure",method= RequestMethod.GET)
    public String ServerFailure(){
        return "Server Is Down";
    }
}
