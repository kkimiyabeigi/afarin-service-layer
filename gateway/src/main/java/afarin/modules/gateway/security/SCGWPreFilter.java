package afarin.modules.gateway.security;

import afarin.modules.gateway.constant.Constants;
import afarin.modules.gateway.security.jwt.JwtTokenUtil;
import afarin.modules.gateway.service.CustomUserDetailsService;
import io.jsonwebtoken.ExpiredJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
public class SCGWPreFilter extends AbstractGatewayFilterFactory<Object> {
  private final Logger log = LoggerFactory.getLogger(getClass().getName());
  private final JwtTokenUtil jwtTokenUtil;
  private final CustomUserDetailsService customUserDetailsService;

  public SCGWPreFilter(
      JwtTokenUtil jwtTokenUtil, CustomUserDetailsService customUserDetailsService) {
    //    super(Config.class);
    this.jwtTokenUtil = jwtTokenUtil;
    this.customUserDetailsService = customUserDetailsService;
  }

  private boolean isAuthorizationValid(ServerHttpRequest request, UserDetails userDetails) {
    boolean isValid = false;

    for (GrantedAuthority authority : userDetails.getAuthorities()) {
      if (request.getURI().getPath().contains(authority.getAuthority())) {
        isValid = true;
        break;
      }
    }
    return isValid;
  }

  private Mono<Void> onError(ServerWebExchange exchange, String err, HttpStatus httpStatus) {
    ServerHttpResponse response = exchange.getResponse();
    response.setStatusCode(httpStatus);

    return response.setComplete();
  }

  @Override
  public GatewayFilter apply(Object config) {
    return (exchange, chain) -> {
      UserDetails userDetails = jwtValidate(exchange.getRequest());
      if (userDetails == null
          || !isAuthorizationValid(exchange.getRequest(), userDetails)
          || !userDetails.isEnabled()) {
        return this.onError(exchange, "Unauthorized user", HttpStatus.UNAUTHORIZED);
      }

      /*ServerHttpRequest modifiedRequest = exchange.getRequest().mutate().
              header("secret", RandomStringUtils.random(10)).
              build();

      return chain.filter(exchange.mutate().request(modifiedRequest).build());*/

      return chain.filter(exchange);
    };
  }

  private UserDetails jwtValidate(ServerHttpRequest request) {
    UserDetails result = null;

    final String requestTokenHeader =
        request.getHeaders().getFirst(Constants.security.jwtToken.HEADER_STRING);
    String username = null;
    String jwtToken = null;

    // JWT Token is in the form "Bearer token". Remove Bearer word and get
    // only the Token
    if (requestTokenHeader != null
        && requestTokenHeader.startsWith(Constants.security.jwtToken.TOKEN_PREFIX_BEARER)) {
      jwtToken = requestTokenHeader.substring(7);
      try {
        username = jwtTokenUtil.getUsernameFromToken(jwtToken);
      } catch (IllegalArgumentException e) {
        log.info("Unable to get JWT Token");
      } catch (ExpiredJwtException e) {
        log.info("JWT Token has expired");
      }

    } else {
      log.info("JWT Token does not begin with Bearer String");
    }

    // Once we get the token validate it.

    if (username != null) {

      UserDetails userDetails = customUserDetailsService.loadUserByUsername(username);

      // if token is valid configure Spring Security to manually set
      // authentication
      if (jwtTokenUtil.validateToken(jwtToken, userDetails, request)) {
        result = userDetails;
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
            new UsernamePasswordAuthenticationToken(
                userDetails, null, userDetails.getAuthorities());

        // After setting the Authentication in the context, we specify
        // that the current user is authenticated. So it passes the
        // Spring Security Configurations successfully.
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
      }
    }

    return result;
  }
}
