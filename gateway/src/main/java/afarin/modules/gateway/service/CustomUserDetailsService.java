package afarin.modules.gateway.service;

import afarin.modules.gateway.model.dao.UserRepository;
import afarin.modules.gateway.model.entity.Role;
import afarin.modules.gateway.model.entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
@Transactional
public class CustomUserDetailsService implements UserDetailsService {

  private UserRepository userRepository;

  public CustomUserDetailsService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  /**
   * loadUserByUsername
   *
   * @param username
   * @return UserDetails
   * @throws UsernameNotFoundException
   */
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Optional<User> userOptional = userRepository.findByUsername(username);
    userOptional.orElseThrow(
        () -> new UsernameNotFoundException("No user found with username " + username));

    Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
    for (Role role : userOptional.get().getRoles()) {
      grantedAuthorities.add(new SimpleGrantedAuthority(role.getRole()));
    }

    return new org.springframework.security.core.userdetails.User(
        userOptional.get().getUsername(),
        userOptional.get().getPassword(),
        userOptional.get().getIsActive(),
        true,
        true,
        true,
        grantedAuthorities);
  }
}
