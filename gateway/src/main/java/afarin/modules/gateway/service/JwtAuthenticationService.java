package afarin.modules.gateway.service;

import afarin.modules.gateway.model.JsonWebToken;
import afarin.modules.gateway.model.dto.JwtRequestDTO;
import afarin.modules.gateway.model.dto.JwtResponseDTO;
import afarin.modules.gateway.model.dto.UserDTO;
import afarin.modules.gateway.security.jwt.JwtTokenUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Service
public class JwtAuthenticationService {

  private final Logger log = LoggerFactory.getLogger(getClass().getName());
  private final JwtTokenUtil jwtTokenUtil;
  private final CustomUserDetailsService customUserDetailsService;
  private final UserService userService;

  public JwtAuthenticationService(
          JwtTokenUtil jwtTokenUtil,
          CustomUserDetailsService customUserDetailsService,
          UserService userService) {
    this.jwtTokenUtil = jwtTokenUtil;
    this.customUserDetailsService = customUserDetailsService;
    this.userService = userService;
  }

  /**
   * createAuthenticationToken
   *
   * @param jwtRequestDTO
   * @return JwtResponseDTO
   * @throws Exception
   */
  public JwtResponseDTO createAuthenticationToken(JwtRequestDTO jwtRequestDTO)
          throws BadCredentialsException, DisabledException {
    log.debug("Request to createAuthenticationToken by {}", jwtRequestDTO);

    authenticate(jwtRequestDTO.getUsername(), jwtRequestDTO.getPassword());
    final UserDetails userDetails =
        customUserDetailsService.loadUserByUsername(jwtRequestDTO.getUsername());
    UserDTO userDTO = userService.findByUserName(jwtRequestDTO.getUsername());
    JsonWebToken jsonWebToken = jwtTokenUtil.generateToken(userDetails, userDTO.getIp());
    JwtResponseDTO jwtResponseDTO = new JwtResponseDTO();
    jwtResponseDTO.setJwtToken(jsonWebToken.getToken());
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    jwtResponseDTO.setExpirationStr(dateFormat.format(jsonWebToken.getExpiration()));
    jwtResponseDTO.setExpiration(jsonWebToken.getExpiration().getTime());

    return jwtResponseDTO;
  }

  /**
   * authenticate
   *
   * @param username
   * @param password
   * @throws Exception
   */
  private void authenticate(String username, String password)
      throws BadCredentialsException, DisabledException {
    UserDTO user = userService.findByUserNameAndPassword(username, password);
    if (user == null) throw new BadCredentialsException("INVALID_CREDENTIALS");
    else if (!user.getIsActive()) throw new DisabledException("USER_DISABLED");
  }
}
